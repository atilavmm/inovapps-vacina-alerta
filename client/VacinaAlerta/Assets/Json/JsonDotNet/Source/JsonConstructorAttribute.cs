#if !UNITY_WINRT || UNITY_EDITOR || (UNITY_WP8 &&  !UNITY_METRO)
using System;

namespace NewtonsoftUnity.Json
{
  /// <summary>
  /// Instructs the <see cref="JsonSerializer"/> not to serialize the public field or public read/write property value.
  /// </summary>
  [AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Property, AllowMultiple = false)]
  public sealed class JsonConstructorAttribute : System.Attribute
  {
  }
}
#endif