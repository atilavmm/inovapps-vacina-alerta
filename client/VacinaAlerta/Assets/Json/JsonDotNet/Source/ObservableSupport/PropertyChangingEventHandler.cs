#if !UNITY_WINRT || UNITY_EDITOR || (UNITY_WP8 &&  !UNITY_METRO)
using System;

namespace NewtonsoftUnity.Json.ObservableSupport
{
	public delegate void PropertyChangingEventHandler(Object sender, PropertyChangingEventArgs e);

}

#endif