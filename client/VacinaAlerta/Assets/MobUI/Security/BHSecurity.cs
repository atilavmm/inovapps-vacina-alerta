
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Paddings;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using UnityEngine;
using NewtonsoftUnity.Json;
using com.bighutgames.vacina.application.util;

public class BHSecurity : MonoBehaviour
{
	private static string key = "58azfDGXNo5zVhY5S2Tj07RNvA6uPY2ZsxZOQ5eufdsw8TZEpt2I7ko3bEuV57ZGHyY3DyBoKvc2eBiz0NHIE2qJ48CM7cf4sUnT";

	public static PaddedBufferedBlockCipher GetCipher(bool encrypt)
	{
		var keyBytes = new byte[16];
		var secretKeyBytes = Encoding.UTF8.GetBytes(key.ToCharArray(), 0, key.Length);
		Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
		var iv = keyBytes;

		PaddedBufferedBlockCipher rij = new PaddedBufferedBlockCipher (
			new CbcBlockCipher (new RijndaelEngine (128)),new Pkcs7Padding());
		ParametersWithIV ivAndKey = new ParametersWithIV (new KeyParameter(keyBytes),iv);
		rij.Init (encrypt, ivAndKey);
		return rij;
	}

	public static string Encrypt(string plainTextRaw)
    {
        try
        {
			
			byte[] plainText = Encoding.UTF8.GetBytes(plainTextRaw.ToCharArray(), 0, plainTextRaw.Length);

			PaddedBufferedBlockCipher cipher = GetCipher(true);
			
			byte[] cipherText = new byte[cipher.GetOutputSize(plainText.Length)];
		
			int cipherLength = cipher.ProcessBytes(plainText, 0, plainText.Length, cipherText, 0);
			cipher.DoFinal (cipherText, cipherLength);

			string result = Convert.ToBase64String(cipherText);
			return result;
        } catch (InvalidCipherTextException e) {
            throw new Exception (e.Message, e);
        }
        return "";
    }

	public static string Decrypt(string plainTextRaw)
    {
        try
		{
			byte[] plainText = Convert.FromBase64String(plainTextRaw);


			PaddedBufferedBlockCipher cipher = GetCipher(false);

			byte[] cipherText = new byte[cipher.GetOutputSize(plainText.Length)];

			int cipherLength = cipher.ProcessBytes(plainText, 0, plainText.Length, cipherText, 0);
			cipher.DoFinal (cipherText, cipherLength);

			string result = Encoding.UTF8.GetString(cipherText, 0, cipherText.Length);

			int endIndex = result.IndexOf('\0');
			if(endIndex != -1)
				result = result.Substring(0, endIndex);
			
			return result;

        } catch (InvalidCipherTextException e) {
            throw new Exception (e.Message, e);
        }
        return "";
    }

	void Awake()
	{
		string data = "E/j9UDRjG3NdKzSeZvPigbs9/o8BW4MZCh8oECjcXp9AkJHodzqogcRZZcOYl8dn5lciqOcKyw1TdTFakf8pzp7VM6lsg9Yq+oc2p2FN9qd4KA8rdsKPFrrjMObgTs51HE5eXcBdWnMBt/JoYUTjWUQXngALPc+lbN8nXslWRDOsyJ4E2GYO+UzQar8DeMO+t/F7HS/tS6KEasYnomshVL+PJI4TxmOSXLIY7MEmUSPk8BrlRheoEkp+jytcDCJ+wDGyqT7JF+/55dtLYMF5eKvcEaRQFID7OMy8e6o75XtwAv9cDp5pnAZr0eH1pnRiXQRQwhYpFWhymnpQuBn8H0rmt0hifZCQEYrxtGaTBdUnlW+pRbLNQplTbNcgRKgiJl/60E6mF1f8aDAd//FPtNI+L/kQKFlBu4JXwWst/qSqUh9m4B9iINvHOJdreWTfIwDchlTyn0/90GMJR3hrZT2pIY3zlhxzAgELu2FKZV/3uEyRx0AylFWWmihjxFDWMU/9tsqE4Xnzl53RG1YM9uRQjXilbbtoF0jFAB3pNEtqdygPqSi88zs+e6IJxzAs2ob2+Ml1lOPn4hJ+4n1sSvP0855KlqEne7rHFnZivHQGDyxjQqA36VRGmHsZIphVdkSnzZX1PdtFV+WFbgLg+woAyEY/SwJGlWeaJA++56YNXj2Wtxvm6uU9/rtOXhY5n3bxZIxt92pLM4xTUvelzJhp3LDGlPMBr9J7g2M1cq1ouGAyK6et10NYxzv2NAgT10HzUjxm5NgF8AG7UR8FDiQPr9edy7HHFafEBSGdZSkTlSVmeiYAehJKrm+8M3+8r5z6PSnDeCHEoT5XkNN0rWTGLF3Jte6cWgVLRFtlgGKafFEj9wxxi+T6sGj+BFKplRHEw2ntruWTxWxXG4Z6BHKar5cGmPF115ZaDA6CkPz8D5ct3fLyeW/WQ5kyCo4UJ4TOySnuJ38XgRTrhTpBM9ayD+t2vM9wDPva7Db/ZS1W62JyYRlXsvIobJZQ5m/JAZUF+BUGWCfs7yBPEDCfU02/zNVZ0UAyGyA4rVUttTPD3MrsCvsKXCxcV/siey/pPMrf8fXfNr30yzafrhvzlKrPaeFyqUsYyPaNdC5W/VLiEwOkEWiNy8wYXFB1W97lBSNTwGzCdaRuQ6V/JBAb7WWjTCjXED4XANH1jpMVuTlHasl4pgJJkPOi7xMGy4IquLYi7xOrysvs8ZcsQpNIoAxIgz34yGhAxFpXFVbowvkuoZS2Y3xwdSxeNbbYYDfRavC+fHJ3gP84sbt9K+QpjlKl1qJHbUqA4hhHVHEn48Aygc6GL3MSlGb6ZZ2+1aN2OVGGcq/OdcXhOtw65UCSg6qMtZiDR1bQ8XUqTVmS0WJWH9xGaSGgSxIjakYeVNlzyLYbUAa1CHcGUi1UVOPBf+52zRGMu/+2SCr7zlwWJD6HXpldZyIA2XjcYrNlTm7JEOGr6hLI0nN+U1omUjAHd8nUpCgepmJo57IvOlGLmX0vCQdOykGcsnpkLK1VVugSJM9ilV/Ydv31KWeGcLYqIgd3mJrrQak78MirySCWmiWHFg8KMN0hQ/ZnwV01SNDEUfjVC2Shmv6OFbCA3bfxYJxb/+w1wEJGgsP8Ub9wnwAd1usV/Jn6mgjk4vaZBJJLO9yCuAYGyZ/1TofeibV8GeSdH15kfOd7epKIwRUstejWh9VbAwFCi2mZgMLbEDKnd9kLoIXP6HviU7vakA8doxRBYCCC8QtEjZGh5BT0E+ETTlrXOksySAXknLPaK3W/8goUvU/dhdrKXK4iJw+7ndaYoE3iFuiWU1/D0F9KYjZAUbSoW7ILMntZp7UBiAHkOFOl0pQ11mk2umBZ+V2c+gFYHsgHAPTjOTW5448wxNziJMKF1gUmVZ348+aVdkzrSbnZzrddFV2AbxNrHpCbGGtLGjIU82Mg0CDIR00d0HM7J0TA3Np+pz0zg4vDmqa1YYb4dn/hNwjgVx9MEwRwWtlBHrwfOfajKkFH9NB2TDpjA1kYngCX2EZA6biOwzqKI4WcXMvfy7zKBFxm+kRytO0M4cxM/+qf7z3Vy1ufeRw6Zks7pJ4ZdI7qv7mIaS3RhmwctO+/2RxetlEvZGOWzNMD4gZxle6g74mq0Wucnd8gaNFzG+yJmmcuep91tp6/aRFahP/tPRbneL7uJ6p65WVcikRRXPQVpHm1T7hQYCrREtdOPAXyXOdSnRU3ZtEudXLxB/ljogK3mFwmesuXsrocVHDI/Fq4ii16IJ7AWgaA5aT8q+KTKtp/CQz018rNaBqIttASBb3Mqk7z6MZ85tz96RxArw1JDnQAZPchqjwCS3nxwelWniI47WYt4VuomKIx6NhK/Sysh4mPJ3/PXuKIH8KcGslSsUUo5ssZY1OvLy9E98Ea17V7/EKYht3Naza4r7VquxlYyp40InwnoBjt4q9TpCW2HPYCsTpquTtsFnHUOG91sfQiDAUh+7nVUuUxrKnLU7lXr1fW86xGuF3SbKUmVToLIGgnJwTTrMvu/OhANlaS0YuVUzXtydLTu27ydgkX96stoHYu/YsFDg8nUwxVOcNzBxvf+ep7Hx9R+1IIBocEa019KhsvKt2MaQFg7VIf3rvZCBR/WvKVB0Bak5NmWjiDrBzh73htEUijlKjOR4S/4wgWSsUhqh/d3xVZaiyrzWIZnH53sOAPcbyjgdsw+yRKhKhprU1XsHajcPg2nybBvk7oY+vt2U4r9UFlHASel9QJfpeMRfy3zGCWMCPiV4wJ1Y3/Vsw7B8wxulHc4zxTLh9D3rPuAWFPiEZkdgnj5sBz/ss1qCX0s31WXqsdUOoNbgHH4SXkwfh036gj4zKWtwhAulnH67c4SiaSAmITuELjTp99k50sUtXlm1fnz92sTN4bjeEEKzBYN5fz6M5dL6yz0LsG0ayTrCU+vE79oWMGooDWmVf6JPaSafWZk4eGeL75J4dECaanJDByQMSvg0mVQx5rvMdFbz4WdVkQ0h51S8JmsAbF4nWkyg+iZhx2FDi2euzxm+t/mYuOhHUM+++UKbJuGNrf4FdjXHJ+js8Lhszwwb7b3TA7Vmfgm+W9JehXu7Rc8lTttIvZi2jbjvAPl6Q60vsSim0AvtzfWqEQ6F8pGXvKV2xv4DDS4xqewrEdnjaXC7uJzslPkdGdYvY5aRhpwOqWRnJkMmXunEi2MPLAdEz2hHk7UzKzeFwzyJv1D9BShV72i0QzJ5T03YeOwk4ez4x10Kd6W2sz/PoO/xzfrJ62eTXHf8gq1Lspa6s8cWzOvdCXiATrJMM2cacQ4to3ZPraa3ctPMBjsWLtIZ5Ix0N7PgW4pbbj+270dJtbf796QqbcM0yY7MuiDdWCd/e8ovWT2i39vnh7QGWCpRZB/qjiZmCeFmkB/EHpXVqK2CacTJIh6N195+7b+YElx++Redz/m1XEnvw1X/2aENaYv6kMgMzxrRum+IIWjQPBXJ9rOrfZkXYDmuyEwY+i23fiMtzbLfW8X6tds4TB/D/a4c9nW2cFsIfg4DYPqYuAYvodpGsZIxMWGw9lbAwNxPH/MiarT6FWv88HA/3vumHgGS1GTN/k3gxwnSn/tAb+qWdVidp44MLaRkcDOpOC+nVpeVuVJgUmhRqvgBCiN3wn/iLLp8LqwR7nE0HuWqubkl06etBvRqNcKYCT1RzvLLTIsdJ5WSg76v1NIKhB4j6JcxGYzmk8zcpDQs8ZCo/P+RnKX75kLl0pUqOfL26lRwfkHh9QxFscPQpF+VvccwqkfyTrI+0o6+b5KmbrDZV/TU7u8x/rXEi43uKzEZicY/nsG3h0G6gmoBRsE1YoZ64Q9bbpGxGLxcmduFrONGb+GH3EYcsksAPGV6meLKs7tXQMbt36lTy24Zg511TeEEl1ONDpkda/XpShYn5o0D6VtN2byIMbz7vdrAiwBTglH9rHBUcOvQjueuk9Tpxh/VbMUwPws+7rNYu0V4XNwwv9RIRoYZg=";
		string decrypted = Decrypt (data);
		Debug.Log (decrypted);
		JsonConvert.DeserializeObject<LoginReturn> (decrypted);

	}
}
