using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

[ExecuteInEditMode()]
public class LocalizeLabel : MonoBehaviour 
{
	#region Unity Fields
	public bool IsStatic;
	#endregion Unity Fields

	#region Fields
	[SerializeField]
	private string key;
	[SerializeField]
	private string[] elements;

	private string lastKey;
	private Text text;
	private TextMesh textMesh;
	#endregion Fields

	#region Properties
	public string Key 
	{
		get 
		{
			if(lastKey != key)
			{
				lastKey = key;
				Refresh();
			}
			return key;
		}
		set
		{
			key = value;
			Refresh();
		}
	}

	public string[] Elements 
	{
		get
		{
			return elements;
		}
		set 
		{
			elements = value;
			Refresh();
		}
	}

	#endregion Properties

	#region Monobehaviour
	void Awake()
	{
		text = GetComponent<Text>();
		textMesh = GetComponent<TextMesh>();

		Refresh ();
		if(IsStatic)
			Destroy(this);
	}
#if UNITY_EDITOR
	void Update()
	{
		Refresh();
	}
#endif
	#endregion Monobehaviour

	#region Methods
	public void Refresh()
	{
		if (!string.IsNullOrEmpty (key)) 
		{
			try 
			{
				SetText(string.Format (key.Localize (), elements));
			}
			catch (Exception e) 
			{
				Debug.LogWarning (e.Message);
				Debug.LogWarning ("Can't localize text " + key + " GO: " + gameObject.name);
			}
		}
	}
	#endregion Methods

	#region Auxiliar Methods
	private void SetText(string message)
	{
		if(textMesh != null)
			textMesh.text = message;
		if(text != null)
			text.text = message;
	}
	#endregion Auxiliar Methods
}
