using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BigHut
{
	public class LocalizeManager
	{
		#region Singleton
		private static LocalizeManager instance;
	
		public static LocalizeManager Instance 
		{
			get 
			{
				if(instance == null)
					instance = new LocalizeManager();
				return instance;
			}
		}
		#endregion Singleton
		
		#region Fields
		private Dictionary<string, object> language = null;
		#endregion Fields
		
		
		#region Properties
		public Dictionary<string, object> Language 
		{
			get 
			{
				if(language == null)
				{
					LoadDefaultLanguage();
				}
				return this.language;
			}
		}
		#endregion Properties
		
		#region Constructor
		public LocalizeManager ()
		{
			
		}
		#endregion Constructor
		
		
		#region Methods
		public void LoadDefaultLanguage ()
		{
			string defaultLanguage = Application.systemLanguage.ToString();
			string pathLanguage = string.Format("Localization/{0}",defaultLanguage);
			UnityEngine.Object asset = Resources.Load(pathLanguage);
			if(asset == null)
				asset = Resources.Load("Localization/English");
			language =  (asset as TextAsset).text.hashtableFromJson();
		}
		
		public string GetElement(string key)
		{
			return Language[key].ToString();
		}

		public string GetSpecificElement(string key, string languageName)
		{
			string pathLanguage = string.Format("Localization/{0}",languageName);
			UnityEngine.Object asset = Resources.Load(pathLanguage);
			Dictionary<string, object> specificLanguage = (asset as TextAsset).text.hashtableFromJson();
			return specificLanguage[key].ToString();
		}
		
		public bool Contains(string key)
		{
			return Language.ContainsKey(key);
		}
		#endregion Methods
	}
	
}