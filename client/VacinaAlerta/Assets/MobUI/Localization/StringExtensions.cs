﻿using BigHut;

public static class StringExtensions
{
	public static string Localize(this string element)
	{
		return LocalizeManager.Instance.GetElement(element);
	}
}
