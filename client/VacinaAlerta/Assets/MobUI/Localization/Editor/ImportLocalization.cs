﻿using UnityEngine;
using System.Collections;
using Google.GData.Client;
using Google.GData.Spreadsheets;
using UnityEditor;
using System.Collections.Generic;
using NewtonsoftUnity.Json;
using System.IO;
using System.Net;

public class ImportLocalization : EditorWindow 
{

	#region Fields
	private static string CLIENT_ID = "563637927751-l5244o6muq74kgdhegv5qn9ml2q4j2am.apps.googleusercontent.com";
	private static string CLIENT_SECRET = "MJWCTd4Mt1rapew4W7ooBJS-";
	private static string REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";
	private static string SCOPE = "https://spreadsheets.google.com/feeds/ https://docs.google.com/feeds/";
	
	private static string ACCESS_TOKEN = "";

	public static string SHEET;

	private int currentStep;
	#endregion Fields

	#region UI Methods
	[MenuItem ("MobUI/Importer/Localization")]
	static void OpenLocalizationWindow() 
	{
		var window = ScriptableObject.CreateInstance<ImportLocalization>();
		window.Show();
	}

	void Awake()
	{
		if(System.IO.File.Exists(string.Format("{0}/localization.config",Application.dataPath)))
		{
			string raw = System.IO.File.ReadAllText(string.Format("{0}/localization.config",Application.dataPath));
			Dictionary<string, string> data = JsonConvert.DeserializeObject<Dictionary<string, string>>(raw);

			CLIENT_ID = data["id"];
			CLIENT_SECRET = data["secret"];
			SHEET = data["sheet"];
			ACCESS_TOKEN = data["access"];
		}
	}
	void OnGUI () 
	{
		if(currentStep == 0)
		{
			GUILayout.Label ("Base Settings", EditorStyles.boldLabel);
			CLIENT_ID = EditorGUILayout.TextField ("ID", CLIENT_ID);
			CLIENT_SECRET = EditorGUILayout.TextField ("Secret", CLIENT_SECRET);
			SHEET = EditorGUILayout.TextField ("Sheet", SHEET);
			
			if(GUILayout.Button("Import Localization"))
			{
				GetAuthenticate();
				currentStep = 1;
				Save();
			}
		}
		else if(currentStep == 1)
		{
			GUILayout.Label ("Base Settings", EditorStyles.boldLabel);
			ACCESS_TOKEN = EditorGUILayout.TextField ("Access Token", ACCESS_TOKEN);
			if(GUILayout.Button("Download"))
			{
				ImportAssets();
				currentStep = 0;
				Save();
			}
		}
	}
	#endregion UI Methods

	#region Auxiliar Methods
	private void Save()
	{
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["id"] = CLIENT_ID;
		data["secret"] = CLIENT_SECRET;
		data["sheet"] = SHEET;
		data["access"] = ACCESS_TOKEN;
		System.IO.File.WriteAllText(string.Format("{0}/localization.config",Application.dataPath), JsonConvert.SerializeObject(data));

	}

	private void GetAuthenticate()
	{
		// Create OAuth2 Parameters.
		OAuth2Parameters oAuthParams = new OAuth2Parameters();
		oAuthParams.ClientId = CLIENT_ID;
		oAuthParams.ClientSecret = CLIENT_SECRET;
		oAuthParams.RedirectUri = REDIRECT_URI;
		oAuthParams.Scope = SCOPE;
		
		// Open web browser for user to authenticate.
		string authUrl = OAuthUtil.CreateOAuth2AuthorizationUrl(oAuthParams);
		Application.OpenURL(authUrl);
	}
	
	private void ImportAssets()
	{
		ServicePointManager.ServerCertificateValidationCallback = (a,b,c,d) => true;

		SpreadsheetsService service = new SpreadsheetsService("bhlocalization");

		OAuth2Parameters parameters = new OAuth2Parameters();
		parameters.ClientId = CLIENT_ID;
		parameters.ClientSecret = CLIENT_SECRET;
		parameters.RedirectUri = REDIRECT_URI;
		parameters.Scope = SCOPE;
		parameters.AccessCode = ACCESS_TOKEN;

		OAuthUtil.GetAccessToken(parameters);
		string accessToken = parameters.AccessToken;

		GOAuth2RequestFactory requestFactory = new GOAuth2RequestFactory(null, "bhlocalization", parameters);
		service.RequestFactory = requestFactory;


		SpreadsheetQuery query = new SpreadsheetQuery();
		
		SpreadsheetFeed feed = service.Query(query);
		
		SpreadsheetEntry balance = null;



		foreach (SpreadsheetEntry entry in feed.Entries)
		{
			if(entry.Title.Text.Equals(SHEET))
			{
				balance = entry;
				break;
			}
		}
		
		if(balance == null)
		{
			Debug.LogError("Connection error");
			return;
		}
		
		WorksheetEntry mission = null;
		
		foreach (WorksheetEntry worksheet in balance.Worksheets.Entries)
		{
			if(worksheet.Title.Text.Equals("Base"))
			{
				mission = worksheet;
				break;
			}
		}
		
		if(mission == null)
		{
			Debug.LogError("Connection error");
			return;
		}

		
		CellQuery cellQuery = new CellQuery(mission.CellFeedLink);
		
		
		
		CellFeed cellFeed = service.Query(cellQuery);
		
		Hashtable languages = new Hashtable();
		
		int columnCount = (int)mission.Cols;
		
		for(int i = 2; i < columnCount;i++)
		{
			string retorno = "{\n";
			string language = (cellFeed.Entries[i] as CellEntry).Value;
			for(int j = 1; j < mission.Rows;j++)
			{
				string key = (cellFeed.Entries[j*columnCount] as CellEntry).Value.ToString();
				string value = ((cellFeed.Entries[j*columnCount+i] as CellEntry).Value).ToString();
				value = value.Replace("\"", "\\\"");
				retorno = string.Format("{2}\n\"{0}\":\"{1}\"",key,value,retorno);
				if(j+1<mission.Rows)
					retorno = string.Format("{0},",retorno);
			}
			languages.Add(language, (retorno+"\n}"));
		}

		string path = string.Format("{0}/Resources", Application.dataPath);
		if(!Directory.Exists(path))
			Directory.CreateDirectory(path);
		path = string.Format("{0}/Resources/Localization", Application.dataPath);
		if(!Directory.Exists(path))
			Directory.CreateDirectory(path);
		
		for(int i = 2; i < columnCount;i++)
		{
			System.IO.File.Delete(string.Format("{0}/Resources/Localization/{1}.txt",Application.dataPath,(cellFeed.Entries[i] as CellEntry).Value));
			string data = languages[(cellFeed.Entries[i] as CellEntry).Value].ToString();
			System.IO.File.WriteAllText(
				string.Format("{0}/Resources/Localization/{1}.txt",Application.dataPath,(cellFeed.Entries[i] as CellEntry).Value),
				data);
		}
		
		
		AssetDatabase.Refresh();
		AssetDatabase.SaveAssets();
	}
	#endregion Auxiliar Methods
}
