﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq;
using System.IO;
using Unity.IO.Compression;

public class ImportGamedata : EditorWindow  {

	[MenuItem ("MobUI/Importer/GameData")]
	static void GetGameData() 
	{
		WWW www = GetRequest("compressedbackup/get");


		while (!www.isDone) {
			System.Threading.Thread.Sleep(0);
		}

		if (string.IsNullOrEmpty (www.error)) {
			File.WriteAllBytes("Assets/Resources/Data/GameData.bytes",Zipper.ZipData(www.text));
			Debug.Log("Game Data Imported");
		} else {
			Debug.Log(www.error);
		}
	}

	public static WWW GetRequest(string method)
	{
		string currentUrl =  "https://vacinaalerta.appspot.com/_ah/api/vacinaserver/v1/"+method;
		WWW www = new WWW(currentUrl);
		return www;
	}

	[MenuItem ("MobUI/Importer/GameData(Stage)")]
	static void GetGameDataStage() 
	{
		WWW www = GetRequestStage("compressedbackup/get");
		
		
		while (!www.isDone) {
			System.Threading.Thread.Sleep(0);
		}
		
		if (string.IsNullOrEmpty (www.error)) {
			File.WriteAllBytes("Assets/Resources/Data/GameData.bytes",Zipper.ZipData(www.text));
			Debug.Log("Game Data Imported");
		} else {
			Debug.Log(www.error);
		}
	}
	
	
	public static WWW GetRequestStage(string method)
	{
		string currentUrl =  "https://vacinaalerta.appspot.com/_ah/api/vacinaserver/v1/"+method;
		WWW www = new WWW(currentUrl);
		return www;
	}

	[MenuItem ("MobUI/Importer/GameData(Local)")]
	static void GetGameDataLocal() 
	{
		WWW www = GetRequestLocal("compressedbackup/get");
		
		
		while (!www.isDone) {
			System.Threading.Thread.Sleep(0);
		}
		
		if (string.IsNullOrEmpty (www.error)) {
			File.WriteAllBytes("Assets/Resources/Data/GameData.bytes",Zipper.ZipData(www.text));
			Debug.Log("Game Data Imported");
		} else {
			Debug.Log(www.error);
		}
	}

	
	public static WWW GetRequestLocal(string method)
	{
		string currentUrl =  "http://localhost:8888/_ah/api/vacinaserver/v1/"+method;
		WWW www = new WWW(currentUrl);
		return www;
	}
}
