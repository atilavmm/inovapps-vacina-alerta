using UnityEngine;
using System.Collections;
using inovapps.Client;

namespace BigHut
{
	public class MobUI : MonoBehaviour 
	{
		#region Singleton
		private static bool hadPreviousInstance;
		private static MobUI instance;
		public static MobUI Instance
		{
			get 
			{
				if(instance == null && !hadPreviousInstance)
				{
					Debug.Log("B "+instance+" "+hadPreviousInstance);
					GameObject mobui = GameObject.Instantiate(Resources.Load("MobUI")) as GameObject;
					instance = mobui.GetComponent<MobUI>();
				}
				return instance;
			}
		}
		#endregion Singleton

		#region Unity Fields
		public bool TestMode;
		public string FacebookNamespace;
		public string AndroidPublicKey;
		public string GameId;
		public GameObject MoreGames;
		#endregion Unity Fields

		#region Fields
		private GameObject moreGames;
		private GameObject featured;
		private string version;
		#endregion Fields

		#region Properties
		public GameObject Featured 
		{
			get 
			{
				return featured;
			}
			set 
			{
				featured = value;
			}
		}

		public string Version
		{
			get
			{
				return version;
			}
		}
		#endregion Properties

		#region Monobehaviour
		void Awake()
		{
			if(!hadPreviousInstance)
			{
				version = VersionConfig.GetVersion();

				hadPreviousInstance = true;
				instance = this;
				DontDestroyOnLoad(this.gameObject);
			}
			else
			{
				GameObject.Destroy(gameObject);
			}
		}

		void Update()
		{
			CrossPromotionManager.Instance.Update();
		}
		
		void OnGUI()
		{

			if(Input.GetKeyDown(KeyCode.Q))
			{
				Screen.SetResolution(1080,1920, false);
			}
			else if(Input.GetKeyDown(KeyCode.W))
			{
				Screen.SetResolution(750, 1334, false);
			}
			else if(Input.GetKeyDown(KeyCode.E))
			{
				Screen.SetResolution(900, 1200, false);
			}
			else if(Input.GetKeyDown(KeyCode.R))
			{
				Screen.SetResolution(1080,1920, true);
			}
			else if(Input.GetKeyDown(KeyCode.T))
			{
				Screen.SetResolution(750, 1334, true);
			}
			else if(Input.GetKeyDown(KeyCode.Y))
			{
				Screen.SetResolution(900, 1200, true);
			}
		}
		#endregion Monobehaviour

		#region Methods
		public void ShowMoreGames()
		{
			if(moreGames == null)
			{
				moreGames = GameObject.Instantiate(MoreGames) as GameObject;
			}
		}

		public bool HasActiveUI ()
		{
			return moreGames != null || featured != null;
		}
		#endregion Methods
	}
}
