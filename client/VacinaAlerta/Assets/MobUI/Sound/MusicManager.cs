using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace BigHut
{
	public struct MusicInfo
	{
		#region Fields
		private AudioClip music;
		private bool loop;
		#endregion Fields
		
		#region Properties
		public bool Loop 
		{
			get
			{
				return this.loop;
			}
			set 
			{
				loop = value;
			}
		}

		public AudioClip Music
		{
			get
			{
				return this.music;
			}
			set
			{
				music = value;
			}
		}
		#endregion Properties
		
		#region Constructor
		public MusicInfo (AudioClip music, bool loop)
		{
			this.music = music;
			this.loop = loop;
		}
		#endregion Constructor
	}
	public class MusicManager
	{
		#region Singleton
		private static MusicManager instance;
	
		public static MusicManager Instance 
		{
			get 
			{
				if(instance == null)
				{
					instance = new MusicManager();

					GameObject musicObject = new GameObject("MusicManager");
					instance.musicAudioSource = musicObject.AddComponent<AudioSource>();
					instance.effectAudioSource = musicObject.AddComponent<AudioSource>();
					instance.musicController = musicObject.AddComponent<MusicController>();
				}
				return instance;
			}
		}
		#endregion Singleton
		
		#region Fields
		private MusicController musicController;
		private float musicVolume;
		private bool mute;
		private bool muteEffects;
		
		private DateTime lastUpdate;
		private bool isSoundActive;
		private bool isMusicActive;
		private AudioSource musicAudioSource;
		private AudioSource effectAudioSource;
		#endregion Fields
		
		#region Properties
		public MusicController MusicController 
		{
			get 
			{
				return this.musicController;
			}
			set 
			{
				musicController = value;
			}
		}
		public AudioClip CurrentMusic
		{
			get
			{
				return musicAudioSource.clip;
			}
		}

		public float MusicVolume 
		{
			get 
			{
				return this.musicVolume;
			}
			set
			{
				musicVolume = value;
			}
		}


		public bool Mute 
		{
			get
			{
				return this.mute;
			}
			set 
			{
				mute = value;
			}
		}

		public bool MuteEffects
		{
			get 
			{
				return this.muteEffects;
			}
			set 
			{
				muteEffects = value;
			}
		}		
		
		public bool IsSoundActive
		{
			get
			{
				if(lastUpdate == null || (DateTime.Now-lastUpdate).TotalSeconds >= 1.0f)
				{
					isSoundActive = PersistanceManager.Instance.GetSound();
					lastUpdate = DateTime.Now;
				}
				return isSoundActive && !MuteEffects && !Mute;
			}
		}

		public bool IsMusicActive
		{
			get
			{
				if(lastUpdate == null || (DateTime.Now-lastUpdate).TotalSeconds >= 1.0f)
				{
					isMusicActive = PersistanceManager.Instance.GetMusic();
					lastUpdate = DateTime.Now;
				}
				return isMusicActive;
			}
		}

		public AudioSource MusicAudioSource
		{
			get 
			{
				return musicAudioSource;
			}
		}

		public AudioSource EffectAudioSource 
		{
			get 
			{
				return effectAudioSource;
			}
		}
		#endregion Properties
		
		#region Constructor
		public MusicManager ()
		{
			mute = false;
			muteEffects = false;
			musicVolume = 1;
		}
		#endregion Constructor
		
		#region Methods
		public void PlayMusic(AudioClip music, bool loop)
		{
			if(MusicController != null && musicAudioSource.clip != music)
			{
				MusicController.PlayList.Clear();
				musicAudioSource.clip = music;
				musicAudioSource.loop = loop;
				musicAudioSource.Play();
				MusicController.RefreshMusic();
			}
		}
		public void PlayMusicQueued(AudioClip music, bool loop)
		{
			if(MusicController != null)
			{
				MusicController.PlayList.Add(new MusicInfo(music, loop));
			}
		}
		
		public AudioSource PlaySound(AudioClip audioClip, float volume = 1.0f)
		{
			if(GetSound())
			{
				effectAudioSource.PlayOneShot(audioClip, volume);
				if(effectAudioSource != null)
					effectAudioSource.mute = !IsSoundActive && !MuteEffects;
				return effectAudioSource;
			}
			return null;
		}

		public void FadeMusic(float volume, float duration)
		{
			MusicController.StopAllCoroutines();
			MusicController.StartCoroutine(FadeMusicCoroutine(volume, duration));
		}
		
		public bool GetSound()
		{
			return IsSoundActive && !Mute;
		}

		public void ForceUpdate ()
		{
			isSoundActive = PersistanceManager.Instance.GetSound();
			lastUpdate = DateTime.Now;
		}
		#endregion Methods

		#region Coroutine
		private IEnumerator FadeMusicCoroutine(float volume, float duration)
		{
			float initVolume = this.musicVolume;
			float initTime = Time.time;
			while(Time.time-initTime < duration)
			{
				float alpha = (Time.time-initTime)/duration;
				this.musicVolume = Mathf.Lerp(initTime, volume, alpha);
				yield return new WaitForEndOfFrame();
			}
			this.musicVolume = volume;
		}
		#endregion Coroutine

	}
}

