using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BigHut
{
	public class MusicController : MonoBehaviour
	{
		#region Fields
		private List<MusicInfo> playList;
		#endregion Fields
		
		#region Properties
		public List<MusicInfo> PlayList 
		{
			get 
			{
				return this.playList;
			}
			set 
			{
				playList = value;
			}
		}
		#endregion Properties
		
		// Use this for initialization
		#region Monobehaviour Methods
		void Awake () 
		{
			this.playList = new List<MusicInfo>();
			DontDestroyOnLoad(gameObject);
		}
		
		void OnDestroy()
		{
			this.playList.Clear();
			this.playList = null;
		}

		void Start()
		{
			RefreshMusic ();
		}

		void Update()
		{
			RefreshMusic ();
		}
		#endregion Monobehaviour Methods

		#region Methods
		public void RefreshMusic()
		{
			MusicManager.Instance.MusicAudioSource.volume = (PersistanceManager.Instance.GetMusic())?MusicManager.Instance.MusicVolume:0.0f;
			MusicManager.Instance.MusicAudioSource.mute = MusicManager.Instance.Mute;

			if(this.playList != null && this.playList.Count != 0 && MusicManager.Instance.MusicAudioSource.isPlaying)
			{
				MusicInfo currentMusic = playList[0];
				MusicManager.Instance.PlayMusic(currentMusic.Music, currentMusic.Loop);
				playList.Remove(currentMusic);
			}
		}
		#endregion Methods
	}
}
