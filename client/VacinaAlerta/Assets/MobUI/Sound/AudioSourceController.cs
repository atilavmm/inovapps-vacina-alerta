using UnityEngine;
using System.Collections;

namespace BigHut
{
	public class AudioSourceController : MonoBehaviour
	{
		private AudioSource source;

		void Awake()
		{
			source = this.GetComponent<AudioSource>();
			source.mute = !MusicManager.Instance.IsSoundActive;
		}

		void Update () 
		{
			if(Time.frameCount%5 == 0)
			{
				source.mute = !MusicManager.Instance.IsSoundActive;
			}
		}
	}
}