using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Globalization;
using System.Text;
using System.IO;
using NewtonsoftUnity.Json;
using NewtonsoftUnity.Json.Bson;

#if UNITY_WINRT
using UnityEngine.Windows;
using File = UnityEngine.Windows.File;
using Directory = UnityEngine.Windows.Directory;
#else
using System.IO;
using File = System.IO.File;
using Directory = System.IO.Directory;
#endif

namespace BigHut
{
	
	public class PersistanceManager
	{
		#region Singleton
		private static PersistanceManager instance;
	
		public static PersistanceManager Instance 
		{
			get 
			{
				if(instance == null)
				{
					instance = new PersistanceManager();
				}
				return instance;
			}
		}
		#endregion Singleton

		#region Fields
		private Dictionary<string, string> fileMap; 
		private string persistentPath;
		#endregion Fields

		#region Constructor
		public PersistanceManager ()
		{
			fileMap = new Dictionary<string, string> ();
			persistentPath = Application.persistentDataPath;
		}
		#endregion Constructor
		
        #region Methods
		public void Reset ()
		{
			PlayerPrefs.DeleteAll();
			foreach(var file in System.IO.Directory.GetFiles(Application.persistentDataPath))
			{
				System.IO.File.Delete(file);
			}
		}

		public bool Contains(string key)
        {
            return PlayerPrefs.HasKey(key);
        }
        public T GetElement<T>(string key)
        {
            return (T)Convert.ChangeType(PlayerPrefs.GetString(key).ToString(), typeof(T));
        }

		public void DeleteElement (string key)
		{
			if(Contains(key))
			{
				PlayerPrefs.DeleteKey(key);
			}
		}
		        
		public void SetElement(string key, object element, bool forceSave = false)
        {
			PlayerPrefs.SetString(key,(String)Convert.ChangeType(element,typeof(String)));
			if (forceSave)
				ForceSave ();
        }

		public void ForceSave ()
		{
			PlayerPrefs.Save ();
		}

		public bool GetMusic()
		{
			if(!Contains("music"))
				SetElement("music", true);
			
			return GetElement<bool>("music");
		}
		public void SetMusic(bool music)
		{
			SetElement("music", music);
		}	
		
		public bool GetSound()
		{
			if(!Contains("sound"))
				SetElement("sound", true);
			
			return GetElement<bool>("sound");
		}
		public void SetSound(bool music)
		{
			SetElement("sound", music);
		}

		public bool AskedNotification()
		{
			return Contains("notification");
		}

		public bool GetNotification()
		{
			if(!Contains("notification"))
				SetElement("notification", true);
			
			return GetElement<bool>("notification");
		}
		public void SetNotification(bool notification)
		{
			SetElement("notification", notification);
		}
		#endregion Methods

		#region Methods
		public bool ContainsWithFile(string file, string key = null)
		{
			if(key == null)
			{
				InitFile(file, true);
				return fileMap.ContainsKey(file);
			}
			else
			{
				InitFile(file);
				Dictionary<string, object> hash = fileMap [file].hashtableFromJson();
				return hash.ContainsKey (key);
			}
		}

		public T GetElementWithFile<T>(string file, string key = null)
		{

			if(key != null)
			{
				InitFile (file);
				Dictionary<string, object> hash = fileMap [file].hashtableFromJson();
				if(hash.ContainsKey(key))
				{
					return (T)Convert.ChangeType(hash[key], typeof(T));
				}
				return default(T);
			}
			else
			{
				InitFile (file, true);
				return (T)Convert.ChangeType(fileMap [file], typeof(T));
			}
		}
		
		public void DeleteElementWithFile (string file, string key = null, bool keepInMemory = true)
		{
			if(key == null)
			{
				DeleteFile(file);
			}
			else
			{
				InitFile (file);

				Dictionary<string, object> hash = fileMap [file].hashtableFromJson();
				if(hash.ContainsKey(key))
				{
					hash.Remove(key);
				}
				
				SaveFile (file, hash.toJson());
				RefreshMemory (file, keepInMemory);
			}
		}
		
		public void SetElementWithFile(string file, string key = null, object element = null, bool keepInMemory = true)
		{
			if(key == null)
			{
				InitFile (file, true);
				SaveFile (file, element.ToString());
				RefreshMemory (file, keepInMemory);
			}
			else
			{
				InitFile (file);
				
				Dictionary<string, object> hash = fileMap [file].hashtableFromJson();
				hash [key] = (String)Convert.ChangeType (element, typeof(String));
				
				SaveFile (file, hash.toJson());
				RefreshMemory (file, keepInMemory);
			}
		}

		public void InitFile(string file, bool directAccess = false)
		{
			if(!fileMap.ContainsKey(file))
			{
				if(directAccess)
				{
					if (HasLocalFile (file))
					{
						string data = ReadFile(file);
						fileMap [file] = data;
					}
				}
				else
				{
					if (!HasLocalFile (file))
					{
						SaveFile(file, "{}");
						fileMap [file] = "{}";
					}
					else
					{
						string data = ReadFile(file);
						fileMap [file] = data;
					}
				}
			}
		}

		public void SaveAllFiles()
		{
			foreach(var element in fileMap)
			{
				SaveFile(element.Key, element.Value);
			}
		}

		public void RefreshMemory(string file, bool keepInMemory)
		{
			if(!keepInMemory)
			{
				fileMap.Remove(file);
			}
		}
		#endregion Methods

		#region File
		public void SaveFile(string path, string content)
		{	
			#if UNITY_METRO
			string filePath = string.Format("{0}/{1}.txt", persistentPath, path);
			byte[] data = GetBytes(content);
			File.WriteAllBytes(filePath, data);
			#elif !UNITY_WEBGL || UNITY_EDITOR
			string filePath = string.Format("{0}/{1}.txt", persistentPath, path);
            PlayerPrefs.SetString(filePath, content);
			#endif
		}
		
		public bool HasLocalFile(string path)
		{
			#if !UNITY_WEBGL || UNITY_EDITOR
			string filePath = string.Format("{0}/{1}.txt", persistentPath, path);
			return PlayerPrefs.HasKey(filePath);
			#else
			return false;
			#endif
		}

		public string ReadFile(string path)
		{
			#if UNITY_METRO
			string filePath = string.Format("{0}/{1}.txt", persistentPath, path);
			byte[] data = File.ReadAllBytes(filePath);
			string text = GetString(data);
			return text;
			#elif !UNITY_WEBGL || UNITY_EDITOR
			string filePath = string.Format("{0}/{1}.txt", persistentPath, path);
			string text = PlayerPrefs.GetString(filePath);
            return text;
			#else
			return "";
			#endif
		}

		public void DeleteFile(string path)
		{
			#if !UNITY_WEBGL || UNITY_EDITOR
			string filePath = string.Format("{0}/{1}.txt", persistentPath, path);
            PlayerPrefs.DeleteKey(filePath);
			#endif
		}
		#endregion File


		#region Auxiliar Methods
		static byte[] GetBytes(string str)
		{
			byte[] bytes = new byte[str.Length * sizeof(char)];
			System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}

		static string GetString(byte[] bytes)
		{
			char[] chars = new char[bytes.Length / sizeof(char)];
			System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
			return new string(chars);
		}
		#endregion Auxiliar Methods
	}
}

