﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using BigHut;

public class DeleteAllPrefs : EditorWindow
{

    [MenuItem("MobUI/Persistence/Delete Local")]
    static void DeleteAll()
    {
		PersistanceManager.Instance.Reset ();
    }

}
