﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Prime31;

namespace BigHut
{
	public class NotificationManager
	{

		#region Singleton
		private static NotificationManager instance;
		public static NotificationManager Instance
		{
			get
			{
				if(instance == null)
					instance = new NotificationManager();
				return instance;
			}
		}
		#endregion Singleton

		#region Monobehaviour
		public void Awake()
		{
			instance = this;
		}
		#endregion Monobehaviour

		#region Methods
		public void RegisterForNotifications(bool force = false)
		{
			if(!PersistanceManager.Instance.AskedNotification() || force)
			{
#if UNITY_IOS
                UnityEngine.iOS.NotificationServices.RegisterForNotifications(
                        UnityEngine.iOS.NotificationType.Alert |
                        UnityEngine.iOS.NotificationType.Badge |
                        UnityEngine.iOS.NotificationType.Sound);
#endif
                PersistanceManager.Instance.SetNotification(true);
			}
		}

		public int ReScheduleNotification(string title,string message,DateTime date, int code)
		{
			CancelNotification(code);
			return ScheduleNotification(title, message, date, code);
		}

		public int ScheduleNotification(string title, string message, DateTime date, int code = -1)
		{
#if UNITY_IOS
			UnityEngine.iOS.LocalNotification localNotification = new UnityEngine.iOS.LocalNotification();
			localNotification.fireDate = date;
			localNotification.alertAction = title;
			localNotification.alertBody = message;
			Dictionary<string, object> data = new Dictionary<string, object> ();
			data["code"] = code.ToString();
			localNotification.userInfo = data;
			UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(localNotification);
			UnityEngine.iOS.NotificationServices.ClearLocalNotifications();
			return code;
#elif UNITY_ANDROID
			TimeSpan span = date - DateTime.Now;
			AndroidNotificationConfiguration notificationConfig = new AndroidNotificationConfiguration((long)span.TotalSeconds,title,message,message);
			return EtceteraAndroid.scheduleNotification(notificationConfig);
#else
			return 0;
#endif
		}
		
		public void CancelNotification(int code)
		{
#if UNITY_IOS
			foreach(UnityEngine.iOS.LocalNotification notification in UnityEngine.iOS.NotificationServices.scheduledLocalNotifications)
			{
				if(notification.userInfo.Contains("code") && notification.userInfo["code"].Equals(code.ToString()))
				{
					UnityEngine.iOS.NotificationServices.CancelLocalNotification(notification);
				}
			}
			foreach(UnityEngine.iOS.LocalNotification notification in UnityEngine.iOS.NotificationServices.localNotifications)
			{
				if(notification.userInfo.Contains("code") && notification.userInfo["code"].Equals(code.ToString()))
				{
					UnityEngine.iOS.NotificationServices.CancelLocalNotification(notification);
				}
			}
#elif UNITY_ANDROID
			EtceteraAndroid.cancelNotification(code);
#endif
		}

		public void CancelAll()
		{
#if UNITY_IOS
			foreach(UnityEngine.iOS.LocalNotification notification in UnityEngine.iOS.NotificationServices.scheduledLocalNotifications)
			{
				UnityEngine.iOS.NotificationServices.CancelLocalNotification(notification);
			}
			foreach(UnityEngine.iOS.LocalNotification notification in UnityEngine.iOS.NotificationServices.localNotifications)
			{
				UnityEngine.iOS.NotificationServices.CancelLocalNotification(notification);
			}
#elif UNITY_ANDROID
			EtceteraAndroid.cancelAllNotifications ();
#endif
		}

#endregion Methods

	}
}