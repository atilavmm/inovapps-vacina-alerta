using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using inovapps.Client;

namespace BigHut
{
	public class LoadingView : MonoBehaviour 
	{
		#region Unity Fields
		public bool StartActive;
		#endregion Unity Fields
		
		#region Static Fields
		private static bool initialized;
		#endregion Static Fields
		
		
		#region Fields
		private Text text;
		private Slider slider;

		private float targetValue;
		#endregion Fields

		#region Properties
		public float ProgressValue 
		{
			get
			{
				return slider.value;
			}
		}
		#endregion Properties
		
		#region Monobehaviour
		// Use this for initialization
		void Awake () 
		{
			slider = transform.Find ("Progress/Slider").GetComponent<Slider> ();

			if(!initialized)
			{
				PopupManager.Instance.LoadingInstance = this;
				text = transform.Find("Progress/Text").GetComponent<Text>();
				DontDestroyOnLoad(this);
				initialized = true;
			}
			else
			{
				gameObject.active = false;
				GameObject.Destroy(gameObject);
			}
		}

		void Start()
		{
			if(Facade.Instance.ServerController != null)
			{
				transform.Find("Game Version").GetComponent<Text>().text = "Version: "+Facade.Instance.ServerController.Type.ToString () + " " + MobUI.Instance.Version;
			}
		}

		public void OnEnable()
		{
			StartCoroutine (LoadingAnimation ());
		}
		
		public void Update()
		{
			slider.value = Mathf.MoveTowards (slider.value, targetValue, Time.deltaTime);
		}
		#endregion Monobehaviour

		#region Methods
		public void ShowBar()
		{
			slider.value = 0;
			slider.gameObject.SetActive (true);
		}

		public void SetBarProgress(float value)
		{
			targetValue = value;
		}

		public void HideBar()
		{
			slider.gameObject.SetActive (false);
		}
		#endregion Methods

		#region Loading Coroutine
		private IEnumerator LoadingAnimation()
		{
			while(true)
			{
				text.text = string.Format("{0}","Carregando");
				yield return new WaitForSeconds(0.3f);
				text.text = string.Format("{0}.","Carregando");
				yield return new WaitForSeconds(0.3f);
				text.text = string.Format("{0}..","Carregando");
				yield return new WaitForSeconds(0.3f);
				text.text = string.Format("{0}...","Carregando");
				yield return new WaitForSeconds(0.3f);
			}
		}
		#endregion Loading Coroutine
	}
}
