using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


namespace BigHut
{
	public class PopupController : MonoBehaviour 
	{
		#region Unity Fields
		public GameObject[] Popups;
		#endregion Unity Fields
		
		#region Fields
		private Dictionary<Type, GameObject> genericPopupsHash;
		#endregion Fields

		#region Properties
		public Dictionary<Type, GameObject> GenericPopupsHash 
		{
			get 
			{
				return genericPopupsHash;
			}
			set 
			{
				genericPopupsHash = value;
			}
		}
		#endregion Properties
		
		#region Monobehaviour Methods
		void Awake () 
		{
			genericPopupsHash = new Dictionary<Type, GameObject>();
			foreach(GameObject go in Popups)
			{
				if(go != null)
					genericPopupsHash.Add(go.GetComponent<GenericPopup>().GetType(), go);
			}
			PopupManager.Instance.CurrentInstance = gameObject;
		}

		void Update()
		{
			if(PopupManager.Instance.HasPopupActived && Input.GetKeyDown(KeyCode.Escape))
			{
				PopupManager.Instance.TopPopup.OnBackClick();
			}
		}
		#endregion Monobehaviour Methods
		
		#region Methods
		public T ShowPopup<T> () where T:GenericPopup
		{
			GameObject instance = GameObject.Instantiate(genericPopupsHash[typeof(T)]) as GameObject;
			instance.transform.SetParent(transform);
			instance.transform.localScale = Vector3.one;
			instance.transform.localPosition = Vector3.zero;
			
			RectTransform rectTransform = instance.GetComponent<RectTransform>();
			rectTransform.anchorMin = Vector2.zero;
			rectTransform.anchorMax = Vector2.one;
			rectTransform.anchoredPosition = Vector2.zero;
			rectTransform.sizeDelta = Vector2.zero;
			rectTransform.localRotation = Quaternion.identity;

			instance.GetComponent<Canvas> ().overrideSorting = true;
			instance.GetComponent<Canvas>().sortingOrder = PopupManager.Instance.HighestDepth+100;

			T script =  instance.GetComponent<T>();
			script.Show();
			return script;
		}
		#endregion Methods
	}
}
