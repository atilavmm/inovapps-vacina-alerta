using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace BigHut
{
	public class PopupManager
	{
		#region Singleton
		private static PopupManager instance;
		
		public static PopupManager Instance 
		{
			get 
			{
				if(instance == null)
					instance = new PopupManager();
				return instance;
			}
		}
		#endregion Singleton
		
		#region Fields
		private GameObject currentInstance;
		private LoadingView loadingInstance;
		private bool loading;
		#endregion Fields
		
		#region Properties
		public GameObject CurrentInstance
		{
			get
			{
				return this.currentInstance;
			}
			set
			{
				currentInstance = value;
			}
		}
		
		public bool HasPopupActived
		{
			get
			{
				return CurrentInstance.transform.childCount != 0;
			}
		}
		
		public int PopupCount
		{
			get
			{
				return CurrentInstance.transform.childCount;
			}
		}
		
		public int HighestDepth
		{
			get
			{
				int highestDepth = 0;
				if(CurrentInstance != null && PopupCount != 0)
				{
					GenericPopup[] popups = CurrentInstance.GetComponentsInChildren<GenericPopup>();
					highestDepth = popups.Select(o => o.GetComponent<Canvas>().sortingOrder).Max();
				}
				highestDepth = Mathf.Max(highestDepth, 1000);
				
				if(Loading)
				{
					highestDepth += 20;
				}
				return highestDepth;
			}
		}

		public GenericPopup TopPopup
		{
			get
			{
				if(CurrentInstance != null && PopupCount > 0)
				{
					int count = CurrentInstance.transform.childCount;
					Transform topPopup = CurrentInstance.transform.GetChild(count-1);
					return topPopup.GetComponent<GenericPopup>();
				}
				return null;
			}
		}
		
		public bool Loading
		{
			get
			{
				return loading;
			}
			set
			{
				loading = value;
				if(loadingInstance != null)
				{
					if(loadingInstance.gameObject.activeSelf!=value)
					{
						loadingInstance.gameObject.SetActive(value);
						if(!value)
							loadingInstance.HideBar();
					}
				}
			}
		}
		public LoadingView LoadingInstance
		{
			get 
			{
				return this.loadingInstance;
			}
			set 
			{
				loadingInstance = value;
			}
		}

		public float ProgressValue 
		{
			get
			{
				return this.loadingInstance.ProgressValue;
			}
		}
		#endregion Properties
		
		
		#region Methods		
		public T ShowPopup<T>() where T:GenericPopup
		{
			return CurrentInstance.GetComponent<PopupController>().ShowPopup<T>();
		}
		#endregion Methods

		#region Loading Methods		
		public void ShowBar()
		{
			LoadingInstance.ShowBar ();
		}
		
		public void SetBarProgress(float value)
		{
			LoadingInstance.SetBarProgress (value);
		}
		
		public void HideBar()
		{
			LoadingInstance.HideBar ();
		}

		public bool HasPopupOfType (System.Type type)
		{
			for(int i = 0; i < CurrentInstance.transform.childCount;i++)
			{
				Transform child = CurrentInstance.transform.GetChild(i);
				return child.GetComponent(type) != null;
			}
			return false;
		}
		#endregion Loading Methods
	}
}

