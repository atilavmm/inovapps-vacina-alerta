﻿using UnityEngine;
using System.Collections;
using com.bighutgames.cross.bo.game;
using System.Collections.Generic;
using NewtonsoftUnity.Json;

namespace BigHut
{
	public class CrossPromotionManager
	{
		#region Singleton
		private static CrossPromotionManager instance;
		public static CrossPromotionManager Instance 
		{
			get 
			{
				if(instance == null)
					instance = new CrossPromotionManager();
				return instance;
			}
		}
		#endregion Singleton

		#region Fields
		private List<Game> games;
		private string mail;
		private bool isProcessing = false;
		private bool hasReceivedList = false;
		private bool hasChange = false;
		private float lastTime;
		private List<CrossPromotionButton> crossPromotionButtons;
		private int lockCount;
		#endregion Fields

		#region Properties
		public string Mail 
		{
			get 
			{
				return mail;
			}
			set 
			{
				mail = value;
				PersistanceManager.Instance.SetElement("mobui.email", mail);
				hasChange = true;
			}
		}

		public List<Game> Games 
		{
			get 
			{
				return games;
			}
		}
		#endregion Properties

		#region Constructor
		public CrossPromotionManager ()
		{
			if(PersistanceManager.Instance.Contains("mobui.email"))
				mail = PersistanceManager.Instance.GetElement<string>("mobui.email");
			games = new List<Game>();
			crossPromotionButtons = new List<CrossPromotionButton>();
			lastTime = float.MinValue;
		}
		#endregion Constructor

		#region Methods
		public void Update()
		{
			if(!isProcessing && (!hasReceivedList || hasChange) && Time.time+5 > lastTime)
			{
				MobUI.Instance.StartCoroutine(LoadCrossPromotion());
				isProcessing = true;
				hasChange = false;
			}
		}

		public Game GetFeaturedGame()
		{
			if(games.Count > 0)
				return games[0];
			return null;
		}

		public void AddButton(CrossPromotionButton button)
		{
			crossPromotionButtons.Add(button);
		}

		public void RemoveButton(CrossPromotionButton button)
		{
			crossPromotionButtons.Remove(button);
		}

		public void LockButton()
		{
			lockCount = lockCount+1;
			RefreshButtons();
		}

		public void ReleaseButton()
		{
			lockCount = Mathf.Max(lockCount-1,0);
			RefreshButtons();
		}
		#endregion Methods

		#region Auxiliar Methods
		private void RefreshButtons()
		{
			foreach(CrossPromotionButton button in crossPromotionButtons)
			{
				if(button != null)
					button.gameObject.SetActive(lockCount==0);
			}
		}

		private IEnumerator LoadCrossPromotion()
		{
			string url = "https://bhoptimizads.appspot.com/_ah/api/bhCrossServer/v1/portfolio/get?";
			url += string.Format("gameId={0}", BigHut.MobUI.Instance.GameId);
			string platform = "ios";
#if UNITY_WEBPLAYER
			platform = "webplayer";
#elif UNITY_IOS
			platform = "ios";
#elif UNITY_ANDROID
			platform = "android";
#elif UNITY_WP8
			platform = "wp8";
#endif
            url += string.Format("&platform={0}", platform);
			if(!string.IsNullOrEmpty(mail))
				url += string.Format("&mail={0}", mail);


            WWW www = new WWW(url);
			yield return www;
			if(string.IsNullOrEmpty(www.error))
			{
				string data = www.text;
				Dictionary<string, object> hashInfo = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
                if (hashInfo != null && hashInfo.ContainsKey("items"))
				{
					data = JsonConvert.SerializeObject(hashInfo["items"]);
					games = new List<Game>(JsonConvert.DeserializeObject<Game[]>(data));
				}
				else
				{
					games = new List<Game>();
				}
				hasReceivedList = true;
			}
			isProcessing = false;
			lastTime = Time.time;
		}
		#endregion Auxiliar Methods
	}
}