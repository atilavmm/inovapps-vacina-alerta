﻿using UnityEngine;
using System.Collections;
using com.bighutgames.cross.bo.game;
using System.Collections.Generic;
using UnityEngine.UI;


namespace BigHut
{
	public class MoreGamesController : MonoBehaviour 
	{
		#region Unity Fields
		public GameObject FeaturedPrefab;
		public GameObject ItemPrefab;
		#endregion Unity Fields

		#region Fields
		private List<Game> games;
		#endregion Fields

		#region Monobehaviour
		void Update()
		{
			if(games == null || games.Count == 0)
			{
				games = CrossPromotionManager.Instance.Games;
				if(games.Count > 0)
				{
					StartCoroutine(LoadGames());
				}
			}
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				OnExitClick();
			}
		}
		#endregion Monobehaviour

		#region Load Games Flow
		private IEnumerator LoadGames()
		{
			RectTransform container = transform.Find("Popup/Panel/List").GetComponent<RectTransform>();
			
			for(int i = 0; i < games.Count;i++)
			{
				Game currentGame = games[i];
				if(i == 0)
					currentGame.GetBigBannerBanner();
				else
					currentGame.GetSmallBanner();
			}

            float posY = 0;
            for(int i = 0; i < games.Count;i++)
			{
				
				Game currentGame = games[i];

				Texture2D banner = null;
				while(banner == null)
				{
					banner = i==0?currentGame.GetBigBannerBanner():currentGame.GetSmallBanner();
					yield return new WaitForEndOfFrame();
				}

				GameObject element = GameObject.Instantiate(i==0?FeaturedPrefab:ItemPrefab) as GameObject;
				element.transform.SetParent(container, false);

				RectTransform rectTransform = element.GetComponent<RectTransform>();
				rectTransform.localPosition += Vector3.down*posY;
				float resize = rectTransform.rect.width/banner.width;
				rectTransform.sizeDelta = new Vector2(0,banner.height*resize);
				posY += rectTransform.rect.height+5;
				element.GetComponent<Image>().sprite = Sprite.Create(banner, new Rect(0,0, banner.width, banner.height), Vector2.one*0.5f);

				element.GetComponent<Button>().onClick.AddListener(currentGame.OpenGame);

				//Set new Container Size
				container.sizeDelta = new Vector2(0,posY);
			}

		}
		#endregion Load Games Flow

		#region Event Handlers
		public void OnExitClick()
		{
			Destroy(gameObject);
		}
		#endregion Event Handlers
	}
}