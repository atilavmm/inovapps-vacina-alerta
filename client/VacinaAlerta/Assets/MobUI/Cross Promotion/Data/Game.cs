﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace com.bighutgames.cross.bo.game
{
	public class Game 
	{
		#region Server
		public string projectCode;
		public string name;
		public string description;
		public int priority;
		
		public string iconURL;

		public string smallBannerURL;
		public string bigBannerURL;

		public string buttonURL;
		public string featureURL;
		
		public string iTunesURL;
		public string googlePlayURL;
		public string windowsPhoneURL;
		public string facebookURL;
		
		public string adsNetworkQueue;
		#endregion Server

		#region Local
		private List<string> processingURL;
		private Texture2D smallBanner;
		private Texture2D bigBanner;
		private Texture2D button;
		private Texture2D feature;
		#endregion Local

		#region Methods - Elements
		public bool IsCrossPromotionButtonReady()
		{
			return GetButton() != null && GetFeature() != null;
		}
		#endregion Methods - Elements

		#region Methods - Images
		public Texture2D GetSmallBanner()
		{
			if(smallBanner == null)
			{
				LoadTexture(
					smallBannerURL,
					delegate(Texture2D texture)
					{
						smallBanner = texture;
					}
				);
			}
			return smallBanner;
		}
		
		public Texture2D GetBigBannerBanner()
		{
			if(bigBanner == null)
			{
				LoadTexture(
					bigBannerURL,
					delegate(Texture2D texture)
					{
						bigBanner = texture;
					}
				);
			}
			return bigBanner;
		}
		
		public Texture2D GetButton()
		{
			if(button == null)
			{
				LoadTexture(
					buttonURL,
					delegate(Texture2D texture)
					{
						button = texture;
					}
				);
			}
			return button;
		}
		
		public Texture2D GetFeature()
		{
			if(feature == null)
			{
				LoadTexture(
					featureURL,
					delegate(Texture2D texture)
					{
						feature = texture;
					}
				);
			}
			return feature;
		}

		public void OpenGame ()
		{
#if UNITY_IOS
			Application.OpenURL(iTunesURL);
#elif UNITY_ANDROID
			Application.OpenURL(googlePlayURL);
#elif UNITY_WEBPLAYER
			Application.OpenURL(facebookURL);
#elif UNITY_WP8
			Application.OpenURL(windowsPhoneURL);
#endif
		}
		#endregion Methods - Images

		#region Auxiliar Methods
		private void LoadTexture(string url, Action<Texture2D> success)
		{
			if(processingURL == null)
				processingURL = new List<string>();

			if(!processingURL.Contains(url))
			{
				processingURL.Add(url);
				BigHut.MobUI.Instance.StartCoroutine(LoadTextureCoroutine(url, success));
			}
		}
		private IEnumerator LoadTextureCoroutine(string url, Action<Texture2D> success)
		{
			WWW www = new WWW(url);
			yield return www;
			if(string.IsNullOrEmpty(www.error) && www.texture != null)
			{
				success(www.texture);
			}
			processingURL.Remove(url);
		}
		#endregion Auxiliar Methods
	}
}
