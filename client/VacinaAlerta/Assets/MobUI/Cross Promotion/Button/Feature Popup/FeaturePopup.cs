﻿using UnityEngine;
using System.Collections;
using com.bighutgames.cross.bo.game;
using UnityEngine.UI;

namespace BigHut
{
	public class FeaturePopup : MonoBehaviour 
	{
		#region Fields
		private Game game;
		#endregion Fields

		#region Monobehaviour
		void Update()
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				OnExitClick();
			}
		}
		#endregion Monobehaviour

		#region Methods
		public void SetData(Game game)
		{
			Texture2D banner = game.GetFeature();
			Sprite sprite = Sprite.Create(banner, new Rect(0,0,banner.width, banner.height), Vector3.one*0.5f);
			transform.Find("Popup/Game/Stretcher/Image").GetComponent<Image>().sprite = sprite;
			this.game = game;
		}
		#endregion Methods

		#region Event Handlers
		public void OnExitClick()
		{
			Destroy(gameObject);
		}

		public void OnDownloadClick()
		{
			game.OpenGame();
			Destroy(gameObject);
		}
		#endregion Event Handlers

	}
}