﻿using UnityEngine;
using System.Collections;
using com.bighutgames.cross.bo.game;
using UnityEngine.UI;

namespace BigHut
{
	public class CrossPromotionButton : MonoBehaviour 
	{
		#region Unity Fields
		public GameObject FeaturedPopup;
		#endregion Unity Fields

		#region Fields
		private Game currentGame;
		private bool isShowing;

		private GameObject featuredPopup;
		#endregion Fields

		#region Methods
		void Awake()
		{
			CrossPromotionManager.Instance.AddButton(this);
			GetComponent<Canvas>().enabled = false;
		}

		void Update()
		{
			if(!isShowing)
			{
				if(currentGame == null)
					currentGame = CrossPromotionManager.Instance.GetFeaturedGame();
				if(currentGame != null && currentGame.IsCrossPromotionButtonReady())
				{
					GetComponent<Canvas>().enabled = true;
					Texture2D texture = currentGame.GetButton();
					transform.Find("Button").GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0,0,texture.width,texture.height), Vector2.one*0.5f);
					isShowing = true;
				}
			}
		}

		void OnDestroy()
		{
			CrossPromotionManager.Instance.RemoveButton(this);
		}
		#endregion Methods

		#region Event Handlers
		public void OnButtonClick()
		{
			if(isShowing && featuredPopup == null)
			{
				featuredPopup = GameObject.Instantiate(FeaturedPopup) as GameObject;
				featuredPopup.GetComponent<FeaturePopup>().SetData(currentGame);
				BigHut.MobUI.Instance.Featured = featuredPopup;
			}
		}
		#endregion Event Handlers
	}
}