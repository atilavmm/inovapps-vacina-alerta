﻿using UnityEngine;
using System.Collections;
using System.IO;

public class VersionConfig 
{
	private static string version;
	private static string version_code;

	public static string GetVersion()
	{
		if (version == null)
			Load ();
		return version;
	}
	
	public static string GetVersionCode()
	{
		if (version_code == null)
			Load ();
		return version_code;
	}

	private static void Load()
	{
		string text = Resources.Load<TextAsset> ("version").text;
		StringReader reader = new StringReader (text);
		version = reader.ReadLine ();
		version_code = reader.ReadLine ();
	}
}
