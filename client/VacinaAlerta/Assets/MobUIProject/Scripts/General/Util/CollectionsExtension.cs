using System;
using UnityEngine;
using System.Text;
using System.Globalization;

namespace System.Collections
{
	public static class CollectionsExtension
	{

		public static T GetElement<T>(this ICollection obj, string key)
		{
			Object currentData = obj;			
			string[] splittedKey = key.Split('.');
			StringBuilder builder = new StringBuilder();
			for(int i = 0; i < splittedKey.Length;i++)
			{
				if(builder.Length != 0)
					builder.Append(".");
				builder.Append(splittedKey[i]);
				if(currentData as IDictionary != null)
				{
					IDictionary hash = currentData as IDictionary;
					if(hash.Contains(builder.ToString()))
					{
						currentData = hash[builder.ToString()];
						builder.Remove(0,builder.Length);
					}
				}
				else if(currentData as IList != null)
				{
					currentData = (currentData as IList)[int.Parse(builder.ToString())];
					builder.Remove(0,builder.Length);
				}
			}
			if(builder.Length != 0)
			{
				throw new Exception("Element "+key+" not found");
			}
			return (T)Convert.ChangeType(currentData, typeof(T),CultureInfo.InvariantCulture);
		}
		
		public static bool IsType<T>(this ICollection obj, string key)
		{
			Object currentData = obj;			
			string[] splittedKey = key.Split('.');
			StringBuilder builder = new StringBuilder();
			for(int i = 0; i < splittedKey.Length;i++)
			{
				if(builder.Length != 0)
					builder.Append(".");
				builder.Append(splittedKey[i]);
				if(currentData as IDictionary != null)
				{
					IDictionary hash = currentData as IDictionary;
					if(hash.Contains(builder.ToString()))
					{
						currentData = hash[builder.ToString()];
						builder.Remove(0,builder.Length);
					}
				}
				else if(currentData as IList != null)
				{
					currentData = (currentData as IList)[int.Parse(builder.ToString())];
					builder.Remove(0,builder.Length);
				}
			}
			if(builder.Length != 0)
			{
				throw new Exception("Element "+key+" not found");
			}
			return currentData.GetType() == typeof(T);
		}
		
		public static bool ContainsElement(this ICollection obj, string key)
		{
			
			Object currentData = obj;
			string[] splittedKey = key.Split('.');
			StringBuilder builder = new StringBuilder();
			for(int i = 0; i < splittedKey.Length;i++)
			{
				if(builder.Length != 0)
					builder.Append(".");
				builder.Append(splittedKey[i]);
				if(currentData.GetType() == typeof(Hashtable))
				{
					Hashtable hash = currentData as Hashtable;
					if(hash.ContainsKey(builder.ToString()))
					{
						currentData = hash[builder.ToString()];
						builder.Remove(0,builder.Length);
					}
				}
				else if(currentData.GetType() == typeof(ArrayList))
				{
					currentData = (currentData as ArrayList)[int.Parse(builder.ToString())];
					builder.Remove(0,builder.Length);
				}
			}
			
			if(builder.Length != 0)
			{
				return false;
			}
			return true;
		}
		
		public static void ShuffleInPlace(this ArrayList source)
		{
			  for (int inx = source.Count-1; inx > 0; --inx)
			  {
			    int position = UnityEngine.Random.Range(0,inx);
			    object temp = source[inx];
			    source[inx] = source[position];
			    source[position] = temp;
			  }
		}
		
		
	}
}

