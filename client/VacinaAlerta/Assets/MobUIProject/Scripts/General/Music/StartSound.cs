﻿using UnityEngine;
using System.Collections;
using BigHut;

public class StartSound : MonoBehaviour {

	#region Unity Fields
	public AudioClip sfx;
	#endregion Unity Fields
	
	
	void Start () 
	{
		MusicManager.Instance.PlaySound(sfx);
	}
}
