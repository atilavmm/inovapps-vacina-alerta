﻿using UnityEngine;
using System.Collections;
using BigHut;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonSound : MonoBehaviour,  IPointerClickHandler
{
	#region Unity Fields
	public AudioClip Click;
	#endregion Unity Fields


	#region Methods
	public void OnPointerClick (PointerEventData eventData)
	{
		MusicManager.Instance.PlaySound(Click);
	}
	#endregion Methods
}
