﻿using UnityEngine;
using System.Collections;
using inovapps.Client;
using BigHut;
using System;
using inovapps.Client.Util;
using Project.Main;

namespace Project.Initialization
{
	public class InitializationController : MonoBehaviour 
	{
		#region Unity Fields
		public ServerType ServerType;
		public AudioClip Music;
		#endregion Unity Fields

		#region Fields
		private static bool isInitialized;
		private bool isPlayingMusic;
		#endregion Fields

		#region Monobehaviour
		void Awake ()
		{
			StartCoroutine (StarInitialization ());
			UBSData.Singleton.ToString();
		}

		IEnumerator StarInitialization()
		{
			yield return new WaitForEndOfFrame ();

			PopupManager.Instance.ShowBar ();
			PopupManager.Instance.SetBarProgress (0);
			
			yield return new WaitForEndOfFrame ();

			//FPS
			Application.targetFrameRate = 60;
			Screen.sleepTimeout = SleepTimeout.NeverSleep;

			//Save Format
			StartCoroutine (VerifySave ());
		}

		IEnumerator VerifySave()
		{
			yield return new WaitForEndOfFrame ();
			//Initialize Server
			Facade.Instance.Initialize(ServerType);
			Initialize();
			
			yield return new WaitForEndOfFrame ();
			NotificationManager.Instance.RegisterForNotifications ();

		}

		void Update()
		{
			if(!isPlayingMusic)
			{
				MusicManager.Instance.PlayMusic(Music, true);
				isPlayingMusic = true;
			}

			if(!PopupManager.Instance.HasPopupActived && Input.GetKeyDown(KeyCode.Escape))
			{
				Application.Quit();
			}
		}
		#endregion Monobehaviour

		#region Methods
		private void Initialize()
		{
			PopupManager.Instance.SetBarProgress (0.05f);
			PopupManager.Instance.Loading = true;
#if PREMIUM
			InitializeServer();
#else
			if(!isInitialized && (Application.isWebPlayer || Application.isMobilePlatform || Application.isEditor))
			{
				Facade.Instance.InitializeSocial(
					delegate() 
					{
						if(!isInitialized)
						{
							InitializeServer();
						}
						isInitialized = true;
					}
				);
			}
			else
			{
				InitializeServer();
				isInitialized = true;
			}
#endif
		}


		private void InitializeServer()
		{
			PopupManager.Instance.SetBarProgress (0.15f);
			Facade.Instance.Login(
				delegate 
				{
					StartFlow();
				}
				,
				delegate(ServerException error) 
				{
					StartFlow();
					#if UNITY_EDITOR
					Debug.LogError(error.Name);
					#else
					Debug.LogWarning(error.Name);
					#endif
				}
				,
				delegate(float value) 
				{
					PopupManager.Instance.SetBarProgress (Mathf.Lerp(0.15f, 0.35f, value));
				}
			);
		}

		private void StartFlow()
		{
			Debug.Log ("StartFlow");
			#if UNITY_WEBPLAYER
			Facade.Instance.LoginFlow(
				delegate 
				{ 
					StartCoroutine(
						PlayProgress(
							delegate
							{
								PopupManager.Instance.Loading = false;
								Facade.Instance.LoadSaga();
							}
						)
					);
				}
			);
			#else
			StartCoroutine(
				PlayProgress(
					delegate
					{
						Debug.Log ("Finish Play Progress");
						PopupManager.Instance.Loading = false;
#if UNITY_IOS
                       Facade.Instance.LoadMain();
#else
                        if (Facade.Instance.IsFBLogged())
                            Facade.Instance.LoadMain();
                        else
                            Facade.Instance.LoadLogin();
#endif
                    }
                )
			);
#endif
                    }
		#endregion Methods

		#region Coroutine
		private IEnumerator PlayProgress(Action onEndProgress)
		{
			Debug.Log ("Start Play Progress");
			PopupManager.Instance.SetBarProgress (1f);

			yield return new WaitForSeconds (0.6f);
			if(onEndProgress != null)
				onEndProgress();
		}
		#endregion Coroutine
	}
}