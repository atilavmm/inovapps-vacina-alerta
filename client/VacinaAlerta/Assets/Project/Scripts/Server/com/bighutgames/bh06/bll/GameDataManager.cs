using System;
using com.bighutgames.vacina.dal;
using com.bighutgames.vacina.bo;

namespace com.bighutgames.vacina.bll
{
	public class GameDataManager
	{

		private GameDataDAO dao;

		public GameDataManager ()
		{
			dao = new GameDataDAO ();
		}

		public GameData getGameData()
		{
			return dao.retrieve ();
		}

		public void setGameData (GameData data)
		{
			dao.update (data);
		}
	}
}

