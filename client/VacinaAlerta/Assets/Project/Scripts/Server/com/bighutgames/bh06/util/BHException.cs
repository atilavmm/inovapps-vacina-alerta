﻿
using System;

namespace com.bighutgames.vacina.util
{
	public class BHException : Exception {
		public BHException(string message) : base(message){}
	}
	public class InvalidArgumentException : Exception {
		public InvalidArgumentException(string message) : base(message){}
	}
	public class ContentLockedException : Exception {
		public ContentLockedException(string message) : base(message){}
	}
	public class OverloadActionException : Exception {
		public OverloadActionException(string message) : base(message){}
	}
	public class OverCapacityException : Exception {
		public OverCapacityException(string message) : base(message){}
	}
	public class NotEnoughtResourcesException : Exception {
		public NotEnoughtResourcesException(string message) : base(message){}
	}
	public class NotEmptyException : Exception {
		public NotEmptyException(string message) : base(message){}
	}
	public class SyncException : Exception {
		public SyncException(string message) : base(message){}
	}
}