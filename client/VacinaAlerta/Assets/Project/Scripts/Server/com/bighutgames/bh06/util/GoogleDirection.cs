﻿using UnityEngine;
using System.Collections;
using BestHTTP;
using System;
using System.Collections.Generic;

public class GoogleDirection : MonoBehaviour {
    
    public static Action<Direction> callBack;

    public static void GetPath(float originLat,float originLong,float destLat,float destLong, Action<Direction> delDirection)
    {
        callBack = delDirection;
        string URL = string.Format("https://maps.googleapis.com/maps/api/directions/json?origin={0},{1}&destination={2},{3}&key=AIzaSyCTE1M-rHgI5d1vqO-27NfaVVnUMER8-Sk",originLat,originLong,destLat,destLong);
        HTTPRequest request = new HTTPRequest(new Uri(URL), OnRequestFinished);
        request.Send();
    }

    static void OnRequestFinished(HTTPRequest request, HTTPResponse response)
    {
        callBack(JsonUtility.FromJson<Direction>(response.DataAsText));
    }
}

public class distance
{
    public string text;
    public int value;
}

public class duration
{
    public string text;
    public int value;
}

public class location
{
    public float lat;
    public float lng;
}

public class Step
{
    public distance distance;
    public duration duration;
    public location end_location;
    public string html_instructions;
    public location start_location;
    public string travel_mode;
}

public class Legs
{
    public distance distance;
    public duration duration;
    public location end_location;
    public string end_address;
    public string start_address;
    public location start_location;
    public List<Step> steps;
}

public class Route
{
    public List<Legs> legs;
}

public class Direction
{
    public List<Route> routes;
}
