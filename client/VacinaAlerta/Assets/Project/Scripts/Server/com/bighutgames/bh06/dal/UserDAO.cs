using System;
using UnityEngine;
using com.bighutgames.vacina.bo;
using BigHut;

namespace com.bighutgames.vacina.dal
{
	public class UserDAO
	{
		private User currentInstance = null;

		public void create(User user)
		{
			PersistanceManager.Instance.SetElementWithFile ("inovapps_user", null, user.serialize ());
			currentInstance = user;
		}

		public User retrieve()
		{
			if(currentInstance == null && PersistanceManager.Instance.ContainsWithFile("inovapps_user"))
				currentInstance = User.deserialize (PersistanceManager.Instance.GetElementWithFile<string> ("inovapps_user"));
			return currentInstance;
		}

		public void update(User user, string rawUser = null)
		{
			if(rawUser == null)
				rawUser = user.serialize();

			Debug.Log (rawUser);
			PersistanceManager.Instance.SetElementWithFile ("inovapps_user", null, rawUser);
			currentInstance = user;
		}

		public void delete()
		{
			PersistanceManager.Instance.DeleteElementWithFile ("inovapps_user");
			currentInstance = null;
		}
	}
}

