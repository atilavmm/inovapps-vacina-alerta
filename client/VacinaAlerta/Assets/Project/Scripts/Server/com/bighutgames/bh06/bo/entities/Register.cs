﻿using System.Collections.Generic;

namespace com.bighutgames.vacina.bo.entities
{
	public class Register {
		public string name;
		public long birth;
		public bool isMale;
		public Dictionary<string,List<long>> doses;
		public string id;
		public bool isPregnant;

		public Register()
		{
			this.doses = new Dictionary<string, List<long>> ();
		}

		public Register(string name, long birth, bool isMale,bool isPregnant)
		{
			this.name = name;
			this.birth = birth;
			this.isMale = isMale;
			this.isPregnant = isPregnant;
			this.doses = new Dictionary<string, List<long>> ();
		}
	}
}
