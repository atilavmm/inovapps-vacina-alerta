﻿using UnityEngine;
using System.Collections;

namespace com.bighutgames.vacina.application.util
{
	public class BHError {
		public string message;
		public ErrorType error;
		
		public enum ErrorType{
			NOT_ENOUGH_GOLD,LEVEL_BLOCKED,INVALID_LEVEL_TYPE,OTHERS
		}
	}
}
