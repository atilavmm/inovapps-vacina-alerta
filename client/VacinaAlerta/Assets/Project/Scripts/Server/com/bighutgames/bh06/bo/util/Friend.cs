﻿using UnityEngine;
using System.Collections;

namespace com.bighutgames.vacina.bo.util
{
	public class Friend {
		public string name;
		public string fbId;
		public int score;
		public int lastLevel;
		public string url;
	}
}
