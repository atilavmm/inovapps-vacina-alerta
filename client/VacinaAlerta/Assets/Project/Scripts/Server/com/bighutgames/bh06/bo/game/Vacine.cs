﻿using UnityEngine;
using System.Collections;
using System;

namespace com.bighutgames.vacina.bo.game
{
	public class Vacine : Metadata {

		public String name;
		public String description;
		public String prevents;
		public String indication;
		public String counterIndication;
		public String adverseEffect;

		public bool male;
		public bool female;

		public String fixedDosage;
		public int periodicDosage;
		public int start;
		public int end;

		public bool pregnantAvailable;

		public bool HasMultipleDosages
		{
			get 
			{
				return periodicDosage != 0 || !string.IsNullOrEmpty (fixedDosage);
			}
		}
	}
}
