using System;
using com.bighutgames.vacina.dal;
using com.bighutgames.vacina.bo;
using com.bighutgames.vacina.bo.util;
using System.Collections;
using com.bighutgames.vacina.bo.entities;
using System.Collections.Generic;
using UnityEngine;
using com.bighutgames.vacina.util;
using com.bighutgames.vacina.application.util;
using com.bighutgames.vacina.bo.game;
using NewtonsoftUnity.Json;
using inovapps.Client;

namespace com.bighutgames.vacina.bll
{
	public class UserManager
	{

		private UserDAO dao;
		private GameDataManager gameManager;

		public UserManager (GameDataManager gameManager)
		{
			dao = new UserDAO ();
			this.gameManager = gameManager;
		}

		public void setUser(User user, string rawUser = null)
		{
			dao.update (user, rawUser);
		}

		public User getUser()
		{
			return dao.retrieve ();
		}

		public void SaveUser()
		{
			User user = getUser ();
			setUser (user);
		}

		public User createNewUser(long currentTime)
		{
			User user = new User();
			user.Id = -1;
			user.startTimeUTC =  currentTime;
			user.data = new Data();

			return user;
			
		}

		public Dictionary<string,object> executeAction(ActionData action)
		{
			User user = dao.retrieve ();
			user.gameplayTime += action.gameplayGapTime;

			Dictionary<string, object> data = null;

			switch ((ActionType)Enum.Parse(typeof(ActionType),action.type))
			{
			case ActionType.DELETE_MEMBER:
				data = executeActionDELETE_MEMBER(user,action);
				break;
			case ActionType.ADD_MEMBER:
				data = executeActionADD_MEMBER (user, action);
				break;
			case ActionType.UPDATE_MEMBER:
				data = executeActionUPDATE_MEMBER (user, action);
				break;
			case ActionType.VACINE:
				data = executeActionVACINE (user, action);
				break;
			default:
				throw new InvalidArgumentException("Invalid Argument: ActionType "+ action.type);
			}

			return data;
		}

		Dictionary<string,object> executeActionDELETE_MEMBER(User user,ActionData action)
		{
			Dictionary<string,object> response = new Dictionary<string,object>();
			Hashtable map = JsonConvert.DeserializeObject<Hashtable>(action.data);

			String id = map ["id"].ToString ();

			user.data.remove (id);

			return response;
		}

		Dictionary<string,object> executeActionADD_MEMBER(User user,ActionData action)
		{
			Dictionary<string,object> response = new Dictionary<string,object>();
			Hashtable map = JsonConvert.DeserializeObject<Hashtable>(action.data);

			String name = map ["name"].ToString ();
			bool isMale = map ["isMale"].ToString ().ToLower ().Equals ("true");
			bool isPregnant = map ["isPregnant"].ToString ().ToLower ().Equals ("true");
			long birth = long.Parse(map ["birth"].ToString ());

			user.data.register (name, birth, isMale,isPregnant);

			return response;
		}

		Dictionary<string,object> executeActionUPDATE_MEMBER (User user,ActionData action)
		{
			Dictionary<string,object> response = new Dictionary<string,object>();
			Hashtable map = JsonConvert.DeserializeObject<Hashtable>(action.data);

			String name = map ["name"].ToString ();
			bool isMale = map ["isMale"].ToString ().ToLower ().Equals ("true");
			bool isPregnant = map ["isPregnant"].ToString ().ToLower ().Equals ("true");
			long birth = long.Parse(map ["birth"].ToString ());
			String id = map ["id"].ToString ();

			Register r = user.data.Registers [id];
			r.name = name;
			r.isMale = isMale;
			r.birth = birth;
			r.isPregnant = isPregnant;

			return response;
		}

		Dictionary<string,object> executeActionVACINE (User user,ActionData action)
		{
			Dictionary<string,object> response = new Dictionary<string,object>();
			Hashtable map = JsonConvert.DeserializeObject<Hashtable>(action.data);

			String id = map ["id"].ToString ();
			String vacineId = map ["vacineId"].ToString ();

			if (!user.data.Registers [id].doses.ContainsKey (vacineId))
				user.data.Registers [id].doses [vacineId] = new List<long> ();
			user.data.Registers [id].doses [vacineId].Add(action.time);

			return response;
		}
	}
}

