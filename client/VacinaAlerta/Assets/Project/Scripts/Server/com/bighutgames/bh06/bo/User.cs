// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using com.bighutgames.vacina.bo.entities;
using System.Collections.Generic;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using NewtonsoftUnity.Json;


namespace com.bighutgames.vacina.bo
{
	public class User
	{
		
		public long Id;
		public string facebookId;
		public long bighutId;
		public Data data;
		
		public long startTimeUTC;
		public long gameplayTime;

		public User ()
		{
		}
		
		public static User deserialize(string json)
		{
			Dictionary<string, Object> rawData = JsonConvert.DeserializeObject<Dictionary<string, Object>>(json);
			User user = JsonConvert.DeserializeObject<User>(json);
			return user;
			
		}
		
		public string serialize() 
		{
			String json = JsonConvert.SerializeObject(this);
			Dictionary<string, Object> rawData = JsonConvert.DeserializeObject<Dictionary<string, Object>>(json);
			return JsonConvert.SerializeObject(rawData);
		}

		public string ToString()
		{
			StringBuilder buffer = new StringBuilder();
			buffer.Append("facebookId: ");
			buffer.Append(this.facebookId);
			buffer.Append(", Id: ");
			buffer.Append(this.Id);
			buffer.Append(", bighutId: ");
			buffer.Append(this.bighutId);
			buffer.Append(", data: [");
			buffer.Append(this.data.ToString());
			buffer.Append("]");
			return buffer.ToString().ToUpper();
		}

		public string getCheckSum()
		{
			string raw = this.ToString ();
			byte[] ByteData = ASCIIEncoding.ASCII.GetBytes (raw.ToCharArray (), 0, raw.Length);
			MD5 oMd5 = MD5.Create();
			byte[] HashData = oMd5.ComputeHash(ByteData);
			StringBuilder oSb = new StringBuilder();
			for (int x = 0; x < HashData.Length; x++)
			{
				oSb.Append(HashData[x].ToString("x2"));
			}
			return oSb.ToString();
		}
	}
}

