﻿using System;
using BigHut;
using UnityEngine;

namespace com.bighutgames.vacina.application.util
{
	public class TimeConverter
	{

		#region Fields
		private TimeSpan timeSpan;
		private DateTime serverTime;
		#endregion Fields

		#region Constructor
		public TimeConverter()
		{
			long lastServerTime = 0;
            long lastClientTime = DateTime.UtcNow.ToMilliseconds();
			if(PersistanceManager.Instance.Contains("mobui.serverTime"))
			{
                lastServerTime = PersistanceManager.Instance.GetElement<long>("mobui.serverTime");
                lastClientTime = PersistanceManager.Instance.GetElement<long>("mobui.clientTime");
			}
            Refresh(lastServerTime, lastClientTime);
		}
		#endregion Constructor

		#region Methods
		public void Refresh(long serverTime, long lastClientTime)
		{
			this.serverTime = serverTime.FromMilliseconds();
            DateTime clientTime = lastClientTime.FromMilliseconds();
            this.timeSpan = this.serverTime - clientTime;
            PersistanceManager.Instance.SetElement("mobui.serverTime", serverTime);
            PersistanceManager.Instance.SetElement("mobui.clientTime", lastClientTime);
		}

        public void Refresh(long serverTime)
        {
			Refresh(serverTime, DateTime.UtcNow.ToMilliseconds());
        }
		#endregion Methods

		#region Properties
		public long ServerTime
		{
			get
			{
				return (DateTime.UtcNow + timeSpan).ToMilliseconds();
			}
		}
		#endregion Properties

		#region Util

		public static int timeSpanInSeconds(long time1,long time2)
		{
			return (int)((time1-time2)/1000);
		}
		
		
		public static string TimeSpanToStringFirstDigit(TimeSpan time)
		{
			if(time.TotalDays >= 1f)
				return string.Format("{0}"+"general.short.days".Localize(),(int)time.TotalDays);
			else if(time.TotalHours >= 1f)
				return string.Format("{0}"+"general.short.hours".Localize(),(int)time.TotalHours);
			else if(time.TotalMinutes>= 1f)
				return string.Format("{0}"+"general.short.minutes".Localize(),(int)time.TotalMinutes);
			else
				return string.Format("{0}"+"general.short.seconds".Localize(),time.Seconds);;
		}

		public static string TimeSpanToString(TimeSpan time)
		{
			if(time.TotalDays >= 1f)
				return string.Format(
					"{0}"+"general.short.days".Localize()+" {1}"+"general.short.hours".Localize()+" {2}"+"general.short.minutes".Localize()+" {3}"+"general.short.seconds".Localize(),
					(long)time.TotalDays, 
					time.Hours, 
					time.Minutes, 
					time.Seconds);
			else if(time.Hours>0)
				return string.Format(
					"{0}"+"general.short.hours".Localize()+" {1}"+"general.short.minutes".Localize()+" {2}"+"general.short.seconds".Localize(),
					time.Hours, 
					time.Minutes, 
					time.Seconds);
			else if(time.Minutes>0)
				return string.Format(
					"{0}"+"general.short.minutes".Localize()+" {1}"+"general.short.seconds".Localize(),
					time.Minutes, 
					time.Seconds);
			else
				return string.Format("{0}"+"general.short.seconds".Localize(), 
					time.Seconds);
		}

		public static string TimeSpanToStringTwoDot(TimeSpan time)
		{
			int totalHour = (int)time.TotalHours;
			if(totalHour>= 0)
				return string.Format("{0:00}:{1:00}:{2:00}",Mathf.Min(99,totalHour),time.Minutes,time.Seconds);
			else if(time.Minutes>0)
				return string.Format("{0:00}:{1:00}",time.Minutes,time.Seconds); 
			else
				return string.Format("{0:00}",time.Seconds);;
		}
		#endregion Util


	}

	public static class DateTimeHelper
	{
		public static long ToMilliseconds(this DateTime obj)
		{
			return (long)(obj-new DateTime(1970,1,1)).TotalMilliseconds;
		}

		public static DateTime FromMilliseconds(this long milliseconds)
		{
			return new DateTime(1970,1,1).AddMilliseconds(milliseconds);
		}

		public static DateTime UTCNow()
		{
			return DateTime.UtcNow.ToMilliseconds().FromMilliseconds();
		}
	}
}