﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace com.bighutgames.vacina.bo
{
	public class Backup {
		public string metaGameData;
		
		public GameData getData(){
			return  GameData.deserialize (metaGameData);
		}
	}
}
