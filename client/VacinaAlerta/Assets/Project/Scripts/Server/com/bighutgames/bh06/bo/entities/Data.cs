// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using com.bighutgames.vacina.bo.util;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Linq;
using NewtonsoftUnity.Json;


namespace com.bighutgames.vacina.bo.entities
{
	public class Data
	{	

		public int currentRegister;
		public Dictionary<string,Register> Registers;

		public Data ()
		{
			this.currentRegister = 0;
			this.Registers = new Dictionary<string, Register> ();
		}

		public void register(string name, long birth, bool isMale,bool isPregnant)
		{
			currentRegister++;
			Register element = new Register (name, birth, isMale, isPregnant);
			this.Registers.Add (currentRegister + "", element);
		}

		public void remove(string id)
		{
			this.Registers.Remove (id);
		}

		public static Data deserialize(string json)
		{
			Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
			Data data = JsonConvert.DeserializeObject<Data>(json);
			return data;
		}
		
		public string serialize()
		{
			return JsonConvert.SerializeObject(this.SerializeHash());
		}
		
		public Dictionary<string, object> SerializeHash()
		{
			String json = JsonConvert.SerializeObject(this);
			Dictionary<string, object> rawData =  JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
			return rawData;
		}

		public string ToString()
		{
			StringBuilder buffer = new StringBuilder();
			return buffer.ToString();
		}
	}
}

