using System;
using com.bighutgames.vacina.bo;

namespace com.bighutgames.vacina.application.util
{
	public class LoginReturn
	{
		public string user;
		public string data;

		public LoginReturn (){
		}

		public LoginReturn(User user,GameData data) {
			this.user = user.serialize();
			this.data = data.serialize();
		}

		public User getUser(){
			return User.deserialize(this.user);
		}

		public GameData getData(){
			if(string.IsNullOrEmpty(data))
				return null;
			return  GameData.deserialize (data);
		}
	}
}

