using System;
using com.bighutgames.vacina.bo;
using UnityEngine;
using NewtonsoftUnity.Json;
using BigHut;

namespace com.bighutgames.vacina.dal
{
	public class GameDataDAO
	{

		private GameData currentInstance = null;


		public void create(GameData gameData)
		{
			PersistanceManager.Instance.SetElementWithFile ("inovapps_gameData", null, JsonConvert.SerializeObject(gameData));
			currentInstance = gameData;
		}

		public GameData retrieve()
		{
			if (currentInstance == null && PersistanceManager.Instance.ContainsWithFile("inovapps_gameData")) {
				UnityEngine.Debug.Log ("Retrive load");
				currentInstance = GameData.deserialize (PersistanceManager.Instance.GetElementWithFile<string> ("inovapps_gameData"));
			}
			return currentInstance;
		}

		public void update(GameData gameData)
		{
			PersistanceManager.Instance.SetElementWithFile("inovapps_gameData", null, gameData.serialize());
			currentInstance = gameData;
		}

		public void delete()
		{
			PersistanceManager.Instance.DeleteElementWithFile ("inovapps_gameData");
			currentInstance = null;
		}

	}
}

