﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

namespace BigHut.inovapps.Transition
{
	public class TransitionView : MonoBehaviour 
	{
		#region Unity Fields
		public bool StartActive;
		#endregion Unity Fields
		
		#region Singleton
		private static TransitionView instance;
		public static TransitionView Instance
		{
			get
			{
				return instance;
			}
		}
		#endregion Singleton
		
		
		#region Fields
		private Graphic[] graphics;
		
		private List<Tweener> tweeners;
		#endregion Fields
		
		#region Monobehaviour
		// Use this for initialization
		void Awake () 
		{
			if(instance == null)
			{
				instance = this;
				tweeners = new List<Tweener>();
				graphics = GetComponentsInChildren<Graphic>();
				
				DontDestroyOnLoad(this);
			}
			else
			{
				gameObject.active = false;
				GameObject.Destroy(gameObject);
			}
			gameObject.SetActive(StartActive);
		}
		#endregion Monobehaviour
		
		#region Static Methods
		public static void Show(string value = "", float duration = 0.3f)
		{
			instance.Reset();
			foreach (Graphic g in instance.graphics)
				g.color = new Color (g.color.r, g.color.g, g.color.b, 0f);
			instance.gameObject.SetActive(true);
			instance.StartCoroutine(instance.ShowCoroutine(value, duration));
		}
		
		public static void Hide(float duration = 0.3f)
		{
			instance.Reset();
			instance.StartCoroutine(instance.HideCoroutine(duration));
		}
		
		public static void PlayTransition(string value, float intro, IEnumerator method, float end)
		{
			instance.Reset();
			foreach (Graphic g in instance.graphics)
				g.color = new Color (g.color.r, g.color.g, g.color.b, 0f);
			instance.gameObject.SetActive(true);
			instance.StartCoroutine(instance.PlayTransitionCoroutine(value, intro, method, end));
		}
		
		public static void PlaySceneTransition(string scene, string value = "")
		{
			Transition.TransitionView.PlayTransition(value, 0.25f, LoadScene(scene), 0.25f);
		}
		public static void PlayTransition(string value, float intro, string scene, float end)
		{
			instance.Reset();
			foreach (var g in instance.graphics)
				g.color = new Color (g.color.r, g.color.g, g.color.b, 0f);
			instance.gameObject.SetActive(true);
			instance.StartCoroutine(instance.PlayTransitionCoroutine(value, intro, LoadScene(scene), end));
		}
		
		private static IEnumerator LoadScene(string scene)
		{
			AsyncOperation asyncOperation = Application.LoadLevelAsync(scene);
			yield return asyncOperation;
		}
		#endregion Static Methods
		
		
		#region Auxiliar Methods
		private void Reset()
		{
			foreach(Tweener tweener in tweeners)
				tweener.Kill();
			instance.StopAllCoroutines();
		}
		
		private IEnumerator ShowCoroutine(string value = "", float duration = 0.3f)
		{
			
			foreach (var g in graphics)
				g.DOFade (1,duration);
			
			yield return new WaitForSeconds(duration);
		}
		
		private IEnumerator HideCoroutine(float duration = 0.3f)
		{
			foreach (var g in graphics)
				g.DOFade (0,duration);
			yield return new WaitForSeconds(duration);
			instance.gameObject.SetActive(false);
		}
		
		private IEnumerator PlayTransitionCoroutine(string value, float intro, IEnumerator method, float end)
		{
			yield return StartCoroutine(ShowCoroutine(value, intro));
			yield return StartCoroutine(method);
			yield return StartCoroutine(HideCoroutine(end));
		}
		#endregion Auxiliar Methods
	}
}
