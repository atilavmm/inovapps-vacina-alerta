﻿using UnityEngine;
using System.Collections;
using inovapps.Client;
using BigHut;
using System;
using inovapps.Client.Util;
using UnityEngine.UI;
using com.bighutgames.vacina.bo.entities;
using com.bighutgames.vacina.application.util;
using com.bighutgames.vacina.bo.game;

namespace Project.Main
{
	public class EditProfileController : MonoBehaviour 
	{
        #region Unity Fields
        public GameObject VacinePrefab;
        #endregion Unity Fields

        #region Fields
        private Register register;

		private InputField nameField;
		private InputField dayField;
		private InputField monthField;
		private InputField yearField;

		private Toggle maleToggle;
		private Toggle femaleToggle;

		private Toggle pregnantCheck;

		private bool isNew = false;

        private GameObject VacineList;
		#endregion Fields


		#region Methods
		public void SetData(Register register)
		{
			isNew = false;
			if (register == null)
			{
				register = new Register ("", DateTime.Now.ToMilliseconds(), true,false);
				isNew = true;
			}
			
			this.register = register;

			nameField = transform.Find ("Main/Content/Name/InputField").GetComponent<InputField> ();
			dayField = transform.Find ("Main/Content/Birthday/Day").GetComponent<InputField> ();
			monthField = transform.Find ("Main/Content/Birthday/Month").GetComponent<InputField> ();
			yearField = transform.Find ("Main/Content/Birthday/Year").GetComponent<InputField> ();

			maleToggle = transform.Find ("Main/Content/Sex/Male").GetComponent<Toggle> ();
			femaleToggle = transform.Find ("Main/Content/Sex/Female").GetComponent<Toggle> ();

			pregnantCheck = transform.Find ("Main/Content/Pregnant/Checkbox").GetComponent<Toggle> ();

			
			nameField.text = this.register.name;
			DateTime date = this.register.birth.FromMilliseconds();

			dayField.text = string.Format("{0}",date.Day);
			monthField.text = string.Format("{0}",date.Month);
			yearField.text = string.Format("{0}",date.Year);

			maleToggle.onValueChanged.RemoveAllListeners();
			femaleToggle.onValueChanged.RemoveAllListeners();

			maleToggle.isOn = this.register.isMale;
			femaleToggle.isOn = !this.register.isMale;

			pregnantCheck.isOn = this.register.isPregnant;

			transform.Find ("Main/Content/Remove").gameObject.SetActive(!isNew);

			maleToggle.onValueChanged.AddListener(OnMaleClick);
			femaleToggle.onValueChanged.AddListener(OnFemaleClick);


            //Vaccinas
            transform.Find("Main/Vaccines").gameObject.SetActive(!isNew);
            Transform scrollParent = transform.Find("Main/Vaccines/Pending/Content").transform;

            for (int i = 0; i < scrollParent.childCount; i++)
                GameObject.Destroy(scrollParent.GetChild(i).gameObject);


            foreach(var element in register.doses)
            {
                Vacine vacine = Facade.Instance.GetVacines()[element.Key];
                int doseCount = 0;
                foreach (var dateElement in element.Value)
                {
                    GameObject vacineGO = GameObject.Instantiate(VacinePrefab) as GameObject;
                    vacineGO.transform.SetParent(scrollParent);
                    vacineGO.transform.localScale = Vector3.one;
                    vacineGO.transform.localPosition = Vector3.zero;

                    string title = string.Format("{0}", vacine.name);
                    if (vacine.HasMultipleDosages)
                        title = string.Format("{0}º dosagem da {1}", ++doseCount, vacine.name);


                    vacineGO.transform.Find("Vacine").GetComponent<Text>().text = title;
                    vacineGO.transform.Find("Date").GetComponent<Text>().text = dateElement.FromMilliseconds().ToString(@"dd\/ MM\/ yyyy");
                }
            }
        }
		#endregion Methods

		#region Event Handlers
		public void OnMaleClick(bool status)
		{
			if(femaleToggle.isOn != !status)
				femaleToggle.isOn = !status;
		}
		public void OnFemaleClick(bool status)
		{
			if(maleToggle.isOn != !status)
				maleToggle.isOn = !status;
		}

		public void OnEditDay()
		{
			int day = int.Parse (dayField.text);
			day = Mathf.Clamp (day, 1, 31);
			dayField.text = day.ToString();
		}

		public void OnEditMonth()
		{
			int month = int.Parse (monthField.text);
			month = Mathf.Clamp (month, 1, 12);
			monthField.text = month.ToString();
		}

		public void OnEditYear()
		{
			int year = int.Parse (yearField.text);
			year = Mathf.Clamp (year, 1900, DateTime.Now.Year);
			yearField.text = year.ToString();
		}


		public void OnConfirm()
		{
			int day = int.Parse (dayField.text);
			int month = int.Parse (monthField.text);
			int year = int.Parse (yearField.text);

			//Todo Confirm
			if (isNew) 
			{
				Facade.Instance.ExecuteActionAddMember (
					nameField.text,
					maleToggle.isOn,
					new DateTime (year, month, day).ToMilliseconds(),
					pregnantCheck.isOn
				);
			}
			else 
			{
				Facade.Instance.ExecuteActionUpdateMember (
					register.id,
					nameField.text,
					maleToggle.isOn,
					pregnantCheck.isOn,
					new DateTime (year, month, day).ToMilliseconds());
			}
			MainControlller.Instance.OnProfileClick();
		}

		public void OnPregnantClick()
		{
			pregnantCheck.isOn = !pregnantCheck.isOn;
		}

		public void OnRemove()
		{
			Facade.Instance.ExecuteActionDeleteMember (register.id);
			//Todo Remove
			MainControlller.Instance.OnProfileClick();
		}
		#endregion Event Handlers
	}
}
