﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using inovapps.Client;
using BigHut;
using Project.Popup;
using System;
using com.bighutgames.vacina.application.util;

namespace Project.Main
{
	public class NotificationElement : MonoBehaviour
    {
		#region Fields
		private string title;
		private string vaccine;
		private string pacient;

        private Toggle toggle;

        private Action refresh;
        private Action lockProcess;
        private Action unlockProcess;

        private bool processingInput;
        #endregion Fields

        #region Properties
        public string Title
        {
            get
            {
                return title;
            }

            set
            {
                title = value;
            }
        }
        #endregion Properties

        #region Methods
        public void SetData(string title, string pacient, string vaccine, Action refresh, Action lockProcess, Action unlockProcess)
		{
			this.Title = title;
			this.vaccine = vaccine;
			this.pacient = pacient;
            this.toggle = GetComponentInChildren<Toggle>();
            this.refresh = refresh;
            this.lockProcess = lockProcess;
            this.unlockProcess = unlockProcess;

            Text text = transform.Find("Toggle/Text").GetComponent<Text>();
            text.text = title;

            processingInput = false;

        }
		#endregion Methods

		#region Event Handlers
		public void OnClick ()
		{
            if(!processingInput)
            {
                processingInput = true;
                this.toggle.isOn = true;
                this.lockProcess();
                PopupManager.Instance.ShowPopup<ConfirmVacine>().SetConfirmation(
                    delegate (DateTime date)
                    {
                        this.unlockProcess();
                        GameObject.Destroy(this.gameObject);
                        Facade.Instance.ExecuteActionVacine(pacient, vaccine, date.ToMilliseconds());
                        processingInput = false;
                        refresh();
                    },
                    delegate
                    {
                        this.unlockProcess();
                        this.toggle.isOn = false;
                        processingInput = false;

                    }
                );
            }
		}
        #endregion Event Handlers
    }
}