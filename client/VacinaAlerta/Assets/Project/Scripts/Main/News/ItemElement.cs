﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Project.Main
{
	public class ItemElement : MonoBehaviour
	{
		#region Fields
		private string title;
		private string url;
		#endregion Fields

		#region Methods
		public void SetData(string title, string url)
		{
			this.title = title;
			this.url = url;

			transform.Find ("Text").GetComponent<Text> ().text = title;
		}
		#endregion Methods

		#region Event Handlers
		public void OnClickRead ()
		{
			Application.OpenURL (this.url);
		}

        public void OnClickShare()
        {
            /*
            Facebook.Unity.FB.FeedShare(
                link: new System.Uri(this.url),
                linkName: this.title,
                linkCaption: this.title,
                linkDescription: this.title,
                callback: delegate { }
            );*/
        }
		#endregion Event Handlers
	}
}