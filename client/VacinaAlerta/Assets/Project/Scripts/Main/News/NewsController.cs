﻿using UnityEngine;
using System.Collections;
using BestHTTP;
using System.Xml;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using inovapps.Client;
using com.bighutgames.vacina.bo.game;
using System;
using com.bighutgames.vacina.bo.entities;
using System.Collections.Generic;

namespace Project.Main
{
	public class NewsController : MonoBehaviour
	{
		#region Unity Fields
		public GameObject NewsPrefab;
		public GameObject NotificationPrefab;
        private DateTime lastTime;
        private int processingCount;

        private List<NotificationElement> notifications;
        #endregion Unity Fields

        #region Methods
        public void SetData()
		{
			foreach (ItemElement element in gameObject.GetComponentsInChildren<ItemElement>())
				GameObject.Destroy (element.gameObject);
			foreach (NotificationElement element in gameObject.GetComponentsInChildren<NotificationElement>())
				GameObject.Destroy (element.gameObject);

            notifications = new List<NotificationElement>();
            processingCount = 0;


            StartCoroutine(SetNews ());
			SetPendencies ();
        }

        public void Update()
        {
            if((DateTime.Now- lastTime).TotalSeconds > 50 && processingCount == 0)
            {
                SetPendencies();
                lastTime = DateTime.Now;
            }
        }
		#endregion Methods

		#region Auxiliar Methods
		private IEnumerator SetNews()
		{
			HTTPRequest request = new HTTPRequest (new System.Uri("http://portalsaude.saude.gov.br/index.php/cidadao/principal/agencia-saude?format=feed&type=rss"));
			request.Send();
			yield return StartCoroutine (request);
			if (request.State == HTTPRequestStates.Finished && request.Response.IsSuccess) 
			{
				string text = request.Response.DataAsText;
				XmlDocument document = new XmlDocument ();
				document.LoadXml (text);

				XmlNode element = document.LastChild;
				if (element.Name == "rss") 
				{
					element = element.FirstChild;
					if (element.Name == "channel") 
					{
						string name = element.FirstChild.InnerText;

						XmlNodeList children = element.ChildNodes;
						for (int i = 0; i < children.Count; i++)
						{
							XmlNode item = children.Item (i);
							if (item.Name == "item")
							{
								XmlNodeList itemChildren = item.ChildNodes;
								string title = itemChildren [0].InnerText.Replace (name, "");
								string url = itemChildren [1].InnerText.Replace (name, "");

								GameObject newsElement = GameObject.Instantiate (NewsPrefab) as GameObject;
								newsElement.transform.SetParent (transform.Find ("Main/News/Content"));
								newsElement.transform.localScale = Vector3.one;

								newsElement.GetComponent<ItemElement> ().SetData (title, url);
							}
						}
					}
				}
			}
		}

		private void SetPendencies()
		{
			if (Facade.Instance.GetUser ().data.Registers.Count == 0) {
				transform.Find ("Main/Pending/Status").GetComponent<Text> ().text = "Ninguém cadastrado, clique no botão de PERFIL abaixo a esquerda.";
			}
			else {
				transform.Find ("Main/Pending/Status").GetComponent<Text>().text = "";

                List<NotificationElement> markedElements = new List<NotificationElement>();
				foreach (Register register in Facade.Instance.GetRegisters()) 
				{
					foreach(Vacine vacine in Facade.Instance.GetRegisterPendencies(register.id))
					{

                        string title = string.Format("{0} tomou a {1}", register.name, vacine.name);
                        if (vacine.HasMultipleDosages)
                            title = string.Format("{0} tomou a {1}º dosagem da {2}", register.name, Facade.Instance.GetCurrentDosage(register.id, vacine.IndexID) + 1, vacine.name);

                        NotificationElement notification = null;
                        if (!notifications.Find(o => o.Title == title))
                        {
                            GameObject notificationElement = GameObject.Instantiate(NotificationPrefab) as GameObject;
                            notificationElement.transform.SetParent(transform.Find("Main/Pending/Content"));
                            notificationElement.transform.localScale = Vector3.one;

                            notification = notificationElement.GetComponent<NotificationElement>();
                            notification.SetData(title, register.id, vacine.IndexID, SetPendencies, LockProcess, UnlockProcess);
                            notifications.Add(notification);
                        }
                        else
                        {
                            notification = notifications.Find(o => o.Title == title);
                        }

                        if(notification != null)
                            markedElements.Add(notification);
                    }
				}

                //Remove old
                List<NotificationElement> toRemove = new List<NotificationElement>();
                foreach (NotificationElement element in notifications)
                {
                    if (!markedElements.Contains(element))
                    {
                        toRemove.Add(element);
                    }
                }
                foreach (NotificationElement element in toRemove)
                {
                    GameObject.Destroy(element);
                    notifications.Remove(element);
                }

                if (transform.Find ("Main/Pending/Content").childCount == 0)
					transform.Find ("Main/Pending/Status").GetComponent<Text>().text = "Não há nenhuma pendência";
			}

		}

        private void LockProcess()
        {
            this.processingCount++;
        }

        private void UnlockProcess()
        {
            this.processingCount--;
        }
		#endregion Auxiliar Methods
	}
}