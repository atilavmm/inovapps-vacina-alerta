﻿using UnityEngine;
using System.Collections;
using BestHTTP;
using System.Xml;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using inovapps.Client;
using com.bighutgames.vacina.bo.entities;


namespace Project.Main
{
	public class ProfileController : MonoBehaviour
	{
		#region Unity Fields
		public GameObject ProfileItemPrefab;
		#endregion Unity Fields

		#region Methods
		public void SetData()
		{
			foreach (ProfileElement element in gameObject.GetComponentsInChildren<ProfileElement>())
				GameObject.Destroy (element.gameObject);

			StartCoroutine(SetProfiles ());
		}
		#endregion Methods

		#region Auxiliar Methods
		private IEnumerator SetProfiles()
		{
			

			//Integrate with Server
			foreach (Register r in Facade.Instance.GetRegisters()) 
			{

				GameObject notificationElement = GameObject.Instantiate (ProfileItemPrefab) as GameObject;
				notificationElement.transform.SetParent (transform.Find ("Main/Profiles/Content"));
				notificationElement.transform.localScale = Vector3.one;

				notificationElement.GetComponent<ProfileElement> ().SetData (r);
			}
			yield return null;
		}
		#endregion Auxiliar Methods

		#region Event Handlers
		public void OnProfileClick()
		{
			MainControlller.Instance.CreateEditProfile (null);
		}
		#endregion Event Handlers
	}
}