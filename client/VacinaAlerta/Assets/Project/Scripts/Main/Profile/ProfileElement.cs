﻿using UnityEngine;
using System.Collections;
using BestHTTP;
using System.Xml;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using com.bighutgames.vacina.bo.entities;
using System;
using com.bighutgames.vacina.application.util;

namespace Project.Main
{
	public class ProfileElement : MonoBehaviour 
	{
		#region Fields
		private Register register;
		#endregion Fields

		#region Methods
		public void SetData(Register register)
		{
			this.register = register;

			transform.Find ("Name").GetComponent<Text> ().text = register.name;
            
            DateTime birth = this.register.birth.FromMilliseconds();
            DateTime now = DateTime.UtcNow;

            int days = (int)(now-birth).TotalDays;
            int weeks = days/7;
            int months = GetDifferenceInMonths(birth, now);
            int years = GetDifferenceInYears(birth, now);

            Text age = transform.Find("Age").GetComponent<Text>();
            if(years > 1)
                age.text = string.Format("{0} anos", years);
            else if(years == 1)
                age.text = "1 ano";
            else if (months > 1)
                age.text = string.Format("{0} meses", months);
            else if (weeks > 1)
                age.text = string.Format("{0} semanas", weeks);
            else if (weeks == 1)
                age.text = "1 semana";
            else if (days > 1)
                age.text = string.Format("{0} dias", days);
            else if (days == 1)
                age.text = "1 dia";
            else
                age.text = "Recém-nascido";
		}
		#endregion Methods

		#region Event Handlers
		public void OnEditClick()
		{
			MainControlller.Instance.CreateEditProfile (this.register);
		}
        #endregion Event Handlers

        #region Auxiliar Methods
        public int GetDifferenceInYears(DateTime startDate, DateTime endDate)
        {
            //Excel documentation says "COMPLETE calendar years in between dates"
            int years = endDate.Year - startDate.Year;

            if (startDate.Month == endDate.Month &&// if the start month and the end month are the same
                endDate.Day < startDate.Day)// BUT the end day is less than the start day
            {
                years--;
            }
            else if (endDate.Month < startDate.Month)// if the end month is less than the start month
            {
                years--;
            }

            return years;
        }

        public int GetDifferenceInMonths(DateTime startDate, DateTime endDate)
        {
            //Excel documentation says "COMPLETE calendar years in between dates"
            int years = endDate.Year - startDate.Year;
            int months = endDate.Month - startDate.Month;
            months = months + years * 12;

            if (endDate.Month < startDate.Month)// if the end month is less than the start month
            {
                months--;
            }

            return months;
        }
        #endregion Auxilair Methods
    }
}
