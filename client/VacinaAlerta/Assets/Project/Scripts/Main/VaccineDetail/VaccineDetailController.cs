﻿using UnityEngine;
using System.Collections;
using BestHTTP;
using System.Xml;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using System.Text;
using inovapps.Client;
using com.bighutgames.vacina.bo.game;
using System;


namespace Project.Main
{
	public class VaccineDetailController : MonoBehaviour
	{
		#region Methods
		public void SetData(string id)
		{
			Vacine v = Facade.Instance.GetVacines()[id];

			StringBuilder builder = new StringBuilder ();
			builder.AppendLine (string.Format ("<b><size=80>{0}</size></b>", v.name));
			builder.AppendLine ();

			builder.AppendLine ("<b><size=70>O que é?</size></b>");
			builder.AppendLine (v.description);
			builder.AppendLine ();

			builder.AppendLine ("<b><size=70>O que previne?</size></b>");
			builder.AppendLine (v.prevents);
			builder.AppendLine ();

			builder.AppendLine ("<b><size=70>Indicações da Vacina</size></b>");
			builder.AppendLine (v.indication);
			builder.AppendLine ();

			builder.AppendLine ("<b><size=70>Contra indicações da Vacina</size></b>");
			builder.AppendLine (v.counterIndication);
			builder.AppendLine ();

			builder.AppendLine ("<b><size=70>Efeitos adversos da Vacina</size></b>");
			builder.AppendLine (v.adverseEffect);
			builder.AppendLine ();

			int totalMonths = DateTime.Now.AddMonths (v.start).TotalMonths (DateTime.Now);
			int totalYears = DateTime.Now.AddMonths (v.start).TotalYears (DateTime.Now);
            if(totalYears < 100)
            {
                builder.AppendLine("<b><size=70>Quando Tomar?</size></b>");
                builder.AppendLine((totalYears > 1) ? (string.Format("A partir de {0} anos", totalYears)) : (string.Format("A partir de {0} meses", totalMonths)));
                builder.AppendLine();
            }

			builder.AppendLine ("<b><size=70>Quem deve tomar?</size></b>");
			if(v.male && v.female)
				builder.AppendLine ("Homens e mulheres");
			if(v.male && !v.female)
				builder.AppendLine ("Apenas homens");
			if(!v.male && v.female)
				builder.AppendLine ("Apenas mulheres");
			builder.AppendLine ();

			transform.Find ("Main/Details/Content").GetComponent<Text> ().text = builder.ToString();
		}
		#endregion Methods

	}
}