﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

namespace Project.Main
{
	public class UBSData {

		private static UBSData singleton;
		public static UBSData Singleton {
			get {
				if (singleton == null)
					singleton = new UBSData ();
				return singleton;
			}
		}

		private List<UBS> UBSs;

		public UBSData()
		{
			TextAsset asset = Resources.Load<TextAsset> ("ubs");
			string[] lines = asset.text.Split('\n');

			this.UBSs = new List<UBS> ();
			for (int i = 1; i < lines.Length-1; i++) {
				string[] values = lines[i].Split(',');
				UBSs.Add (new UBS (float.Parse(values[0]), float.Parse(values[1]),values[4],values[8]));
			}
		}

		public List<UBS> GetNearUBS(double latitude,double longitude, float maxDist)
		{
			List<UBS> tempUBSs = new List<UBS> ();
            Vector2 center = new Vector2((float)latitude, (float)longitude);
			foreach (UBS ubs in UBSs)
            {
                Vector2 position = new Vector2((float)ubs.lat, (float)ubs.lng);
                if (Vector2.Distance(position, center) < maxDist)
					tempUBSs.Add (ubs);
			}
			return tempUBSs;
		}
	}

	public class UBS {
		public float lat;
		public float lng;
		public string name;
		public string telefone;

		public UBS(float lat, float lng, string name, string telefone)
		{
			this.lat = lat;
			this.lng = lng;
			this.name = name;
			this.telefone = telefone;
		}
	}

	public class GPSVector2 {

		double lat;
		double lng;

		public GPSVector2(double lat, double lng)
		{
			this.lat = lat;
			this.lng = lng;
		}

		public static GPSVector2 operator +(GPSVector2 v1,GPSVector2 v2)
		{
			return new GPSVector2 (v1.lat + v2.lat, v1.lng + v2.lng);
		}

		public static GPSVector2 operator -(GPSVector2 v1,GPSVector2 v2)
		{
			return new GPSVector2 (v1.lat - v2.lat, v1.lng - v2.lng);
		}

		public static GPSVector2 operator /(GPSVector2 v,double factor)
		{
			return new GPSVector2 (v.lat/factor, v.lng/factor);
		}

		public static GPSVector2 operator *(GPSVector2 v,double factor)
		{
			return new GPSVector2 (v.lat*factor, v.lng*factor);
		}

		public Vector3 ToVector3()
		{
			return new Vector3 ((float)this.lng, (float)this.lat, 0);
		}
	}
}
