﻿using UnityEngine;
using System.Collections;
using Project.Main;
using System;
using UnityEngine.UI;

public class PlaceController : MonoBehaviour
{
    private UBS data;
    private Action<UBS> callback;

    public void SetData(UBS data, Action<UBS> callback)
    {
        this.data = data;
        transform.GetComponentInChildren<Text>().text = data.name;
        this.callback = callback;
    }

    public void OnSelectPlace()
    {
        this.callback(this.data);
    }
}
