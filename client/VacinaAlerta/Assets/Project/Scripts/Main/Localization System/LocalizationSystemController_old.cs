using UnityEngine;
using System.Collections;
using BestHTTP;
using System.Xml;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic;
using NewtonsoftUnity.Json;
using System;


namespace Project.Main
{
    public class IpData
    {
        public string city;
        public string country;
        public string countryCode;
        public double lat;
        public double lon;
        public string org;
        public string query;
        public string region;
        public string regionName;
        public string status;
        public string timezone;
        public string zip;
    }

    public class LocalizationSystemControllerOld : MonoBehaviour
	{
		#region Unity Fields
		public GameObject placePrefab;
		#endregion Unity Fields

		#region Fields
		private List<GameObject> places;
		#endregion Fields

		#region Methods
		public void SetData()
		{
			if (places != null) 
			{
				foreach (GameObject place in places)
					GameObject.Destroy (place);
			}
			places = new List<GameObject> ();
			LoadMap ();
		}
		#endregion Methods

		#region Event Handlers
		void OnDisable()
		{
		}
		#endregion Event Handlers

		#region Auxiliar Methods
		private void LoadMap()
		{
			StartCoroutine (LoadMapCoroutine ());
		}

		private IEnumerator LoadMapCoroutine()
		{
			int height = (int)(Screen.height * 0.875f);
			int width = Screen.width;

			float heightUI = 2048* 0.875f;
			float widthUI = 2048*Camera.main.aspect;


			IpData ipdata = null;
			bool processing = true;
			yield return StartCoroutine(LoadLocation(
				delegate(IpData result)
				{
					ipdata = result;
					processing = false;
				}
			));
			while (processing)
				yield return null;

			int zoom = 13;

			string url = "https://maps.googleapis.com/maps/api/staticmap?";
			url += "center=" + ipdata.lat + "," + ipdata.lon + "&";
			url += "size=" + width + "x" + height + "&";
			url += "zoom="+zoom+"&";
			url += "key=AIzaSyCTE1M-rHgI5d1vqO-27NfaVVnUMER8-Sk";
			float radius = 200/Mathf.Pow(2,zoom);
			List<UBS> ubs = UBSData.Singleton.GetNearUBS(ipdata.lat, ipdata.lon, radius);
            Debug.Log(ubs.Count);
			/*if (ubs.Count > 0)
			{
				foreach (UBS ub in ubs)
				{
					url += string.Format ("&markers=color:blue%7Clabel:{0}%7C{1},%20{2}", "P", ub.lat, ub.lng);
				}
			}*/
			Debug.Log (url);
			WWW www = new WWW (url);
			yield return www;
            
			transform.Find ("Main").GetComponent<RawImage> ().texture = www.texture;
		}
        
		private IEnumerator LoadLocation(Action<IpData> callback)
        {
			WWW www = new WWW ("http://ip-api.com/json");
			yield return www;
			callback (JsonConvert.DeserializeObject<IpData> (www.text));
        }
		#endregion Auxiliar Methods
	}
}