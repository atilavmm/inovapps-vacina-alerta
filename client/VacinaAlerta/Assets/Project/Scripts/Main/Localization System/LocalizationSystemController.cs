using UnityEngine;
using System.Collections;
using BestHTTP;
using System.Xml;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic;
using NewtonsoftUnity.Json;
using System;
using BigHut;
using Project.Popup;

namespace Project.Main
{
	public class LocalizationSystemController : MonoBehaviour
	{
        #region Unity Fields
        public GameObject HospitalContainer;
        public GameObject PlacePrefab;
        public GameObject PlayerPrefab;

        public float Radius;
        #endregion Unity Fields

        #region Fields
        private OnlineMaps mapnav;

        OnlineMapsMarker3D playerMark;

        public float userLat;
        public float userLon;

        private float lastUserLat = -100000;
        private float lastUserLon = -100000;

        private float lastLat = -100000;
        private float lastLon = -100000;

        private DateTime lastRefresh;


        private List<Transform> currentListWaypoint;

        private UBS currentTarget;

        private Image baseText;
        private Text text;
        #endregion Fields
        
        #region Methods
        public void SetData()
        {
            mapnav =  GameObject.FindGameObjectWithTag("GameController").GetComponent<OnlineMaps>();
            mapnav.zoomRange = new OnlineMapsRange(15, 20);

            baseText = transform.Find("Base").GetComponent<Image>();
            text = baseText.transform.Find("Text").GetComponent<Text>();

            lastRefresh = DateTime.MinValue;
            currentListWaypoint = new List<Transform>();

            StopAllCoroutines();
            StartCoroutine(Initialize());
        }
        IEnumerator Initialize()
        {
            text.text = "Inicializando sistema de mapa";
#if UNITY_IOS
            if (!Input.location.isEnabledByUser)
            {
                text.text = "Habilite sistema de gps e tente novamente";
                StartCoroutine(Refresh());
                yield break;
            }
#endif
            Input.location.Start();

            int maxWait = 20;
            while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
            {
                text.text = string.Format("Conectando ao gps {0}s de {1}s", 20-maxWait, 20);
                yield return new WaitForSeconds(1);
                maxWait--;
            }
            
            if (maxWait < 1)
            {
                text.text = "Sem internet";
                StartCoroutine(Refresh());
                yield break;
            }
            
            if (Input.location.status == LocationServiceStatus.Failed)
            {
                text.text = "Sem sinal de gps";
                print("Unable to determine device location");
                StartCoroutine(Refresh());

            }
            else
            {
                print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
            }

            text.text = "Mapa";


#if !UNITY_EDITOR
            mapnav.position = new Vector2(Input.location.lastData.longitude, Input.location.lastData.latitude);
            userLat = Input.location.lastData.latitude;
            userLon = Input.location.lastData.longitude;
#else
            userLat = mapnav.position.y;
            userLon = mapnav.position.x;
#endif
            lastRefresh = DateTime.MinValue;
            
        }

        private IEnumerator Refresh()
        {

            yield return new WaitForSeconds(2);
            int maxWait = 10;
            while (maxWait > 0)
            {
                text.text = string.Format("Tentando novamente em {0}...", maxWait);
                yield return new WaitForSeconds(1);
                maxWait--;
            }

            StartCoroutine(Initialize());
        }

        void OnDisable()
        {
            Input.location.Stop();
        }

        public void Update()
        {
            float lat = mapnav.position.y;
            float lon = mapnav.position.x;

            bool refreshPath = false;
#if !UNITY_EDITOR
            userLat = Input.location.lastData.latitude;
            userLon = Input.location.lastData.longitude;
#endif
            if ((lastUserLat != userLat || lastUserLon != userLon) && (DateTime.Now-lastRefresh).TotalSeconds > 0.6f)
            {
                RefreshPath();
                refreshPath = true;
                lastRefresh = DateTime.Now;
            }
#if !UNITY_EDITOR
            if (lastUserLat != userLat || lastUserLon != userLon)
            {
                mapnav.position = new Vector2(userLon, userLat);
                lastUserLon = userLon;
                lastUserLat = userLat;
            }
#endif

            if (refreshPath || (lat != lastLat || lon != lastLon) && Vector2.Distance(new Vector2(lon, lat), new Vector2(lastLat, lastLon)) > Radius/2)
            {
                List<UBS> ubs = UBSData.Singleton.GetNearUBS(lat, lon, Radius);
                if (ubs.Count > 0 && lat != lastLat && lon != lastLon)
                {
                    //Destroy all places
                    for (int i = 0; i < HospitalContainer.transform.childCount; i++)
                    {
                        GameObject.Destroy(HospitalContainer.transform.GetChild(i).gameObject);
                    }

                    //Populate news
                    OnlineMapsTileSetControl.instance.RemoveAllMarker3D();

                    playerMark = OnlineMapsTileSetControl.instance.AddMarker3D(new Vector2(userLon, userLat), PlayerPrefab);
                    playerMark.scale = 60;
                    playerMark.rotation = Quaternion.Euler(90, 0, 180);

                    foreach (UBS ub in ubs)
                    {
                        OnlineMapsMarker3D marker = OnlineMapsTileSetControl.instance.AddMarker3D(new Vector2(ub.lng, ub.lat), PlacePrefab);
                        marker.scale = 60;
                        marker.rotation = Quaternion.Euler(90,0, 180);
                        marker.transform.gameObject.GetComponent<PlaceController>().SetData(ub, CalculatePath);
                    }

                    HospitalContainer.transform.position = mapnav.transform.position;
                    HospitalContainer.transform.localScale = mapnav.transform.localScale;
                }
                lastLat = lat;
                lastLon = lon;
            }

            lastUserLon = userLon;
            lastUserLat = userLat;
        }

        public void CalculatePath(UBS data)
        {
            currentTarget = data;
            PopupManager.Instance.ShowPopup<OpenRoute>().SetData(lastUserLat, lastUserLon, data.lat, data.lng);

            RefreshPath();
        }

        public void RefreshPath()
        {
            if(currentTarget != null)
            {
                OnlineMapsFindDirectionAdvanced.Find(
                    new Vector2(lastUserLon, lastUserLat),
                    new Vector2(currentTarget.lng, currentTarget.lat)).OnComplete += ShowPath;
            
            }
        }

        public void ShowPath(string response)
        {
            OnlineMapsFindDirectionResult result = OnlineMapsFindDirectionAdvanced.GetResult(response);
            if (result != null)
            {
                OnlineMaps.instance.RemoveAllDrawingElements();

                Debug.Log(result.routes.Length);
                foreach (OnlineMapsFindDirectionResult.Route route in result.routes)
                {
                    OnlineMaps.instance.AddDrawingElement(
                        new OnlineMapsDrawingLine(route.overview_polyline.ToList(), Color.red, 3)
                    );
                }
            }
            else
            {
                Debug.Log(response);
            }
        }

        public void OpenGoogleMaps()
        {
            string url = "https://maps.google.com/?saddr="+lastLat+","+lastLon+"&daddr=" + currentTarget.lat + "," + currentTarget.lng;
            Application.OpenURL(url);
        }
        
#endregion Methods
    }
}