﻿using UnityEngine;
using System.Collections;
using BestHTTP;
using System.Xml;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using inovapps.Client;
using com.bighutgames.vacina.bo.game;
using System.Collections.Generic;


namespace Project.Main
{
	public class VaccinesController : MonoBehaviour
	{
		#region Unity Fields
		public GameObject VaccinePrefab;
		#endregion Unity Fields

		#region Methods
		public void SetData()
		{
			foreach (VaccineElement element in gameObject.GetComponentsInChildren<VaccineElement>())
				GameObject.Destroy (element.gameObject);

			StartCoroutine(SetVaccines ());
		}
		#endregion Methods

		#region Auxiliar Methods
		private IEnumerator SetVaccines()
		{
			Dictionary<string,Vacine> vacines = Facade.Instance.GetVacines();
			foreach (string v in vacines.Keys) {
				GameObject vaccineElement = GameObject.Instantiate (VaccinePrefab) as GameObject;
				vaccineElement.transform.SetParent (transform.Find ("Main/Vaccines/Content"));
				vaccineElement.transform.localScale = Vector3.one;
				vaccineElement.GetComponent<VaccineElement> ().SetData (vacines[v].name,v);
			}
			yield return null;
		}
		#endregion Auxiliar Methods
	}
}