﻿using UnityEngine;
using System.Collections;
using BestHTTP;
using System.Xml;
using System.IO;
using System.Linq;
using UnityEngine.UI;


namespace Project.Main
{
	public class VaccineElement : MonoBehaviour 
	{
		#region Fields
		private string name;
		private string key;
		#endregion Fields

		#region Methods
		public void SetData(string name,string key)
		{
			this.name = name;
			transform.Find ("Text").GetComponent<Text> ().text = name;
			this.key = key;
		}
		#endregion Methods

		#region Event Handlers
		public void OnDetailsClick()
		{
			MainControlller.Instance.ShowVaccineDetail (key);
		}
		#endregion Event Handlers
	}
}
