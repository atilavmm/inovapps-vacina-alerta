﻿using UnityEngine;
using System.Collections;
using inovapps.Client;
using BigHut;
using System;
using inovapps.Client.Util;
using com.bighutgames.vacina.bo.entities;
using UnityEngine.UI;

namespace Project.Main
{
	public class MainControlller : MonoBehaviour 
	{
		#region Singleton
		private static MainControlller instance;
		public static MainControlller Instance
		{
			get 
			{
				return instance;
			}
		}
        #endregion Singleton

        #region Fields
        private Image background;
        #endregion Fields

        #region Monobehaviour
        void Awake()
		{
			instance = this;
            background = transform.Find("Background").GetComponent<Image>();

            ShowNews ();
		}
		#endregion Monobehaviour

		#region Methods
		protected void ShowNews()
        {
            ShowPanel ("News");
			SetBar ("News");
            background.enabled = true;


            transform.Find ("News").GetComponent<NewsController> ().SetData ();

		}
		protected void ShowProfile()
        {
            ShowPanel ("Profile");
			SetBar ("Profile");
            background.enabled = true;

            transform.Find ("Profile").GetComponent<ProfileController> ().SetData ();
		}
		protected void ShowVaccines()
        {
            ShowPanel ("Vaccines");
			SetBar ("Vaccines");
            background.enabled = true;

            transform.Find ("Vaccines").GetComponent<VaccinesController> ().SetData ();
		}

		public void ShowVaccineDetail(string id)
        {
            ShowPanel ("Vaccine Details");
			SetBar ("Vaccines");
            background.enabled = true;

            transform.Find ("Vaccine Details").GetComponent<VaccineDetailController> ().SetData (id);
		}

		public void CreateEditProfile(Register register)
        {
            ShowPanel ("Edit Profile");
			SetBar ("Profile");
            background.enabled = true;

            transform.Find ("Edit Profile").GetComponent<EditProfileController> ().SetData (register);
		}

		public void ShowLocalizationSystem()
        {
            ShowPanel ("Localization System");
			SetBar ("Localization System");
            background.enabled = false;

            transform.Find ("Localization System").GetComponent<LocalizationSystemController> ().SetData ();
		}
		#endregion Methods

		#region Event Handlers
		public void OnBackClick()
		{
			ShowNews ();
		}

		public void OnProfileClick()
		{
			ShowProfile ();
		}

		public void OnVaccineClick()
		{
			ShowVaccines ();
		}

		public void OnLocalizationSystemClick()
		{
			ShowLocalizationSystem ();
		}
		#endregion Event Handlers

		#region Auxiliar Methods
		private void ShowPanel(string name)
		{
			for (int i = 0; i < transform.childCount; i++) 
			{
				Transform child = transform.GetChild (i);
				if (child.name != "Background" && child.name != "NavBar" && child.name != "PopupController") 
				{
					child.gameObject.SetActive (name == child.name);
				}
			}
		}

		private void SetBar(string name)
		{
			Transform navbar = transform.Find ("NavBar");
			for (int i = 0; i < navbar.childCount; i++) 
			{
				Transform child = navbar.GetChild (i);
                if(child.name != name)
                {
                    child.Find("Image").GetComponent<Image>().color = Color.gray;
                    child.Find("Text").GetComponent<Text>().color = Color.gray;
                }
                else
                {
                    child.Find("Image").GetComponent<Image>().color = new Color(0,0.509f,0.7647f);
                    child.Find("Text").GetComponent<Text>().color = new Color(0, 0.509f, 0.7647f);
                }
			}
		}
		#endregion Auxiliar Methods
	}
}