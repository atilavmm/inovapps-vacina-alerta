using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.bighutgames.vacina.bo.util;
using com.bighutgames.vacina.bo;
using System;
using System.Linq;
using com.bighutgames.vacina.application.util;
//using System.Threading;
using NewtonsoftUnity.Json;
using BigHut;
using System.Linq;
using System.IO;
using Unity.IO.Compression;
using com.bighutgames.vacina.util;
using inovapps.Client.Util;

namespace inovapps.Client
{
	public partial class ServerController 
	{

		#region Methods
		public IEnumerator Login(Action success, Action<ServerException> fail, Action<float> onProgress, bool reloadLocalData = true)
		{
			if (reloadLocalData) 
			{
				bool loadedLocalGame = false;
				LoadLocalGameData (
					delegate {
						loadedLocalGame = true;
					}
				);

				while (!loadedLocalGame)
					yield return null;
			}

			LoginAfterInitialization(success, fail, onProgress);
		}


		private void LoginAfterInitialization(Action success, Action<ServerException> fail, Action<float> onProgress)
		{
			User user = userManager.getUser();
			if(user == null)
			{
				user = userManager.createNewUser(UTCServer.ToMilliseconds());
				userManager.setUser(user);
			}

			Facade.Instance.GetFacebookUserId (
				delegate(string facebookId)
				{
					bool isSameUser = string.IsNullOrEmpty(user.facebookId) || user.facebookId == facebookId;

					List<ActionData> data = new List<ActionData>();
					if(isSameUser && isAutomaticSyncActive)
						data = syncServer.GetCurrentList();

					LoginRequest
					(
						data
						,
						delegate(User processedUser, GameData processedGameData) 
						{
							isSameUser &= (user.Id == processedUser.Id);

							hasConnection = true;
							SetDataOutOfSync(false);

							foreach(ActionData action in data)
								syncServer.Remove(action);

							if(!isSameUser)
								ClearPartial ();

							SyncLocalData(processedUser, processedGameData);

							if(success != null)
								success();
						}
						,
						delegate(ServerException error) 
						{
							if(fail != null)
								fail(error);
						}
						,
						onProgress
					);
				}
			);

		}
		#endregion Methods

		#region Auxiliar Methods
		private void LoginRequest(List<ActionData> actions, Action<User, GameData> success, Action<ServerException> fail, Action<float> onProgress)
		{
			Facade.Instance.GetFacebookUserId (
				delegate(string facebookId)
				{

					String deviceID = null;
					if(PlayerPrefs.HasKey("game.deviceID"))
						deviceID = PlayerPrefs.GetString("game.deviceID");
					Hashtable data = new Hashtable();
					if (userManager.getUser() != null)
						data ["userID"] = userManager.getUser().Id;
					
					data["facebookID"] = facebookId;

					if(deviceID != null)
						data["deviceID"] = long.Parse(deviceID);
					if(dataManager.getGameData() != null)
						data["gameDataVersion"] = dataManager.getGameData().version;

					data["actions"] = BHSecurity.Encrypt(JsonConvert.SerializeObject(actions));

					ExecuteRequest(
						"user/login",
						data,
						true,
						delegate(GenericMessage genericMessage)
						{
							LoginReturn login = JsonConvert.DeserializeObject<LoginReturn>(BHSecurity.Decrypt(genericMessage.message));
							success(login.getUser(), login.getData());
							syncServer.RefreshServerTime(genericMessage.time);
							SetDataOutOfSync(false);
						}
						,
						delegate(ServerException error) 
						{
							fail(error);
						}
						,
						onProgress
					);
				}
			);
		}

		private void LoadLocalGameData(Action onComplete)
		{
			GameData previousGameData = dataManager.getGameData ();

			TextAsset asset = Resources.Load("Data/GameData", typeof(TextAsset)) as TextAsset;
			string rawLocalGameData = Zipper.UnzipData(asset.bytes);

			//Thread thread = new Thread(
			//	new ThreadStart(() =>
			//        {
						LoadLocalGameDataThread(previousGameData, rawLocalGameData);
						onComplete();
			//		}
			//	)
			//);
			//thread.Start();
		}

		private void LoadLocalGameDataThread(GameData previousGameData, string rawLocalGameData)
		{
			Backup backup = JsonConvert.DeserializeObject<Backup>(rawLocalGameData);
			GameData rawGameData = backup.getData ();
			if(
				previousGameData == null 
				||
				previousGameData.version < rawGameData.version
				)
			{
				dataManager.setGameData (rawGameData);
			}
		}

		protected void SyncLocalData(User user, GameData gameData = null)
		{
			if(user != null)
				userManager.setUser(user);

			if(gameData != null)
			{
				dataManager.setGameData (gameData);
			}
		}
		#endregion Auxiliar Methods

		#region Pending Processes
		protected void SyncLocalDataThread(GenericMessage genericMessage)
		{
			Sync(genericMessage);
		}
		
		private void Sync(object data)
		{
			GenericMessage genericMessage = (GenericMessage)data;
			string rawUser = BHSecurity.Decrypt(genericMessage.message);
			User processedUser = JsonConvert.DeserializeObject<User>(rawUser);
			userManager.setUser(processedUser, rawUser);;
		}
		#endregion Pending Processes
	}
	
}
