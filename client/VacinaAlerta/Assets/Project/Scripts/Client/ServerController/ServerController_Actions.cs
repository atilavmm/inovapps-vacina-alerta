using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.bighutgames.vacina.bo.util;
using com.bighutgames.vacina.bo;
using System;
using NewtonsoftUnity.Json;

namespace inovapps.Client
{
	public partial class ServerController 
	{

		public Dictionary<string,object> ExecuteActionDeleteMember(string id)
		{
			Dictionary<string,object> hash = new Dictionary<string,object>();
			hash["id"] = id;
			return ExecuteGenericAction(hash,ActionType.DELETE_MEMBER);
		}

		public Dictionary<string,object> ExecuteActionAddMember(string name, bool isMale,bool isPregnant, long birth)
		{
			Dictionary<string,object> hash = new Dictionary<string,object>();
			hash["name"] = name;
			hash["isMale"] = isMale?"true":"false";
			hash["isPregnant"] = isPregnant?"true":"false";
			hash["birth"] = birth.ToString("0");
			return ExecuteGenericAction(hash,ActionType.ADD_MEMBER);
		}
		public Dictionary<string,object> ExecuteActionUpdateMember(string id,string name, bool isMale,bool isPregnant, long birth)
		{
			Dictionary<string,object> hash = new Dictionary<string,object>();
			hash["id"] = id;
			hash["name"] = name;
			hash["isMale"] = isMale?"true":"false";
			hash["isPregnant"] = isPregnant?"true":"false";
			hash["birth"] = birth.ToString("0");
			return ExecuteGenericAction(hash,ActionType.UPDATE_MEMBER);
		}
		public Dictionary<string,object> ExecuteActionVacine(string id,string vacineId, long actionTime)
		{
			Dictionary<string,object> hash = new Dictionary<string,object>();
			hash["id"] = id;
			hash["vacineId"] = vacineId;
            return ExecuteGenericAction(hash,ActionType.VACINE, actionTime);
		}
	}

}