using System;
using System.Collections.Generic;
using com.bighutgames.vacina.bo.util;
using com.bighutgames.vacina.application.util;
using UnityEngine;
using NewtonsoftUnity.Json;
using BigHut;

namespace inovapps.Client.Util
{
	public class SyncServer
	{
		#region Fields
		private List<ActionData> bufferedCommands;
		private DateTime lastSaveTime;
		private DateTime lastSyncTime;
		private TimeConverter timeConverter;
		private bool isOutOfSync;

		private bool changedBufferedCommands;
		#endregion Fields

		#region Properties
		public DateTime LastSyncTime 
		{
			get 
			{
				return lastSyncTime;
			}
			set 
			{
				lastSyncTime = value;
			}
		}

		public TimeConverter TimeConverter 
		{
			get 
			{
				return timeConverter;
			}
			set 
			{
				timeConverter = value;
			}
		}

		public bool HasElementOnQueue
		{
			get
			{
				return bufferedCommands.Count != 0;
			}
		}
		#endregion Properties

		#region Constructor
		public SyncServer ()
		{
			bufferedCommands = new List<ActionData> ();
			lastSyncTime = DateTime.MinValue;
			timeConverter = new TimeConverter ();
			isOutOfSync = false;

			if(PersistanceManager.Instance.ContainsWithFile("actions"))
			{
				try
				{
					bufferedCommands = new List<ActionData>(JsonConvert.DeserializeObject<ActionData[]>(PersistanceManager.Instance.GetElementWithFile<string>("actions")));
				}
				catch(Exception e)
				{
					Debug.Log(e.StackTrace);
					PersistanceManager.Instance.DeleteElementWithFile("actions");
				}
			}
		}
		#endregion Constructor

		#region Methods
		public void RefreshServerTime(long serverTime)
		{
			timeConverter.Refresh (serverTime);
		}

		public bool IsTimeForSync()
		{
			//if (lastSyncTime > DateTimeHelper.UTCNow())
			//	lastSyncTime = DateTimeHelper.UTCNow();
			//return ((DateTimeHelper.UTCNow() - lastSyncTime).TotalSeconds > Facade.Instance.GetMaxSyncTime() || bufferedCommands.Count >= Facade.Instance.GetMaxSyncBufferSize()) && !(bufferedCommands.Count == 0 && Application.loadedLevelName == "Game");
			return true;
		}

		public void RefreshUpdateTime()
		{
			lastSyncTime = DateTimeHelper.UTCNow();
		}

		public void Update()
		{
			if(DateTime.Now > lastSaveTime.AddSeconds(5))
			{
				ForceSaveData();
				lastSaveTime = DateTime.Now;
			}
		}

		public void ForceSaveData()
		{
			if(changedBufferedCommands)
			{
				PersistanceManager.Instance.ForceSave ();
				changedBufferedCommands = false;
			}
		}

		public void Add(ActionData action)
		{
			bufferedCommands.Add(action);
			string raw =  JsonConvert.SerializeObject(bufferedCommands.ToArray());
			PersistanceManager.Instance.SetElementWithFile("actions", null, raw);
			changedBufferedCommands = true;
		}

		public void Remove(ActionData action)
		{
			bufferedCommands.Remove(action);
			string raw =  JsonConvert.SerializeObject(bufferedCommands.ToArray());
			PersistanceManager.Instance.SetElementWithFile("actions", null, raw);
			changedBufferedCommands = true;
		}

		public List<ActionData> GetCurrentList()
		{
			return new List<ActionData>(bufferedCommands);
		}

		public void ResetActions ()
		{
			PersistanceManager.Instance.DeleteElementWithFile("data", "actions");
			bufferedCommands = new List<ActionData>();
		}
		#endregion Methods
	}

}

