﻿using UnityEngine;
using System.Collections;
using System.IO;
using Unity.IO.Compression;

public class Zipper 
{
	public static byte[] ZipData(string text)
	{
		byte[] data = new byte[0];
		using (var memoryStream = new MemoryStream())
		{
			using (var gzipStream = new GZipStream(memoryStream, CompressionMode.Compress, false))
				using (var writer = new StreamWriter(gzipStream))
			{
				writer.Write(text);
				
			}
			data = memoryStream.ToArray();
		}
		return data;
	}

	public static string UnzipData(byte[] bytes)
	{
		string data = "";
		using (var memoryStream = new MemoryStream(bytes))
			using (var gzipStream = new GZipStream(memoryStream, CompressionMode.Decompress, false))
				using (var reader = new StreamReader(gzipStream))
		{
			string text = reader.ReadToEnd();
			data = text;
		}
		return data;
	}
}
