using System;
using System.Collections.Generic;
using com.bighutgames.vacina.bo.util;
using com.bighutgames.vacina.application.util;
using UnityEngine;
using NewtonsoftUnity.Json;

namespace inovapps.Client.Util
{
	public class ServerException
	{
		#region Fields
		private string name;
		private string message;
		#endregion Fields

		#region Properties
		public string Name 
		{
			get 
			{
				return name;
			}
			set 
			{
				name = value;
			}
		}

		public string Message 
		{
			get
			{
				return message;
			}
			set
			{
				message = value;
			}
		}
		#endregion Properties

		#region Constructor
		public ServerException (string name, string message)
		{
			this.name = name;
			this.message = message;
		}
		#endregion Constructor
	}
}

