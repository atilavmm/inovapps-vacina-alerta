using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using com.bighutgames.vacina.bo.util;
using com.bighutgames.vacina.bo;
using inovapps.Client.Util;
using com.bighutgames.vacina.bll;
using com.bighutgames.vacina.bo.entities;
using com.bighutgames.vacina.application.util;
using System.Threading;
using NewtonsoftUnity.Json;
using BigHut;
using BestHTTP;

namespace inovapps.Client
{	
	public class ServerConstants
	{
		public const string DEVELOPMENT_SERVER = "https://vacinaalerta.appspot.com/_ah/api/vacinaserver/v1/";
		public const string STAGE_SERVER = "https://vacinaalerta.appspot.com/_ah/api/vacinaserver/v1/";
		public const string LIVE_SERVER = "https://vacinaalerta.appspot.com/_ah/api/vacinaserver/v1/";
		public const string LOCAL_SERVER = "http://localhost:8888/_ah/api/vacinaserver/v1/";

		public const float CONNECTION_TIMEOUT = 12f;
		public const float DOWNLOAD_TIMEOUT = 100f;
	}
	
	public enum ServerType
	{
		LOCAL,
		DEVELOPMENT,
		STAGE,
		PRODUCTION,
		LIVE
	}
	
	public partial class ServerController : MonoBehaviour 
	{
		#region Static Methods
		public static ServerController CreateInstance()
		{
			ServerController instance = null;
			GameObject serverManager = new GameObject("ServerManager");
			instance = serverManager.AddComponent<ServerController>();
			GameObject.DontDestroyOnLoad(serverManager);
			return instance;
		}
		#endregion Static Methods

		#region Unity Fields
		public event Action<string> OnSyncIssueEvent;
		#endregion Unity Fields

		#region Fields
		private ServerType type;
		private string url;
		private bool isDataOutOfSync;

		private int isProcessingServerCount;
		private bool isAutomaticSyncActive = true;
		private SyncServer syncServer;
		
		private UserManager userManager;
		private GameDataManager dataManager;

		private float timeSinceLastAction;

		private int currentErrorMessage;
		private string previousErrorMessage;
		private bool hasConnection;
		#endregion Fields

		#region Properties
		public DateTime UTCServer
		{
			get
			{
				return syncServer.TimeConverter.ServerTime.FromMilliseconds();
			}
		}
		
		public long UTCServerMilliseconds
		{
			get
			{
				return syncServer.TimeConverter.ServerTime;
			}
		}

		public ServerType Type 
		{
			get 
			{
				return type;
			}
		}


		public UserManager UserManager 
		{
			get 
			{
				return userManager;
			}
		}

		public GameDataManager DataManager 
		{
			get 
			{
				return dataManager;
			}
		}

		public string BaseURL
		{
			get
			{
				return url.Substring(0, url.IndexOf('/',15)+1);
			}
		}

		public float CurrentGameTime
		{
			get
			{
				long partialTime = (long)((Time.time-timeSinceLastAction)*1000);
				long currentGameTime = partialTime+userManager.getUser().gameplayTime;
				return currentGameTime/1000f;
			}
		}

		public bool HasConnection 
		{
			get
			{
				return hasConnection;
			}
			set 
			{
				hasConnection = value;
			}
		}

		public bool IsAutomaticSyncActive 
		{
			get 
			{
				return isAutomaticSyncActive;
			}
			set 
			{
				isAutomaticSyncActive = value;
			}
		}
		#endregion Properties

		#region Methods
		public void Initialize(ServerType type)
		{
			this.type = type;
			switch (type) 
			{
			case ServerType.LOCAL:
				url = ServerConstants.LOCAL_SERVER;
				break;
			case ServerType.DEVELOPMENT:
				url = ServerConstants.DEVELOPMENT_SERVER;
				break;
			case ServerType.STAGE:
				url = ServerConstants.STAGE_SERVER;
				break;
			case ServerType.LIVE:
				url = ServerConstants.LIVE_SERVER;
				break;
			}
			Debug.Log("Stop All Coroutines");
			StopAllCoroutines();
			isProcessingServerCount = 0;

			dataManager = new GameDataManager ();
			userManager = new UserManager (dataManager);
			syncServer = new SyncServer ();
			OnSyncIssueEvent = null;
			previousErrorMessage = "";
			currentErrorMessage = 0;

			if(PersistanceManager.Instance.Contains("user.dataoutofsync"))
				isDataOutOfSync = PersistanceManager.Instance.GetElement<bool>("user.dataoutofsync");
		}


		public Dictionary<string,object> ExecuteGenericAction(Dictionary<string,object> hash,ActionType type, long? actionTime = null)
		{
			ActionData action = GenerateActionData(hash, type);
			return ExecuteAction(action, isAutomaticSyncActive, actionTime);
		}

		public ActionData GenerateActionData(Dictionary<string,object> hash,ActionType type)
		{
			ActionData action = new ActionData ();
			action.data = JsonConvert.SerializeObject(hash);
			action.data = action.data.Replace("\\u0001","");
			action.type = type.ToString();
			action.time = syncServer.TimeConverter.ServerTime;
			//Set Time
			action.gameplayGapTime =(long) ((Time.time-timeSinceLastAction)*1000);
			timeSinceLastAction = Time.time;
			return action;
		}

		public void Update()
		{
			if(userManager != null)
			{
				User user = userManager.getUser();
				if(user != null && user.Id != -1)
				{
					if(Time.frameCount%10 == 0)
					{
						if(isAutomaticSyncActive && !isDataOutOfSync && syncServer != null && syncServer.IsTimeForSync() && isProcessingServerCount == 0) 
						{
							SynchronizeServer();
							syncServer.RefreshUpdateTime ();
						}
					}
				}
			}

			if(isDataOutOfSync)
			{
				ShowOutOfSyncError();
			}

			syncServer.Update ();
		}
        
		public void OnGUI()
		{
			Facade.Instance.RefreshRequestList ();
		}
        
		public void ClearPartial()
		{
			syncServer.ResetActions ();
			SetDataOutOfSync (false);
		}
		
		public void ForceSubmit()
		{
			SynchronizeServer();
			userManager.SaveUser();

			syncServer.RefreshUpdateTime ();
		}

		public void SetDataOutOfSync(bool state)
		{
			isDataOutOfSync = state;
			PersistanceManager.Instance.SetElement("user.dataoutofsync", state);
		}
		#endregion Methods
		
		#region Auxiliar Methods
		private void SynchronizeServer() 
		{
			List<ActionData> actionData = syncServer.GetCurrentList();
			SynchronizeServer(actionData);
		}

		
		private void SynchronizeServer(List<ActionData> commands) 
		{
			SubmitSynchronizeServer(commands);
		}

		private void SubmitSynchronizeServer(List<ActionData> commands)
		{
			Hashtable data = new Hashtable();
			User user = userManager.getUser();
			data["userID"] = user.Id;
			
			List<ActionData> actionData = new List<ActionData>(commands);
			data["actions"] = BHSecurity.Encrypt(JsonConvert.SerializeObject(actionData));
			
			ExecuteRequest(
				"game/actions",
				data,
				true,
				delegate(GenericMessage genericMessage)
				{	
					hasConnection = true;
					SetDataOutOfSync(false);

					syncServer.RefreshServerTime(genericMessage.time);

					foreach(ActionData action in commands)
						syncServer.Remove(action);

					if(!syncServer.HasElementOnQueue)
					{
						//SyncLocalDataThread(genericMessage);
					}
				}
				,
				delegate(ServerException error)
				{
					#if UNITY_EDITOR
					Debug.LogWarning(error.Name);
					#else
					Debug.LogWarning(error.Name);
					#endif

					if(error.Name.Contains("409 Conflict"))
					{
						SetDataOutOfSync (true);
						ShowOutOfSyncError();
					}
				}
				,
				null
			);
		}

		private Dictionary<string,object> ExecuteAction(ActionData action, bool isSyncingUser, long? actionTime)
		{
			action.time = syncServer.TimeConverter.ServerTime;
            if (actionTime != null)
                action.time = actionTime.Value;

            action.checkSumBefore = userManager.getUser().getCheckSum();
			Dictionary<string,object> response = userManager.executeAction (action);
			action.checkSum = userManager.getUser().getCheckSum();
			syncServer.Add (action);

			if(isSyncingUser)
				userManager.SaveUser();

			return response;
		}

		private void ShowOutOfSyncError()
		{
			//if(!OutOfSync.HasOutOfSync())
			//{
			//	PopupManager.Instance.ShowPopup<OutOfSync>().SetData();
			//}
		}
		#endregion Auxiliar Methods
		
		#region Server Communication Helper
		private void ExecuteRequest(string method, Hashtable data, bool handleResponse, Action<GenericMessage> success, Action<ServerException> fail, Action<float> onProgress)
		{
			if(handleResponse)
			{
				isProcessingServerCount += 1;
			}

			StartCoroutine(ExecuteRequestCoroutine(method, data, handleResponse, success, delegate(ServerException error) {
				fail(error);
			}, onProgress));
		}
		
		private IEnumerator ExecuteRequestCoroutine(string method, Hashtable data, bool handleResponse, Action<GenericMessage> success, Action<ServerException> fail, Action<float> onProgress)
		{
			HTTPRequest request = GetRequest(method, data);
			request.OnProgress += delegate(HTTPRequest originalRequest, int downloaded, int downloadLength) {
				if (onProgress != null)
					onProgress (downloaded / (float)downloadLength);
			};
			request.Send ();

			yield return StartCoroutine (request);

			if(handleResponse)
			{
				if(request.State == HTTPRequestStates.Finished && request.Response.IsSuccess)
				{
					GenericMessage message = JsonConvert.DeserializeObject<GenericMessage>(request.Response.DataAsText);
					if(message.error == null)
					{
						if(success != null)
							success(message);
					}
					else
					{
						fail(new ServerException("409 Conflict", (message.error.error.ToString()+"\n"+message.error.message)));
					}
				}
				else
				{
					if(request.Response == null)
					{
						fail(new ServerException(request.Exception.Message, request.Exception.Message));
					}
					else
					{
						fail(new ServerException(request.Response.Message, request.Response.DataAsText));
					}
				}
				ProcessConnection(request.State);
				isProcessingServerCount -= 1;
			}
		}

		public void ProcessConnection(HTTPRequestStates state)
		{
			if (state == HTTPRequestStates.ConnectionTimedOut || state == HTTPRequestStates.TimedOut  || state == HTTPRequestStates.Error)
				hasConnection = false;
			else
				hasConnection = true;
		}

		public HTTPRequest GetRequest(string method, Hashtable data)
		{
			HTTPRequest request = new HTTPRequest(new Uri(url+method), HTTPMethods.Post);
			foreach(string key in data.Keys)
				request.AddField(key, data[key].ToString());

			request.FormUsage = BestHTTP.Forms.HTTPFormUsage.Unity;
			request.UseStreaming = false;
			request.DisableCache = true;
			request.IsCookiesEnabled = false;
			request.SetHeader("Content-Type", "application/x-www-form-urlencoded");
			request.ConnectTimeout = TimeSpan.FromSeconds(ServerConstants.CONNECTION_TIMEOUT);
			request.Timeout = TimeSpan.FromSeconds(ServerConstants.DOWNLOAD_TIMEOUT);

			return request;
		}
		public HTTPRequest GetRequest(string method)
		{
			HTTPRequest request = new HTTPRequest(new Uri(url+method));
			request.FormUsage = BestHTTP.Forms.HTTPFormUsage.Unity;
			request.UseStreaming = false;
			request.DisableCache = true;
			request.IsCookiesEnabled = false;
			request.SetHeader("Content-Type", "application/x-www-form-urlencoded");
			request.ConnectTimeout = TimeSpan.FromSeconds(ServerConstants.CONNECTION_TIMEOUT);
			request.Timeout = TimeSpan.FromSeconds(ServerConstants.DOWNLOAD_TIMEOUT);

			return request;
		}
		#endregion Server Communication Helper
	}
}
