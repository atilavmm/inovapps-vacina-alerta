﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.bighutgames.vacina.bo.util;
using com.bighutgames.vacina.bo;
using System;
using System.Linq;
using com.bighutgames.vacina.application.util;
using System.Threading;
using NewtonsoftUnity.Json;
using BigHut;
using System.Linq;
using System.IO;
using com.bighutgames.vacina.util;
using inovapps.Client.Util;

namespace inovapps.Client
{
	public partial class ServerController 
	{
		#region Fields
		private DateTime lastUnfocusTime;
		private bool force;
		#endregion Fields

		
		#region Methods
		public void SetForceRefreshGame()
		{
			force = true;
		}
		#endregion Methods

		#region Event Handlers
		void Awake()
		{
		}

		void OnApplicationFocus(bool focusStatus)
		{
			if (focusStatus) 
			{
				if(force || (lastUnfocusTime != default(DateTime) && (DateTime.UtcNow-lastUnfocusTime).TotalMinutes > 30))
				{
					Facade.Instance.LoadInitialization();
					force = false;
				}
			}
			else
			{
				lastUnfocusTime = DateTime.UtcNow;
			}

		}

		void OnApplicationPause(bool focusStatus)
		{
			syncServer.ForceSaveData ();
		}
		#endregion Event Handlers
	}
	
}
