using UnityEngine;
using com.bighutgames.vacina.bll;
using System;
using com.bighutgames.vacina.bo;
using BigHut;
using System.Collections.Generic;
using inovapps.Client.Util;

namespace inovapps.Client
{
	public partial class Facade 
	{
		#region Singleton
		private static Facade instance;
		public static Facade Instance
		{
			get
			{
				if(instance == null)
				{
					instance = new Facade();
				}
				return instance;
			}
		}
		#endregion Singleton

		#region Fields
		private ServerController serverController;
		private bool isInitialized;
		
		private bool isGameDataSynced;
		private bool isLevelSynced;
		private Dictionary<string, object> modifiers;

		private float rumBountyChance;

		private List<Action> facebookCalls;
		#endregion Fields

		#region Properties
		public DateTime UTCServer
		{
			get
			{
				return serverController.UTCServer;
			}
		}
		
		public long UTCServerMilliseconds
		{
			get
			{
				return serverController.UTCServerMilliseconds;
			}
		}

		public ServerController ServerController 
		{
			get 
			{
				return serverController;
			}
			set 
			{
				serverController = value;
			}
		}

		public bool IsInitialized
		{
			get
			{
				return isInitialized;
			}
			set 
			{
				isInitialized = value;
			}
		}

		public Dictionary<string, object> Modifiers
		{
			get
			{
				return modifiers;
			}
		}

		public List<Action> FacebookCalls
		{
			get 
			{
				return facebookCalls;
			}
		}
		#endregion Properties

		#region Config / Initialization
		public void Initialize(ServerType type)
		{
            if (!isInitialized)
			{
				isInitialized = true;
                serverController = ServerController.CreateInstance();
				serverController.Initialize(type);
				modifiers = new Dictionary<string, object>();
				rumBountyChance = 0;

				//IAPManager.Instance.SetTransactionHandler(this);

				facebookCalls = new List<Action> ();
            }
		}
		#endregion Config / Initialization

		#region Login

		public void Login(Action success, Action<ServerException> fail, Action<float> onProgress, bool reloadLocalData = true)
		{
			serverController.StartCoroutine(
				serverController.Login(
					delegate 
					{
						if(success != null)
							success();
						RefreshNotifications();
					}, 
					fail,
					onProgress,
					reloadLocalData
				)
			);
		}
		/*
		public void DownloadContent(Action success, Action fail)
		{
			serverController.DownloadContent (success,fail);
		}
		*/
		#endregion Login

		#region Methods
		public void SetAutoSyncServer(bool enable)
		{
			if(serverController != null)
				serverController.IsAutomaticSyncActive = enable;
		}

		public void ClearPartialProgress()
		{
			if (serverController != null)
				serverController.ClearPartial ();
		}

		public void SetModifiers(Dictionary<string, object> newModifiers)
		{
			bool changed = false;
			foreach(var element in newModifiers)
			{
				if(!modifiers.ContainsKey(element.Key) || modifiers[element.Key] != element.Value)
					changed = true;
				modifiers[element.Key] = element.Value;
			}
			//processedGameData = null;
		}
		#endregion Methods

		#region Environment
		public ServerType GetCurrentServerType()
		{
			return serverController.Type;
		}
		public bool IsLive()
		{
			return serverController.Type == ServerType.LIVE;
		}
		#endregion Environment
	}
}