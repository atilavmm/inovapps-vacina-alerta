using System;
using com.bighutgames.vacina.bo;
using System.Collections.Generic;
using com.bighutgames.vacina.bo.util;
using UnityEngine;
using com.bighutgames.vacina.bo.entities;
using System.Linq;
using BigHut;
using com.bighutgames.vacina.bo.game;

namespace inovapps.Client
{
	public partial class Facade
	{
		public Dictionary<string,object> ExecuteActionDeleteMember(string id)
		{
			serverController.ExecuteActionDeleteMember (id);
			RefreshNotifications ();
			return new Dictionary<string, object>();
		}

		public Dictionary<string,object> ExecuteActionAddMember(string name, bool isMale, long birth,bool isPregnant)
		{
			serverController.ExecuteActionAddMember (name,isMale,isPregnant,birth);
			foreach (Vacine v in GetVacines().Values) {
				if(!(!v.male && isMale))
					NotificationManager.Instance.ReScheduleNotification (string.Format (v.name), string.Format ("{0} precisa tomar {1}",name,v.name), new DateTime(v.start), 0);
			}
			RefreshNotifications ();
			return new Dictionary<string, object>();
		}

		public Dictionary<string,object> ExecuteActionUpdateMember(string id,string name, bool isMale,bool isPregnant, long birth)
		{
			serverController.ExecuteActionUpdateMember (id,name,isMale,isPregnant,birth);
			foreach (Vacine v in GetVacines().Values) {
				if(!(!v.male && isMale))
					NotificationManager.Instance.ReScheduleNotification (string.Format (v.name), string.Format ("{0} precisa tomar {1}",name,v.name), new DateTime(v.start), 0);
			}
			RefreshNotifications ();
			return new Dictionary<string, object>();
		}

		public Dictionary<string,object> ExecuteActionVacine(string id, string vacineId, long actionTime)
		{
			serverController.ExecuteActionVacine (id,vacineId, actionTime);
			RefreshNotifications ();
			return new Dictionary<string, object>();
		}
	}
}
