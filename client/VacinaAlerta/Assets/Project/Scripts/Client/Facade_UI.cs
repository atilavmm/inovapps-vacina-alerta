using System;
using System.Linq;
using com.bighutgames.vacina.bo;
using com.bighutgames.vacina.bo.entities;
using System.Collections.Generic;
using com.bighutgames.vacina.bo.util;
using com.bighutgames.vacina.bll;
using System.Collections;
using UnityEngine;
using NewtonsoftUnity.Json;
using BigHut;
using inovapps.Client.Util;
#if UNITY_WINRT
using Prime31.WinPhoneSocialNetworking;
#else
using Facebook.Unity;
using Facebook.MiniJSON;
#endif
using Project.Popup;
using BigHut.inovapps.Transition;

namespace inovapps.Client
{
	public partial class Facade 
	{
        #region Login
		public void LoginFlow(Action success, Action fail = null)
		{
			FacebookLogging logging = null;
			#if !UNITY_WEBGL
			logging = PopupManager.Instance.ShowPopup<FacebookLogging>();
			#endif
			long previousUserId = Facade.instance.GetUser ().Id;

			LoginSocial(
				delegate() 
				{
					LoginWithFacebookId(logging, previousUserId, success, fail);
				}
				,
				delegate 
				{
					#if UNITY_WEBGL
					LoginFlow(success, fail);
					#else
					GameObject.Destroy(logging.gameObject);
					FacebookConnectionChange connectionChange = PopupManager.Instance.ShowPopup<FacebookConnectionChange>();
					connectionChange.SetData(FacebookConnectionChangeType.Failed);
					if(fail != null)
						fail();
					#endif

				}
			);
		}

		private void LoginWithFacebookId(FacebookLogging logging, long previousUserId, Action success, Action fail = null, int count = 3)
		{
			//Setting User Id
			Facade.Instance.Login
			(
				delegate
				{
					CheckSocialConsistency(
						delegate(bool consistent) 
						{
							if(consistent)
							{
								// User Facebook Id is Equals to Access Token Facebook Id
								#if !UNITY_WEBGL
								GameObject.Destroy(logging.gameObject);
								FacebookConnectionChange connectionChange = PopupManager.Instance.ShowPopup<FacebookConnectionChange>();
								connectionChange.SetData(FacebookConnectionChangeType.Connected, 
									delegate
									{ 
								#endif
										Facade.Instance.RefreshFriendsPlayingCount();
										Facade.Instance.RefreshInvitableFriendsCount();

										if(success != null)
											success();

										if(previousUserId != Facade.Instance.GetUser().Id)
											LoadMain();
										#if !UNITY_WEBGL
									}
								);
										#endif
							}
							else
							{
								// Logged Account is different from facebook account
								if(count > 0)
								{
									LoginWithFacebookId(logging, previousUserId, success, fail, count-1);
								}
								else
								{
									#if !UNITY_WEBGL
									GameObject.Destroy(logging.gameObject);
									LogoutFlow();
									#endif
									if(fail != null)
										fail();
								}	
							}
						}
					);
				}
				,
				delegate(ServerException error)
				{
					#if UNITY_EDITOR
					Debug.LogError(error.Name);
					#else
					Debug.LogWarning(error.Name);
					#endif

					#if !UNITY_WEBGL
					GameObject.Destroy(logging.gameObject);
					LogoutFlow();
					#endif

					if(fail != null)
						fail();
				}
				,
				null
				,
				false
			);
		}

		public void LogoutFlow()
		{
			FacebookConnectionChange connectionChange = PopupManager.Instance.ShowPopup<FacebookConnectionChange>();
			connectionChange.SetData(FacebookConnectionChangeType.Failed);

			cachedFacebookId = null;
			#if UNITY_WINRT
			FacebookAccess.logout();
			#else
			FB.LogOut();
			#endif
		}
		#endregion Login

		public void LoadLogin()
		{
			TransitionView.PlaySceneTransition("Login");
		}

		public void LoadMain()
		{
			TransitionView.PlaySceneTransition("Main");
		}

		public void LoadInitialization()
		{
			Application.LoadLevel("Initialization");
		}

		public void SolveOutOfSync()
		{
			ServerController.ClearPartial ();
			LoadInitialization ();
		}

		public void OpenStorePage()
		{
			string urlPage = "";

			#if UNITY_IOS
			urlPage = "itms-apps://itunes.apple.com/app/id1003186698";
			#else
			urlPage = "market://details?id=com.bulkypix.sevenseas";
			#endif

			Application.OpenURL(urlPage);
		}
	}
}