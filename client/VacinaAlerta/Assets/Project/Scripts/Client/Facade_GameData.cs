﻿using System;
using System.Linq;
using com.bighutgames.vacina.bo;
using com.bighutgames.vacina.bo.entities;
using System.Collections.Generic;
using com.bighutgames.vacina.bo.util;
using com.bighutgames.vacina.bll;
using com.bighutgames.vacina.bo.game;
using com.bighutgames.vacina.application.util;
using System.Collections;
using BigHut;
using UnityEngine;

namespace inovapps.Client
{
	public partial class Facade 
	{
		public GameData GetGameData()
		{
			return serverController.DataManager.getGameData ();
		}

		public Dictionary<string,Vacine> GetVacines()
		{
			Dictionary<string,Vacine> vacines = new Dictionary<string,Vacine> ();
			foreach (string key in GetGameData().metadata.Keys) {
				if (GetGameData ().metadata [key] is Vacine)
					vacines.Add (key, GetGameData ().metadata [key] as Vacine);
			}
			return vacines;
		}

		public string GetCurrentPlatformVersion()
		{
			return MobUI.Instance.Version;
		}

	}
}
