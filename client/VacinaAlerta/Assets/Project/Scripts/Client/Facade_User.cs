using System;
using System.Linq;
using com.bighutgames.vacina.bo;
using com.bighutgames.vacina.bo.entities;
using System.Collections.Generic;
using UnityEngine;
using com.bighutgames.vacina.bo.game;
using com.bighutgames.vacina.application.util;

namespace inovapps.Client
{
	public partial class Facade 
	{
		#region General Info
		public User GetUser ()
		{
			return serverController.UserManager.getUser();
		}

		public Data GetUserData()
		{
			return serverController.UserManager.getUser().data;
		}
		#endregion General Info

		#region Registers
		public List<Register> GetRegisters()
		{
			List<Register> registers = new List<Register> ();
			foreach (var element in GetUserData().Registers) 
			{
				element.Value.id = element.Key;
				registers.Add (element.Value);
			}
			return registers;
		}
		public Register GetRegister(string id)
		{
			return Facade.Instance.GetUserData ().Registers[id];
		}


		public int GetCurrentDosage(string id, string vacineId)
		{
			Register register = GetRegister (id);
			if (register != null && register.doses.ContainsKey (vacineId))
				return register.doses [vacineId].Count;
			return 0;
		}

		public List<Vacine> GetRegisterPendencies(string id)
		{					
			Register register = GetRegister (id);
			List<Vacine> result = new List<Vacine> ();
			if (register != null)
			{
				int currentMonths = DateTime.UtcNow.TotalMonths (register.birth.FromMilliseconds());
				foreach (var element in Facade.Instance.GetVacines()) 
				{
					Vacine vacine = element.Value;

					bool isPendent = true;
                    //Check Period
                    isPendent &= (currentMonths >= vacine.start);// && currentMonths <= vacine.end);

					//Is Pregnant
					if(register.isPregnant)
						isPendent &= (vacine.pregnantAvailable);

					//Check Sex
					isPendent &= (register.isMale && vacine.male || !register.isMale && vacine.female);

					//Check Dose
					if (isPendent && register.doses.ContainsKey(vacine.IndexID)) 
					{
						List<long> doses = register.doses [vacine.IndexID];
						if (doses.Count != 0) 
						{
							int totalDoses = doses.Count;
							long lastDose = doses [doses.Count - 1];

							//Checkando dosagem periodica
							if (vacine.periodicDosage != 0) 
							{
								isPendent &= DateTime.UtcNow.TotalMonths (lastDose.FromMilliseconds ()) >= vacine.periodicDosage;
							} 
							//Checkando dosagem fixa
							else if (!string.IsNullOrEmpty (vacine.fixedDosage)) 
							{
								List<int> periods = vacine.fixedDosage.Split (',').Select (o => int.Parse (o)).ToList<int> ();
								if (totalDoses - 1 < periods.Count) 
								{
									int nextDose = periods [totalDoses - 1];
									isPendent &= DateTime.UtcNow.TotalMonths (lastDose.FromMilliseconds ()) >= nextDose;
								} 
								else 
								{
									isPendent = false;
								}
							}
                            else
                            {
                                isPendent = false;
                            }
						}
					}

					if (isPendent)
						result.Add (vacine);
				}
			}
			return result;
		}
		#endregion Registers
	}
    
    public static class DateTimeExtensions
    {
        public static int TotalMonths(this DateTime start, DateTime end)
        {
            return (start.Year * 12 + start.Month) - (end.Year * 12 + end.Month);
		}
		public static int TotalYears(this DateTime start, DateTime end)
		{
			return (start.Year) - (end.Year);
		}
    }
}
