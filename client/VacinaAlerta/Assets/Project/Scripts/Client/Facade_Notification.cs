using System;
using System.Linq;
using com.bighutgames.vacina.bo;
using com.bighutgames.vacina.bo.entities;
using System.Collections.Generic;
using com.bighutgames.vacina.bo.util;
using com.bighutgames.vacina.application.util;
using UnityEngine;
using com.bighutgames.vacina.bo.game;
using BigHut;

namespace inovapps.Client
{
	public enum NotificationCode
	{
		DailyReward,
		LifeRecharged,
		Gate
	}
	public partial class Facade 
	{
		#region Methods
		public void RegisterNotification()
		{
			NotificationManager.Instance.RegisterForNotifications ();
		}


		public void RefreshNotifications()
		{
			NotificationManager.Instance.CancelAll ();
			foreach(Register register in GetRegisters())
			{
				if (register != null)
				{
					int currentMonths = DateTime.UtcNow.TotalMonths (register.birth.FromMilliseconds());
					foreach (var element in Facade.Instance.GetVacines()) 
					{
						Vacine vacine = element.Value;

						bool isPendent = true;
						//Is Pregnant
						if(register.isPregnant)
							isPendent &= (vacine.pregnantAvailable);

						//Check Sex
						isPendent &= (register.isMale && vacine.male || !register.isMale && vacine.female);

						//Check Period
						//isPendent &= (currentMonths <= vacine.end);

						//Set Notifications
						if (isPendent) 
						{
							//Se nao tomou nenhuma dose
							if (!register.doses.ContainsKey (vacine.IndexID))
							{
								ScheduleNotification (register, vacine, register.birth.FromMilliseconds().AddMonths(vacine.start));
							} 
							else 
							{
								List<long> doses = register.doses [vacine.IndexID];
								if (doses.Count != 0) 
								{
									int totalDoses = doses.Count;
									long lastDose = doses [doses.Count - 1];

									//Checkando dosagem periodica
									if (vacine.periodicDosage != 0) 
									{
										ScheduleNotification (register, vacine, lastDose.FromMilliseconds ().AddMonths(vacine.periodicDosage));
									} 
									//Checkando dosagem fixa
									else if (!string.IsNullOrEmpty (vacine.fixedDosage)) 
									{
										string dosage = vacine.fixedDosage;
										List<int> periods = dosage.Replace("[", "").Replace("]", "").Split (',').Select (o => int.Parse (o)).ToList<int> ();
										if (totalDoses - 1 < periods.Count) 
										{
											int nextDose = periods [totalDoses - 1];
											ScheduleNotification (register, vacine, lastDose.FromMilliseconds ().AddMonths(nextDose));
										} 
									}
								}
							}
						}
					}
				}
			}
		}
		#endregion Methods

		#region Auxiliar Methods
		private void ScheduleNotification(Register register, Vacine vacine, DateTime date)
		{
			if (date < DateTime.UtcNow.AddDays (1))
				date = DateTime.UtcNow.AddDays (1);

			string message = string.Format ("{0} precisa tomar a vacina {1}", register.name, vacine.name);
			if(vacine.HasMultipleDosages)
				message = string.Format ("{0} precisa tomar a {1}º dosagem da vacina {2}", register.name, Facade.Instance.GetCurrentDosage(register.id, vacine.IndexID)+1, vacine.name);
			
            NotificationManager.Instance.ScheduleNotification (
				"Vacina pendente!!!", 
				message, 
				date, 
				-1);
		}
		#endregion Auxiliar Methods
	}
}
