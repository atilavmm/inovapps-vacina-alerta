using System;
using System.Linq;
using Facebook;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using BigHut;
using com.bighutgames.vacina.bo.util;
using BestHTTP;
using NewtonsoftUnity.Json;


#if UNITY_WINRT
using Prime31.WinPhoneSocialNetworking;
#else
using Facebook.Unity;
#endif

namespace inovapps.Client
{
	public class Request
	{
		public string Id;
		public Friend From;
		public string ActionType;
		public string Element;

		public bool IsSend
		{
			get
			{
				return ActionType.ToLower().Contains("send");
			}
		}

		public bool IsAsk
		{
			get
			{
				return ActionType.ToLower().Contains("ask");
			}
		}


		public bool IsLife
		{
			get
			{
				return Element.ToLower().Contains("life");
			}
		}

		public bool IsTicket
		{
			get
			{
				return Element.ToLower().Contains("ticket");
			}
		}

		public Request (string Id, string fromName, string fromId, string actionType, string element)
		{
			this.Id = Id;

			this.From = new Friend();
			this.From.name = fromName;
			this.From.fbId = fromId;

			this.ActionType = actionType;
			this.Element = element;
		}
	}


	public partial class Facade 
	{
		#region Login
		string[] read_permissions = new string[]{};
		string[] publish_permissions = new string[]{};
		string facebook_id = "513577685486924";
		string secret = "5e9c824e8bd60901d3e4c7ee33abc861";
		string redirect_url = "https://vacinaalerta.appspot.com/facebook/";
		string cachedFacebookId;

		public void InitializeSocial(Action callback)
		{
			#if UNITY_IOS || UNITY_ANDROID
			if(!FB.IsInitialized)
			{
			FB.Init (
			delegate()
			{
			callback();
			}
			);
			}
			else
			{
			FB.ActivateApp();
			callback();
			}
			#elif UNITY_WINRT
			FacebookAccess.applicationId = facebook_id;
			callback();
			#else
			callback();
			#endif

		}

		public void LoginSocial(Action success, Action fail)
		{
			CallFacebookUI(
				delegate()
				{
					#if UNITY_EDITOR || UNITY_WINRT
					LoginAll(success, fail);
					#else
					LoginRead (
					delegate 
					{
					LoginPublish(success, fail);
					}
					,
					fail
					);
					#endif
				}
			);
		}


		public void CallFacebookUI(Action method)
		{
			facebookCalls.Add (method);
		}

		private void LoginAll(Action success, Action fail)
		{
			List<string> all_permissions = new List<string>();
			all_permissions.AddRange(read_permissions);
			all_permissions.AddRange(publish_permissions);

			#if UNITY_WINRT
			FacebookAccess.login(
				redirect_url,
				secret, 
				all_permissions.ToArray(),
				delegate(string message, Exception exception) 
				{
					if (IsFBLogged())
					{
						if(success != null)
							success();
					}
					else
					{
						if(fail != null)
							fail();
					}
				}
			);
			#else
			FB.LogInWithPublishPermissions( 
			all_permissions.ToArray(), 
			delegate(ILoginResult resultRead)
			{
				if (string.IsNullOrEmpty(resultRead.Error) && IsFBLogged())
				{
					if(success != null)
						success();
				}
				else
				{
					if(fail != null)
						fail();
				}
			}
			);
			#endif
		}

		private void LoginRead(Action success, Action fail)
		{
			HasPermissions(read_permissions,
				delegate(bool status) 
				{
					if(!status)
					{
						#if UNITY_WINRT
						FacebookAccess.login(
							redirect_url,
							secret, 
							read_permissions,
							delegate(string message, Exception exception) {

								string m2 = message+"\n";
								if(exception != null)
									m2 = m2+exception.ToString();
								Debug.Log(m2);
								if (IsFBLogged())
								{
									if(success != null)
										success();
								}
								else
								{
									if(fail != null)
										fail();
								}
							});
						#else
						FB.LogInWithReadPermissions( 
						read_permissions, 
						delegate(ILoginResult result)
						{
						if (string.IsNullOrEmpty(result.Error) && IsFBLogged())
						{
						if(success != null)
						success();
						}
						else
						{
						if(fail != null)
						fail();
						}
						}
						);
						#endif
					}
					else
					{
						if(success != null)
							success();
					}
				}
			);
		}

		private void LoginPublish(Action success, Action fail)
		{
			HasPermissions(read_permissions,
				delegate(bool status) 
				{
					if(!status)
					{
						#if UNITY_WINRT
						FacebookAccess.login(
							redirect_url,
							secret, 
							publish_permissions,
							delegate(string message, Exception exception)
							{
								if (FacebookAccess.isSessionValid())
								{
									string m2 = message+"\n";
									if(exception != null)
										m2 = m2+exception.ToString();
									Debug.Log(m2);
									if(success != null)
										success();
								}
								else
								{
									if(fail != null)
										fail();
								}
							});
						#else
						FB.LogInWithPublishPermissions( 
						publish_permissions, 
						delegate(ILoginResult result)
						{
						if (string.IsNullOrEmpty(result.Error) && IsFBLogged())
						{
						if(success != null)
						success();
						}
						else
						{
						if(fail != null)
						fail();
						}
						}
						);
						#endif
					}
					else
					{
						if(success != null)
							success();
					}
				}
			);
		}

		private void HasPermissions(string[] permissions, Action<bool> callback)
		{
			#if UNITY_WINRT
			if(IsFBLogged())
			{
				Prime31.Facebook.instance.getSessionPermissionsOnServer(delegate(string status, List<string> sessionPermissions) {
					if(sessionPermissions!=null)
					{
						bool hasAll = true;
						foreach(string permission in permissions)
						{
							hasAll &= sessionPermissions.Contains(permission);
						}
						callback(hasAll);
						return;
					}
					callback(false);
					return;
				});
			}
			else
			{
				callback(false);
				return;
			}
			#else
			if(AccessToken.CurrentAccessToken != null)
			{
			bool hasAll = true;
			foreach(string permission in permissions)
			{
			hasAll &= AccessToken.CurrentAccessToken.Permissions.Contains(permission);
			}
			callback(hasAll);
			return;
			}
			callback(false);
			return;
			#endif
		}

		public void RefreshRequestList()
		{
			if(facebookCalls.Count > 0)
			{
				Action function = facebookCalls[0];
				facebookCalls.RemoveAt(0);
				function();
			}
		}
		#endregion Login

		#region Friend Picture
		public void LoadPictureFromID(string facebookId, Action<Texture2D> callback)
		{
			#if UNITY_WINRT
			if(FacebookAccess.isSessionValid())
			{
				Prime31.Facebook.instance.fetchProfileImageForUserId(facebookId,delegate(Texture2D obj) {
					if(callback != null)
						callback(obj);
				});
			}
			#else
			FB.API(
			facebookId+"/picture?type=square", 
			HttpMethod.GET,
			delegate(IGraphResult result)
			{
			if(callback != null)
			callback(result.Texture);
			}
			);
			#endif
		}

		public void LoadPictureFromURL(string url, Action<Texture2D> callback)
		{
			serverController.StartCoroutine(LoadPictureFromURLCoroutine(url, callback));
		}


		public IEnumerator LoadPictureFromURLCoroutine(string url, Action<Texture2D> callback)
		{
			WWW www = new WWW (url);
			yield return www;
			if (string.IsNullOrEmpty (www.error)) 
			{
				if (www.texture != null && callback != null)
					callback (www.texture as Texture2D);
			}
		}
		#endregion Friend Picture

		#region Friends Info
		private int lastInvitableFriendsCount = 0;
		public int GetInvitableFriendsCount()
		{
			RefreshInvitableFriendsCount();
			if(IsFBLogged())
				return lastInvitableFriendsCount;
			else
				return 0;
		}
		public void RefreshInvitableFriendsCount()
		{
			Facade.Instance.GetInvitableFriends(
				delegate(List<Friend> friends)
				{
					lastInvitableFriendsCount = friends.Count;
				}
			);
		}

		private int lastFriendsPlayingCount = 0;
		public int GetFriendsPlayingCount()
		{
			RefreshFriendsPlayingCount();
			if(IsFBLogged())
				return lastFriendsPlayingCount;
			else
				return 0;
		}

		public void RefreshFriendsPlayingCount()
		{
			#if UNITY_WINRT
			if(FacebookAccess.isSessionValid())
			{
				Prime31.Facebook.instance.getFriends(delegate(string arg1, FacebookFriendsResult arg2) {
					if(arg2!=null)
						lastFriendsPlayingCount = (int)(arg2.data.Count);
				});
			}
			#else
			Facade.Instance.GetFriendsLevelProgress(
				delegate(List<Friend> friends)
				{
					lastFriendsPlayingCount = friends.Count;
				}
			);
			#endif
		}



		public void GetInvitableFriends(Action<List<Friend>> success)
		{
			/*if(IsFBLogged())
			{
				#if UNITY_WINRT
				Prime31.Facebook.instance.graphRequest
				(
					"/"+GetFacebookAccessToken()+"/invitable_friends",
					Prime31.HTTPVerb.GET,
					delegate(string message, object data) 
					{
						Facebook.Unity.IGraphResult result = ConvertToGraphResult(message);
				#else
				FB.API (
				"/"+AccessToken.CurrentAccessToken.UserId+"/invitable_friends",
				HttpMethod.GET,
				delegate(IGraphResult result) 
				{
				#endif
						if(result != null && string.IsNullOrEmpty(result.Error))
						{
							Dictionary<string,object> rawData = (Dictionary<string,object>) BigHut.MiniJSON.jsonDecode(result.RawResult);
							if(rawData.ContainsKey("data"))
							{
								List<object> rawList = rawData["data"] as List<object>;

								List<Friend> friends = new List<Friend>();
								foreach(object element in rawList)
								{
									Dictionary<string,object> userData = element as Dictionary<string,object>;
									Friend friend = new Friend();
									friend.fbId = userData["id"].ToString();
									friend.name = DecodeHtmlChars(userData["name"].ToString());
									friend.url = userData.GetElement<string>("picture.data.url");

									friends.Add(friend);
								}

								if(success != null)
									success(friends);
							}
						}
					}
				);
			}*/
		}

		public void GetFriendsLevelProgress(Action<List<Friend>> success)
		{
			string token = Facade.Instance.GetFacebookAccessToken ();
			if(!string.IsNullOrEmpty(token))
			{
				Dictionary<string, string> data = new Dictionary<string, string>()
				{	
					{"accessToken", token}
				};
				serverController.StartCoroutine(ProcessGetFriends("user/friends/progress", data, success));
			}
		}

		private IEnumerator ProcessGetFriends(string method, Dictionary<string, string> parameters, Action<List<Friend>> success)
		{
			StringBuilder url = new StringBuilder();
			url.Append(method);
			if(parameters.Count > 0)
				url.Append("?");
			List<string> keys = new List<string>(parameters.Keys);
			for(int i = 0; i < parameters.Keys.Count;i++)
			{
				if(i > 0)
					url.Append("&");
				string key = keys[i];
				url.Append(key+"="+parameters[key]);
			}

			Dictionary<string,object> data = null;
			#if UNITY_WEBGL
			WWW request = serverController.GetRequest(url.ToString());
			yield return request;
			if(request.error == null)
			{
			data = (Dictionary<string,object>) BigHut.MiniJSON.jsonDecode(request.text);
			}
			#else
			HTTPRequest request = serverController.GetRequest(url.ToString());
			request.Send();
			yield return serverController.StartCoroutine(request);
			if(request.State == HTTPRequestStates.Finished && request.Response.IsSuccess)
			{
				data = (Dictionary<string,object>) BigHut.MiniJSON.jsonDecode(request.Response.DataAsText);
			}
			#endif

			if(data != null)
			{
				if(data.ContainsKey("items"))
				{
					string rawList = BigHut.MiniJSON.jsonEncode(data["items"]);
					Friend[] friendsList = JsonConvert.DeserializeObject<Friend[]>(rawList);
					if(success != null)
						success(new List<Friend>(friendsList));
				}
			}
		}
		#endregion Friends Info


		#region Helper
		public string GetFacebookAccessToken()
		{
			#if UNITY_WINRT
			return FacebookAccess.accessToken;
			#else
			if (AccessToken.CurrentAccessToken != null)
			return AccessToken.CurrentAccessToken.TokenString;
			return "";
			#endif
		}

		public void GetFacebookUserId(Action<string> callback)
		{

			if (!string.IsNullOrEmpty (cachedFacebookId)) 
			{
				Debug.Log("-----Load Cached Id");
				callback (cachedFacebookId);
			}
			else
			{
				#if UNITY_WINRT
				Debug.Log("-----P31Facebook.instance.getMe");
				Prime31.Facebook.instance.getMe(delegate(string arg1, FacebookMeResult arg2) {
					Debug.Log("Result:"+arg1);
					if(arg2!=null)
					{
						Debug.Log("Result:"+NewtonsoftUnity.Json.JsonConvert.SerializeObject(arg2));
						Debug.Log("-----Facebook ID:"+arg2.id);
						cachedFacebookId = arg2.id;
						callback(arg2.id);
					}
					else
					{
						Debug.Log("Null"+arg2);
						callback("");
					}
				});
				#else
				if (AccessToken.CurrentAccessToken != null)
				{
				cachedFacebookId = AccessToken.CurrentAccessToken.UserId;
				callback(AccessToken.CurrentAccessToken.UserId);
				}
				else
				{
				callback("");
				}
				#endif
			}
		}
		#endregion Helper

		#region Social Consistency
		public void CheckSocialConsistency(Action<bool> callback)
		{
			Debug.Log("-----CheckSocialConsistency");
			if (IsFBLogged ()) 
			{
				Debug.Log("-----FB Logged");
				GetFacebookUserId
				(
					delegate(string facebookId)
					{
						Debug.Log("-----GetFacebookUserId"+facebookId);
						callback (facebookId == GetUser ().facebookId);
					}
				);

			}
			else 
			{
				Debug.Log("-----FB Not Logged");
				callback (false);
			}
		}

		public bool IsFBLogged()
		{
			#if UNITY_WINRT
			return FacebookAccess.accessToken != null;
			#else
			return FB.IsLoggedIn;
			#endif
		}
		#endregion Social Consistency

		#region Auxiliar Methods
        /*
		public Facebook.Unity.GraphResult ConvertToGraphResult(string text)
		{
			Debug.Log ("ConvertToGraphResult " + text);
			if (!string.IsNullOrEmpty (text)) 
			{
				string error = null;
				string rawResult = null;
				if (!text.StartsWith ("{") && !text.StartsWith ("[")) {
					error = text;
				} else {
					rawResult = text;
				}
				return new Facebook.Unity.GraphResult (rawResult, error);
			}
			else 
			{
				return null;
			}
		}


		public Facebook.Unity.IAppRequestResult ConvertToAppRequestResult(string text)
		{
			Debug.Log ("ConvertToIAppRequestResult " + text);
			if (!string.IsNullOrEmpty (text)) 
			{
				string error = null;
				string rawResult = null;
				if (!text.StartsWith ("{") && !text.StartsWith ("[")) {
					error = text;
				} else {
					rawResult = text;
				}
				return new Facebook.Unity.AppRequestResult (rawResult, error);
			}
			else 
			{
				return null;
			}

		}*/
		
		public string DecodeHtmlChars(string aText)
		{
			string[] parts = aText.Split(new string[]{"&#x"}, StringSplitOptions.None);
			for (int i = 1; i < parts.Length; i++)
			{
				int n = parts[i].IndexOf(';');
				string number = parts[i].Substring(0,n);
				try
				{
					int unicode = Convert.ToInt32(number,16);
					parts[i] = ((char)unicode) + parts[i].Substring(n+1);
				} 
				catch 
				{
				}
			}
			return String.Join("",parts);
		}
		#endregion Auxiliar Methods
	}
}
