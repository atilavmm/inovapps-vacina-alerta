﻿using UnityEngine;
using System.Collections;
using Project.Popup;
using BigHut;
using UnityEngine.UI;
using inovapps.Client;
using inovapps.Client.Util;

#if UNITY_IOS || UNITY_ANDROID
using Facebook.Unity;
#endif

namespace Project.Login
{
	public class LoginController : MonoBehaviour 
	{
		#region Unity Fields
		public AudioClip Music;
		#endregion Unity Fields

		#region Fields
		private bool flagLoginClick;
		#endregion Fields

		#region Monobehaviour
		void Awake()
		{
			#if UNITY_EDITOR
			if(!Facade.Instance.IsInitialized)
			{
#if UNITY_IOS || UNITY_ANDROID
                FB.Init(delegate{});
#endif
				Facade.Instance.Initialize(ServerType.STAGE);
				Facade.Instance.Login(delegate{}, delegate(ServerException error) {}, null);
			}
#endif


                MusicManager.Instance.PlayMusic(Music, true);


			
			#if UNITY_WEBPLAYER
			OnLoginClick();
			#endif
		}

		void OnGUI()
		{
			if (flagLoginClick) 
			{
				flagLoginClick = false;

				GameObject instance = gameObject;
				Facade.Instance.LoginFlow(
					delegate 
					{
						Facade.Instance.LoadMain();
					} 
				);
			}
		}
		#endregion Monobehaviour

		void Update()
		{
			if(!PopupManager.Instance.HasPopupActived && Input.GetKeyDown(KeyCode.Escape))
			{
				PopupManager.Instance.ShowPopup<QuitGame>();
			}
		}

		#region Event Handlers
		public void OnLoginClick()
		{
			flagLoginClick = true;
		}
		#endregion Event Handlers
	}
}