﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using inovapps.Client;

namespace Project.Popup
{
	public class OutOfDate : BasePopup 
	{
		#region Fields
		private Action onConfirmClick;
		#endregion Fields
		
		#region Methods
		public void SetData(Action onConfirmClick = null)
		{
			this.onConfirmClick = onConfirmClick;
		}
		#endregion Methods
		
		
		#region Event Handlers
		public void OnConfirmButtonClick()
		{
			Destroy(gameObject);

			if(onConfirmClick != null)
				onConfirmClick();
		}
		
		public override void OnBackClick ()
		{
			
		}
		#endregion Event Handlers
	}
}