using UnityEngine;
using System.Collections;
using System;


namespace Project.Popup
{
	public class QuitGame : BasePopup 
	{
		
		#region Event Handlers
		public void OnContinueClick()
		{
			GameObject.Destroy(gameObject);
		}
		public void OnExitClick()
		{
			GameObject.Destroy(gameObject);
			Application.Quit ();
		}
		public override void OnBackClick ()
		{
			OnExitClick ();
		}
		#endregion Event Handlers
	}
}
