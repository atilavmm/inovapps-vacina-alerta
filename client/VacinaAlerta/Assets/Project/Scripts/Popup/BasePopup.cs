﻿using UnityEngine;
using System.Collections;
using System;
using BigHut;
using DG.Tweening;


namespace Project.Popup
{
	public class BasePopup : GenericPopup 
	{
		
		#region Unity Fields
		public AudioClip ShowSFX;
		public AudioClip HideSFX;
		#endregion Unity Fields

		public override void Show ()
		{
			base.Show ();
			Transform popup = transform.Find("Popup");
			popup.GetComponent<RectTransform>().localScale = Vector3.one*0.4f;
			MusicManager.Instance.PlaySound(ShowSFX);

			RectTransform rT = popup.GetComponent<RectTransform> ();

			Sequence sequence = DOTween.Sequence();
			
			sequence.Append(rT.DOScale(new Vector3(1f,1f,1f),0.16f).SetEase(Ease.Linear));
			sequence.Append(rT.DOScale(new Vector3(0.85f,1.35f,1f),0.09f).SetEase(Ease.Linear));
			sequence.Append(rT.DOScale(new Vector3(1.35f,0.85f,1f),0.09f).SetEase(Ease.Linear));
			sequence.Append(rT.DOScale(new Vector3(1f,1f,1f),0.09f).SetEase(Ease.Linear));
			sequence.Play();
		}

		public virtual IEnumerator Hide()
		{
			Transform popup = transform.Find("Popup");
			MusicManager.Instance.PlaySound(HideSFX);
			
			Sequence sequence = DOTween.Sequence();

			sequence.Append(popup.DOScale(new Vector3(0.5f,1.35f,1f),0.16f).SetEase(Ease.Linear));
			sequence.Append(popup.DOScale(new Vector3(0.1f,0.1f,0.1f),0.2f).SetEase(Ease.Linear));
			sequence.Play();
			yield return new WaitForSeconds(0.36f);
			popup.gameObject.SetActive(false);
		}
	}

}