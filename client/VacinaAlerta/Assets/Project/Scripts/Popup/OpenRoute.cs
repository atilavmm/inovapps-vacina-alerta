﻿using UnityEngine;
using System.Collections;
using System;


namespace Project.Popup
{
    public class OpenRoute : BasePopup
    {
        #region Fields
        private float userLat;
        private float userLon;
        private float targetLat;
        private float targetLon;
        #endregion Fields

        #region Methods
        public void SetData(float userLat, float userLon, float targetLat, float targetLon)
        {
            this.userLat = userLat;
            this.userLon = userLon;
            this.targetLat = targetLat;
            this.targetLon = targetLon;
        }
        #endregion Methods

        #region Event Handlers
        public void OnContinueClick()
        {
            string url = "https://maps.google.com/?saddr=" + userLat + "," + userLon + "&daddr=" + targetLat + "," + targetLon;
            Application.OpenURL(url);

            GameObject.Destroy(gameObject);
        }
        public void OnExitClick()
        {
            GameObject.Destroy(gameObject);
        }
        public override void OnBackClick()
        {
            GameObject.Destroy(gameObject);
        }
        #endregion Event Handlers
    }
}