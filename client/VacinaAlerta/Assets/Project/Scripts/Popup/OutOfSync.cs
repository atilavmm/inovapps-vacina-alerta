using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using inovapps.Client;

namespace Project.Popup
{
	public class OutOfSync : BasePopup 
	{
		#region Fields
		private Action onConfirmClick;
		private static int outOfSyncCount;
		#endregion Fields

		#region Monobehaviour
		void Awake()
		{
			outOfSyncCount++;
		}
		void OnDestroy()
		{
			outOfSyncCount--;
		}
		#endregion Monobehaviour

		#region Monobehaviour
		public static bool HasOutOfSync()
		{
			return outOfSyncCount != 0;
		}
		#endregion Monobehaviour

		#region Methods
		public void SetData(Action onConfirmClick = null)
		{
			this.onConfirmClick = onConfirmClick;
		}
		#endregion Methods
		
		
		#region Event Handlers
		public void OnConfirmButtonClick()
		{
			Facade.Instance.SolveOutOfSync ();

			Destroy(gameObject);

			if(onConfirmClick != null)
				onConfirmClick();
		}
		
		public override void OnBackClick ()
		{
			
		}
		#endregion Event Handlers
	}
}