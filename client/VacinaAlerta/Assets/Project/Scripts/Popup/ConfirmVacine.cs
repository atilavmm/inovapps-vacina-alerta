﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace Project.Popup
{
    public class ConfirmVacine : BasePopup
    {


        private InputField nameField;
        private InputField dayField;
        private InputField monthField;
        private InputField yearField;

        private Action<DateTime> confirm;
        private Action fail;

        public void SetConfirmation(Action<DateTime> confirm, Action fail)
        {
            dayField = transform.Find("Popup/Take/Day").GetComponent<InputField>();
            monthField = transform.Find("Popup/Take/Month").GetComponent<InputField>();
            yearField = transform.Find("Popup/Take/Year").GetComponent<InputField>();

            dayField.text = DateTime.Now.Day.ToString();
            monthField.text = DateTime.Now.Month.ToString();
            yearField.text = DateTime.Now.Year.ToString();

            this.confirm = confirm;
            this.fail = fail;
        }

        #region Event Handlers
        public void OnContinueClick()
        {
            int day = int.Parse(dayField.text);
            int month = int.Parse(monthField.text);
            int year = int.Parse(yearField.text);
            DateTime date = new DateTime(year, month, day);
            confirm(date);

            GameObject.Destroy(gameObject);
        }
        public void OnExitClick()
        {
            fail();
            GameObject.Destroy(gameObject);
        }
        public override void OnBackClick()
        {
            fail();
            GameObject.Destroy(gameObject);
        }
        #endregion Event Handlers
    }
}