﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


namespace Project.Popup
{
	public enum FacebookConnectionChangeType
	{
		Connected,
		Disconnected,
		Failed
	}

	public class FacebookConnectionChange : BasePopup
	{
		#region Fields
		private FacebookConnectionChangeType status;
		private Action onConfirmClick;
		#endregion Fields

		#region Methods
		public void SetData(FacebookConnectionChangeType status, Action onConfirmClick = null)
		{
			Text title = transform.Find("Popup/Title/Text").GetComponent<Text>();
			Text text = transform.Find("Popup/Content").GetComponent<Text>();

			switch (status) 
			{
			case FacebookConnectionChangeType.Connected:
				title.text = "Conectado com Sucesso";
				text.text = "Você conectou ao facebook com sucesso.";
				break;
			case FacebookConnectionChangeType.Disconnected:
				title.text = "Disconectado do Facebook";
				text.text = "Você desconectou ao facebook com sucesso.";
				break;
			case FacebookConnectionChangeType.Failed:
				title.text = "Falha ao Conectar";
				text.text = "Problema ao tentar conectar ao facebook.";
				break;
			}
			this.onConfirmClick = onConfirmClick;
		}
		#endregion Methods


		#region Event Handlers
		public void OnConfirmButtonClick()
		{
			switch (status) 
			{
			case FacebookConnectionChangeType.Connected:
				break;
			case FacebookConnectionChangeType.Disconnected:
				break;
			case FacebookConnectionChangeType.Failed:
				break;
			}
			Destroy(gameObject);
			if(onConfirmClick != null)
				onConfirmClick();
		}
		
		public override void OnBackClick ()
		{
			
		}
		#endregion Event Handlers


	}
}
