﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace Project.Popup
{
	public class FacebookLogging : BasePopup 
	{
		
		#region Fields
		private Text text;
		#endregion Fields

		void Awake()
		{
			text = transform.Find ("Popup/Content").GetComponent<Text> ();
			StartCoroutine (ConnectingAnimation ());
		}

		private IEnumerator ConnectingAnimation()
		{
			while(true)
			{
				text.text = string.Format("{0}","Conectando-se ao facebook");
				yield return new WaitForSeconds(0.3f);
				text.text = string.Format("{0}.","Conectando-se ao facebook");
				yield return new WaitForSeconds(0.3f);
				text.text = string.Format("{0}..","Conectando-se ao facebook");
				yield return new WaitForSeconds(0.3f);
				text.text = string.Format("{0}...","Conectando-se ao facebook");
				yield return new WaitForSeconds(0.3f);
			}
		}
		
		public override void OnBackClick ()
		{
			
		}
	}
}
