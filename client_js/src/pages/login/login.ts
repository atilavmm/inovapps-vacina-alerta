import { Component, OnInit  } from '@angular/core';

import { NavController,Platform } from 'ionic-angular';
import { SocialService } from '../../services/social.service';

@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class LoginPage implements OnInit  {

  constructor(private platform: Platform, private socialService:SocialService, private navCtrl: NavController) {

  }
  ngOnInit():void
  {
    this.checkIsLogged(); 
  }

  loginOnFacebook():void{
      this.socialService.login(
        () =>
        {
          console.log("Success");
          this.changeToMain();
        }
        ,
        (message) =>
        {
          console.log("Error "+message);
        }
      );
  }

  checkIsLogged():void{
    this.socialService.isLogged(
      logged =>
      {
        if(logged)
          this.changeToMain();
      }
    );
  }

  private changeToMain():void{
    this.navCtrl.pop();
    this.navCtrl.push("main-page");
  }
}
