import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { RSSService, New } from '../../services/rss.service';

@Component({
  selector: 'news',
  templateUrl: 'news.html'
})
export class NewsPage {

  private news:New[];

  constructor(public rssService:RSSService, public navCtrl: NavController) {

      this.rssService.getNews().subscribe(
          data => {
            console.log(JSON.stringify(data));
              this.news = data;
          }
      );
  }

}

