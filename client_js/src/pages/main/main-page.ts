import { Component  } from '@angular/core';

import { NavController,Platform } from 'ionic-angular';

import { HomePage } from '../home/home';
import { NewsPage } from '../news/news';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';

@Component({
  selector: 'main-page',
  templateUrl: 'main-page.html'
})
export class MainPage  {
  news: any = NewsPage;
  perfil: any = HomePage;
  vacinas: any = HomePage;
  postos: any = HomePage;

  constructor(private platform: Platform, private navCtrl: NavController) {

  }
}
