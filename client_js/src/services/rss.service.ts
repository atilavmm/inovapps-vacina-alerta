import { Injectable } from '@angular/core';
import { Facebook } from 'ionic-native';

import { Platform } from 'ionic-angular';
import { Observable }     from 'rxjs/Observable';
import {Http} from 'angular2/http';

@Injectable()
export class RSSService 
{
    constructor(private http: Http, private platform:Platform){

    }

    public getNews():Observable<New[]>{
        return this.http.get("http://portalsaude.saude.gov.br/index.php/cidadao/principal/agencia-saude?format=feed&type=rss")
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    
    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}

export class New
{
  public title:string;
  public message:string;
  public link:string;
}