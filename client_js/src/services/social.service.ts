import { Injectable } from '@angular/core';
import { Facebook } from 'ionic-native';

import { Platform } from 'ionic-angular';

@Injectable()
export class SocialService 
{
    constructor(private platform:Platform){

    }

    public login(success:Function, fail:Function):void{
        Facebook.login(['email']).then(
            (result) =>
            {
                console.log(result);
                if(result != null && result.status == "connected")
                    success();
                else
                    fail(result.status);
            }
        )
    }

    public isLogged(callback:Function):void{
        
        this.platform.ready().then(() => {
            Facebook.getLoginStatus().then(
                (result) =>
                {
                    if(result != null)
                        callback(result.status == "connected");
                    else
                        callback(false);
                }
            );
        });
    }
}