package com.bighutgames.vacina.exceptions;

import com.google.api.server.spi.response.ConflictException;

public class OutOfSync extends ConflictException {
	
	public OutOfSync(String message) {
		super(message);
	}
	
	@Override
	public String getMessage()
	{
		String back = super.getMessage() + "\n";
		for(StackTraceElement t : super.getStackTrace())
			back+="File: "+t.getFileName()+" Method:"+t.getMethodName()+"("+t.getLineNumber()+")\n";
		return back;
	}
	
	@Override
	public String getLocalizedMessage()
	{
		return getMessage();
	}

}
