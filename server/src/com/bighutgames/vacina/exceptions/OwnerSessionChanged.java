package com.bighutgames.vacina.exceptions;

import com.google.api.server.spi.response.UnauthorizedException;

public class OwnerSessionChanged extends UnauthorizedException {

	public OwnerSessionChanged() {
		super("Other device connected to the game, login again to play in this device.");
	}
	
	@Override
	public String getMessage()
	{
		String back = super.getMessage() + "\n";
		for(StackTraceElement t : super.getStackTrace())
			back+="File: "+t.getFileName()+" Method:"+t.getMethodName()+"("+t.getLineNumber()+")\n";
		return back;
	}
	
	@Override
	public String getLocalizedMessage()
	{
		return getMessage();
	}

}