package com.bighutgames.vacina.util.cache;

import java.util.Collections;

import javax.cache.Cache;
import javax.cache.CacheException;
import javax.cache.CacheManager;

public class CacheService {
	private static Cache instance;
	
	public static Cache getCache() throws CacheException {
		if(instance == null)
			instance = (Cache) CacheManager.getInstance().getCacheFactory().createCache(Collections.emptyMap());
		return instance;
	}

	
}
