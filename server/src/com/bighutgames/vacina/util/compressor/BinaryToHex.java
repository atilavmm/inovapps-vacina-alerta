package com.bighutgames.vacina.util.compressor;

public class BinaryToHex {

	public static String convertBinarytoHEX(String binary)
	{
		String back = "";
		while(!binary.isEmpty())
		{
			back += getHEXCode(binary.substring(0, 4));
			binary = binary.substring(4);
		}
		return back;
	}

	private static char getHEXCode(String binary)
	{
		switch(binary)
		{
			case "0000":
				return '0';
			case "0001":
				return '1';
			case "0010":
				return '2';
			case "0011":
				return '3';
			case "0100":
				return '4';
			case "0101":
				return '5';
			case "0110":
				return '6';
			case "0111":
				return '7';
			case "1000":
				return '8';
			case "1001":
				return '9';
			case "1010":
				return 'a';
			case "1011":
				return 'b';
			case "1100":
				return 'c';
			case "1101":
				return 'd';
			case "1110":
				return 'e';
			case "1111":
				return 'f';
		}
		return 'x';
	}

}
