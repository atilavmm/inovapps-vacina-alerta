package com.bighutgames.vacina.util.json;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.bighutgames.vacina.bo.*;

public class GsonService {

	private static GsonService singleton;
	public static GsonService getSingleton()
	{
		if(GsonService.singleton == null)
			GsonService.singleton = new GsonService();
		return GsonService.singleton;
	}
	
	
	private Gson gson;
	
	public GsonService() 
	{
		gson = new Gson();
	}

	public Gson getGson() {
		return gson;
	}
	
	public static Object fromJson(String json, Type typeOf) {
		return GsonService.getSingleton().getGson().fromJson(json, typeOf);
	}
	
	public static String toJson(Object object) {
		return GsonService.getSingleton().getGson().toJson(object);
	}
}
