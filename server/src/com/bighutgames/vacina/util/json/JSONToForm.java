package com.bighutgames.vacina.util.json;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bighutgames.vacina.util.formvalidator.FormValidator;
import com.google.common.base.Defaults;

public class JSONToForm {
	
	public final static String INPUT_FORMAT = 	"<div class=\"pure-control-group\"><label for=\"name\" style=\"width:22em;\">%s:</label><input name=\"%s\" id=\"%s\" type=\"%s\" value=\"%s\" %s></div>";

	public final static String HASHMAP = 
			"<div id=\"%sDiv\">"+
			"<input id=\"%s\" name=\"%s\" class=\"hidden-input\" type=\"hidden\" value=\'\'>"+
			"</div>"+
			"<script>"+
			"var %sJson = %s;"+
			"convertHashMapString($(\"#%sDiv\") ,%sJson);" +
			"</script>";
	
	// Field Name -- Form -- Field Name -- Field Name -- Field Name
	public final static String ADD_LIST =
			"<script>"+
			"function onAdd%s() {"+
			"	var html = \'%s\';"+
			"	$(\"#%s\").append(html);"+
			"	$(\"#commitbutton\").click();"+
			"}"+
			"function onRemove%s() {"+
			"	$(\"#%s\").find(\"#%d\").remove();"+
			"	$(\"#commitbutton\").click();"+
			"}"+
			"</script>";
	
	///////////////////////////////////////
	////////		FORM
	//////////////////////////////////////
	public static String classToForm(Object instance) throws IllegalArgumentException, IllegalAccessException, InstantiationException  {
		return classToForm("", instance);
	}
	private static String classToForm(String key, Object instance) throws IllegalArgumentException, IllegalAccessException, InstantiationException  {
		Class currentClass = instance.getClass();
		List<Field> fields = new ArrayList<Field>();
		while(currentClass != Object.class) {
			fields.addAll(Arrays.asList(currentClass.getDeclaredFields()));
			currentClass = currentClass.getSuperclass();
		}
		StringBuilder builder = new StringBuilder();
		for (Field field : fields) {
			field.setAccessible(true);
			Class<?> genericSubtype = null;
			try {
				ParameterizedType listType = (ParameterizedType) field.getGenericType();
				genericSubtype = (Class<?>) listType.getActualTypeArguments()[0];
			} catch(Exception e) {   
			}
			Object fieldInstance = field.get(instance);
			builder.append(fieldToForm(key,field.getName(), field.getType(), fieldInstance, genericSubtype));
		}
		return builder.toString();
	}

	public static String fieldToForm(String key, String fieldName,  Class<?> fieldClass, Object objInstance, Class<?> genericSubtype) throws IllegalArgumentException, IllegalAccessException, InstantiationException{
		
		StringBuilder builder = new StringBuilder();
		String fieldKey = fieldName;
		if(!key.equals(""))
			fieldKey = key+String.format("[%s]", fieldKey);
		
		if(isWrapperType(fieldClass)) {
			Class<?> primitiveType = fieldClass;
			String primitiveName = fieldName;
			Object obj = objInstance;

			builder.append(primitiveToForm(fieldKey, primitiveType, primitiveName,
					obj));
		} else if(Collection.class.isAssignableFrom(fieldClass)) {

			builder.append("<div id =\""+fieldKey+"\">");
			builder.append(String.format("<h2>%s</h2>", fieldName));
			List<?> obj = (List<?>)objInstance;
			
			Class<?> listClass = genericSubtype;
			
			for(int i = 0; i < obj.size();i++) {

				builder.append("<div id =\""+i+"\">");
				String hashFormKey = fieldKey+String.format("[%s]", i);
				if(isWrapperType(listClass))
					builder.append(primitiveToForm(hashFormKey, listClass, i+"",obj.get(i)));
				else
					builder.append(fieldToForm(fieldKey, i+"", genericSubtype, obj.get(i), null));

				builder.append("</div>");
				
			}
			
			// New Field
	        Object newInstance = null;
	        try {
	        	if(listClass == Integer.class)
	        		newInstance = new Integer(0);
	        	else
	        		newInstance = listClass.newInstance();
			} catch (InstantiationException e) {
			}
			String newFieldForm = "";
			String hashFormKey = fieldKey+String.format("[%s]", obj.size());
			if(isWrapperType(listClass))
				newFieldForm = primitiveToForm(hashFormKey, listClass, obj.size()+"",newInstance);
			else
				newFieldForm = fieldToForm(fieldKey, obj.size()+"", genericSubtype, newInstance, null);
			
			builder.append(String.format(ADD_LIST, fieldName, newFieldForm, fieldName, fieldName, fieldName, obj.size()-1));
			builder.append("</div>");
			builder.append("<button type=\"button\" class=\"pure-button button-secondary\" onclick=\"onAdd"+fieldName+"();\">Add</button>");
			builder.append("<button type=\"button\" class=\"pure-button button-warning\" onclick=\"onRemove"+fieldName+"();\">Remove</button>");
		} else if(Map.class.isAssignableFrom(fieldClass)) {
			HashMap obj = (HashMap)objInstance;
			String json = GsonService.toJson(obj);

			builder.append(String.format(HASHMAP, fieldName, fieldName, fieldName, fieldName, json, fieldKey, fieldKey));
			
		//else if(field.getType() == HashMap.class) {
		//	HashMap<String,Object> obj = (HashMap<String,Object>)field.get(instance);
		//	for (String hashKey : obj.keySet()) {
		//		String hashFormKey = fieldKey+String.format("[%s]", hashKey);
		//		builder.append(classToForm(hashFormKey,obj.get(hashKey)));
		//	}
		} else {
			Object obj = objInstance;
			System.out.println(fieldKey+" "+obj);
			builder.append("<div style=\"background-color:#F0F8FF;\">");
			builder.append("<h2>"+fieldName+"</h2>");
			builder.append(classToForm(fieldKey, obj));
			builder.append("</div>");
		}
			

		return builder.toString();
	}
	private static String primitiveToForm(String fieldKey,
			Class<?> primitiveType, String primitiveName, Object obj) {
		String type = "text";
		String misc = "";
		if(IsBoolean(primitiveType)) {
			type = "checkbox";
			if((Boolean)obj)
				misc = "checked";
			obj = new Boolean(true);
		} else if(IsNumber(primitiveType)) {
			type = "number";
		}
		return String.format(JSONToForm.INPUT_FORMAT, primitiveName, fieldKey, primitiveName, type, obj!=null?obj.toString():"",misc);
	}
	
	///////////////////////////////////////
	////////		RULES
	//////////////////////////////////////
	public static String classToRules(Object instance) throws IllegalArgumentException, IllegalAccessException  {
		Class currentClass = instance.getClass();
		List<Field> fields = new ArrayList<Field>();
		while(currentClass != Object.class) {
			fields.addAll(Arrays.asList(currentClass.getDeclaredFields()));
			currentClass = currentClass.getSuperclass();
		}
		HashMap<String, Object> hash = new HashMap<String, Object>();
		for (Field field : fields) {
			HashMap<String, Object> rules = fieldToRules(instance, field);
			if(rules.size() != 0)
				hash.put(field.getName(), rules);
		}
		return GsonService.toJson(hash);
	}
	
	public static HashMap<String, Object> fieldToRules(Object instance, Field field) throws IllegalArgumentException, IllegalAccessException{
		HashMap<String, Object> rules = new HashMap<String, Object>();
			
		if(field.isAnnotationPresent(com.bighutgames.vacina.util.formvalidator.FormValidator.class)) {
			FormValidator formValidator = field.getAnnotation(FormValidator.class);
			if(formValidator.isRequired())
				rules.put("required", true);
		}
		return rules;
	}
	

	
	///////////////////////////////////////
	////////		HELPER
	//////////////////////////////////////
	private static boolean isWrapperType(Class<?> clazz) {
		return clazz.equals(Boolean.class) || 
			clazz.equals(Integer.class) ||
			clazz.equals(Character.class) ||
			clazz.equals(Byte.class) ||
			clazz.equals(Short.class) ||
			clazz.equals(Double.class) ||
			clazz.equals(Long.class) ||
			clazz.equals(Float.class) ||
			clazz.equals(String.class) ||
			clazz.equals(Boolean.TYPE) ||
			clazz.equals(Integer.TYPE) ||
			clazz.equals(Long.TYPE) ||
			clazz.equals(Double.TYPE) ||
			clazz.equals(Float.TYPE);
	}
	
	private static boolean IsBoolean(Class<?> clazz) {
		return clazz.equals(Boolean.TYPE) || clazz.equals(Boolean.class);
	}
	
	private static boolean IsNumber(Class<?> clazz) {
		return clazz.equals(Long.TYPE) || clazz.equals(Long.class) || clazz.equals(Integer.TYPE) || clazz.equals(Integer.class);
	}
}
