package com.bighutgames.vacina.util.formvalidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.FIELD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface FormValidator {
	boolean isNumber() default false;
	int maxValue() default -1;
	int minValue() default -1;
	boolean isRequired() default false;
}
