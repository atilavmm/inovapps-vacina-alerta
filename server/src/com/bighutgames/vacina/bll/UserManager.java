package com.bighutgames.vacina.bll;

import java.lang.reflect.Type;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.cache.CacheException;

import org.datanucleus.metadata.MetaData;

import com.bighutgames.vacina.bo.entities.Data;
import com.bighutgames.vacina.bo.entities.Register;
import com.bighutgames.vacina.bo.game.Vacine;
import com.bighutgames.vacina.bo.util.ActionData;
import com.bighutgames.vacina.bo.util.ActionData.ActionType;
import com.bighutgames.vacina.bo.User;
import com.bighutgames.vacina.dal.UserDAO;
import com.bighutgames.vacina.exceptions.OutOfSync;
import com.bighutgames.vacina.exceptions.OwnerSessionChanged;
import com.bighutgames.vacina.util.json.GsonService;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.gdata.util.ServiceException;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.exception.FacebookJsonMappingException;
import com.restfb.json.JsonObject;

public class UserManager {

	public UserDAO dao;
	public Gson gson;

	public UserManager() {
		this.gson = new Gson();
		this.dao = new UserDAO();
	}

	public User getUser(Long userID, String facebookID, Long deviceId) throws CacheException{
		User user = null;
		try {
			if(facebookID != null)
			{
				user = this.dao.retrieve(facebookID);
			}
			else if(userID != null)
			{
				user = this.dao.retrieve(userID);
			}
			else
			{
				user = createNewUser(facebookID);
				this.dao.create(user);
			}
		} catch (EntityNotFoundException e) {
			user = null;
		}
		
		
		return user;
	}
	
	public void setUser(User user)	{
		this.dao.update(user);
	}

	public User login(Long userID, String facebookID, Long deviceId, String actions) throws CacheException, JsonSyntaxException, OutOfSync, OwnerSessionChanged, NoSuchAlgorithmException, ServiceException, EntityNotFoundException{
		User user = null;
		try {
			if(facebookID != null && !facebookID.isEmpty())
			{
				try {
					user = this.dao.retrieve(facebookID);
				} catch (EntityNotFoundException e) {
					user = this.dao.retrieve(userID);
					user.facebookId = facebookID;
					this.dao.update(user);
				}
				
			}
			else if(userID != null)
			{
				user = this.dao.retrieve(userID);
			}
			else
			{
				user = createNewUser(facebookID);
				this.dao.create(user);
			}
		} catch (EntityNotFoundException e) {
			user = createNewUser(facebookID);
			this.dao.create(user);
		}
		return executeActions(user.Id, actions);
	}

	public User createNewUser(String facebookID) throws CacheException
	{
		User user = new User();

		user.data = new Data();

		user.startTimeUTC = System.currentTimeMillis();

		user.facebookId = facebookID;
		
		return user;

	}

	public User executeActions(Long id, String actions) throws OutOfSync, EntityNotFoundException, OwnerSessionChanged, ServiceException, JsonSyntaxException, CacheException, NoSuchAlgorithmException{

		User user = this.dao.retrieve(id);

		ActionData[] actionList = GsonService.getSingleton().getGson().fromJson(actions, ActionData[].class);
		OutOfSync exception = null;
		
		for(ActionData action:actionList)
		{
			try
			{
				executeAction(user, action);
			}
			catch(OutOfSync e)
			{
				exception = e;
			}
		}
		
		this.dao.update(user);
		if(exception != null)
			throw exception;
		
		return user;
	}

	public void executeAction(User user,ActionData action) throws OutOfSync, ServiceException, JsonSyntaxException, CacheException{

		user.gameplayTime += action.gameplayGapTime;
		switch (ActionType.valueOf(action.type))
		{
		case DELETE_MEMBER:
			executeActionDELETE_MEMBER(user, action);
			break;
		case ADD_MEMBER:
			executeActionADD_MEMBER(user, action);
			break;
		case UPDATE_MEMBER:
			executeActionUPDATE_MEMBER(user,action);
			break;
		case VACINE:
			executeActionVACINE(user,action);
			break;
		default:
			throw new ServiceException("Invalid Argument: ActionType "+ action.type);
		}
	}

	private void executeActionDELETE_MEMBER(User user, ActionData action) throws CacheException {
		Type mapType = new TypeToken<Map<String, Object>>() {}.getType();
		Map<String, Object> map = GsonService.getSingleton().getGson().fromJson(action.data, mapType); 

		String id = map.get("id").toString();
		
		user.data.remove(id);
	}

	private void executeActionADD_MEMBER(User user, ActionData action) {
		Type mapType = new TypeToken<Map<String, Object>>() {}.getType();
		Map<String, Object> map = GsonService.getSingleton().getGson().fromJson(action.data, mapType); 

		String name = map.get("name").toString();
		boolean isMale = map.get("isMale").toString().toLowerCase().equals("true");
		boolean isPregnant = map.get("isPregnant").toString().toLowerCase().equals("true");
		long birth = Long.parseLong(map.get("birth").toString());
		
		user.data.register(name, birth, isMale,isPregnant);
		
	}

	private void executeActionUPDATE_MEMBER(User user, ActionData action) {
		Type mapType = new TypeToken<Map<String, Object>>() {}.getType();
		Map<String, Object> map = GsonService.getSingleton().getGson().fromJson(action.data, mapType); 

		String name = map.get("name").toString();
		boolean isMale = map.get("isMale").toString().toLowerCase().equals("true");
		boolean isPregnant = map.get("isPregnant").toString().toLowerCase().equals("true");
		long birth = Long.parseLong(map.get("birth").toString());
		String id = map.get("id").toString();
		
		Register r = user.data.Registers.get(id);
		r.name = name;
		r.birth = birth;
		r.isMale = isMale;
		r.isPregnant = isPregnant;
		
	}

	private void executeActionVACINE(User user, ActionData action) {
		Type mapType = new TypeToken<Map<String, Object>>() {}.getType();
		Map<String, Object> map = GsonService.getSingleton().getGson().fromJson(action.data, mapType); 

		String id = map.get("id").toString();
		String vacineId = map.get("vacineId").toString();
		
		if(!user.data.Registers.get(id).doses.containsKey(vacineId))
			user.data.Registers.get(id).doses.put(vacineId,new ArrayList<Long>());
		user.data.Registers.get(id).doses.get(vacineId).add(new Long(action.time));
		
	}
}
