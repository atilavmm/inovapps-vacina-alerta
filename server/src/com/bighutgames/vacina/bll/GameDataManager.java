package com.bighutgames.vacina.bll;

import java.io.IOException;
import java.util.HashMap;

import javax.cache.CacheException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import com.bighutgames.vacina.bo.Backup;
import com.bighutgames.vacina.bo.GameData;
import com.bighutgames.vacina.bo.game.Metadata;
import com.bighutgames.vacina.bo.game.Vacine;
import com.bighutgames.vacina.dal.GameDataDAO;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class GameDataManager {

	
	private GameDataDAO dao;
	private Gson gson;
	
	public GameDataManager() {
		this.dao = new GameDataDAO();
		this.gson = new Gson();
	}
	
	public void update() throws CacheException
	{
		long time = System.currentTimeMillis();
		GameData data = getGameData();
	}
	
	public GameData getGameData() throws CacheException {
		GameData gameData = dao.retrieve();
		if(gameData==null){
			dao.create();
			gameData = dao.retrieve();
		}
		gameData = checkDefaultBuild(gameData);
		return gameData;
	}
	
	private GameData checkDefaultBuild(GameData data) throws CacheException {
		
		return data;
	}
	
	public void createMetadata(String key, String jsonMetadata) throws CacheException, JsonSyntaxException, EntityNotFoundException{
		Metadata gamedata = Metadata.deserialize(jsonMetadata);
		GameData data = getGameData();
		data.metadata.put(key, gamedata);
		dao.update(data);
	}
	
	public Metadata readMetadata(String key) throws JsonSyntaxException, CacheException{
		return dao.retrieve().metadata.get(key);
	}
	
	public void updateMetadata(String key, String jsonMetadata) throws CacheException, EntityNotFoundException{
		createMetadata(key, jsonMetadata);
	}
	
	public void deleteMetadata(String key) throws CacheException, JsonSyntaxException, EntityNotFoundException, FactoryConfigurationError, XMLStreamException, IOException{
		GameData data = getGameData();
		data.metadata.remove(key);
		dao.update(data);
	}
	
	public void importMetadata(String json) throws CacheException, JsonSyntaxException, EntityNotFoundException {
		dao.update(GameData.deserialize(json));
	}
	
	public HashMap<String, Metadata> getListMetaData(String type) throws CacheException
	{
		GameData data = getGameData();
		HashMap<String, Metadata> filtered = new HashMap<String, Metadata>();
		for(String key : data.metadata.keySet())
		{
			Metadata metadata = data.metadata.get(key);
			System.out.println(metadata);
			
			String clazz = metadata.getClass().toString().toLowerCase();
			boolean isHabitat = clazz.contains("habitat");
			boolean isBuildingType = type.contains("building");
			
			if(
				metadata.getClass().toString().toLowerCase().contains(type)
				&&
				(!isBuildingType || (isBuildingType && !isHabitat))
				&&
				key.startsWith(type)
			)
			{
				filtered.put(key, data.metadata.get(key));
			}
		}
		return filtered;
	}
	
	public Backup getCompressedBackup() throws CacheException, FactoryConfigurationError, XMLStreamException, IOException, EntityNotFoundException{
		Backup backup = new Backup();
		backup.metaGameData = getGameData().serialize();
		return backup;
	}


	public Backup getBackup() throws CacheException, FactoryConfigurationError, XMLStreamException, IOException, EntityNotFoundException{
		Backup backup = new Backup();
		backup.metaGameData = getGameData().serialize();
		return backup;
	}
	
	public void loadBackup(Backup backup) throws CacheException {
		this.dao.update(GameData.deserialize(backup.metaGameData));
	}
}