package com.bighutgames.vacina.application;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;
import javax.cache.CacheException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.inject.Named;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import com.bighutgames.vacina.application.util.BHError;
import com.bighutgames.vacina.application.util.GenericMessage;
import com.bighutgames.vacina.application.util.LoginReturn;
import com.bighutgames.vacina.bll.UserManager;
import com.bighutgames.vacina.bo.User;
import com.bighutgames.vacina.exceptions.OutOfSync;
import com.bighutgames.vacina.exceptions.OwnerSessionChanged;
import com.bighutgames.vacina.util.json.GsonService;
import com.bighutgames.vacina.util.security.BHSecurity;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.gdata.util.ServiceException;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

@Api(
	    name = "vacinaserver",
	    version = "v1"
	)
public class VacinaServer {
	
	private UserManager userManager;
	private Gson gson;

	public VacinaServer() {
		super();
		userManager = new UserManager();
		gson = new Gson();
	}
	
	@ApiMethod(name="user.login",path="user/login", httpMethod = HttpMethod.POST)
	public GenericMessage login(@Named("userID") @Nullable Long id, @Named("facebookID")  @Nullable String facebook, @Named("deviceId")  @Nullable Long deviceId,@Named("actions") String actions,@Named("gameDataVersion") @Nullable Long gameDataVersion) throws ServiceException, JsonSyntaxException, OutOfSync, OwnerSessionChanged, EntityNotFoundException, KeyException, GeneralSecurityException, IOException
	{
		try {
			User user = userManager.login(id, facebook, deviceId, BHSecurity.decrypt(actions));
			return new GenericMessage(BHSecurity.encrypt(gson.toJson(new LoginReturn(user))));
		} catch (CacheException e) {
			throw new ServiceException("Internal error server: "+ e.getMessage());
		}
	}
	
	@ApiMethod(name="game.actions",path="game/actions", httpMethod = HttpMethod.POST)
	public GenericMessage syncronize(@Named("userID") Long id,@Named("actions") String actions) throws OutOfSync, EntityNotFoundException, OwnerSessionChanged, ServiceException, JsonSyntaxException, CacheException, KeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, GeneralSecurityException, IOException
	{
		return new GenericMessage(BHSecurity.encrypt(userManager.executeActions(id, BHSecurity.decrypt(actions)).serialize()));
	}
	
}
