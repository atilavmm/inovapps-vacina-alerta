package com.bighutgames.vacina.application.util;

public class TimeConverter {
	
	public static int timeSpanInSeconds(long time1,long time2)
	{
		return (int)((time1-time2)/1000);
	}
	
	public static long getMillisecondsDay()
	{
		return getMillisecondsHour()*24;
	}
	
	public static long getMillisecondsHour()
	{
		return 1000*3600;
	}

}
