package com.bighutgames.vacina.application.util;

import java.util.Date;

public class GenericMessage {
	
	private String message;
	private long time;
	private BHError error;

	public GenericMessage() {
		this.time = (new Date()).getTime();
	}
	
	public GenericMessage(String message) {
		this.time = (new Date()).getTime();
		this.message = message;
	}
	
	public GenericMessage(String message,BHError e) {
		this.time = (new Date()).getTime();
		this.message = message;
		this.error = e;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.time = (new Date()).getTime();
		this.message = message;
	}

	
	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
	
	public BHError getError() {
		return error;
	}

	public void setError(BHError error) {
		this.error = error;
	}


}
