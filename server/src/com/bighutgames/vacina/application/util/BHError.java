package com.bighutgames.vacina.application.util;

import com.bighutgames.vacina.exceptions.OutOfSync;

public class BHError {
	
	private String message;
	private ErrorType error;

	public enum ErrorType{
		NOT_ENOUGH_GOLD,LEVEL_BLOCKED,INVALID_LEVEL_TYPE,OTHERS
	}
	
	public BHError(ErrorType error, String message) {
		this.message = message;
		this.error = error;
	}
	
	public BHError() {
		// TODO Auto-generated constructor stub
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ErrorType getError() {
		return error;
	}

	public void setError(ErrorType error) {
		this.error = error;
	}


}
