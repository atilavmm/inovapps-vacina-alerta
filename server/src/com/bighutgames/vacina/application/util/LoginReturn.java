package com.bighutgames.vacina.application.util;

import com.bighutgames.vacina.bo.User;

public class LoginReturn {
	
	public String user;
	
	public LoginReturn() {
		// TODO Auto-generated constructor stub
	}
	
	public LoginReturn(User user) {
		this.user = user.serialize();
	}
	
	public User getUser(){
		return User.deserialize(this.user);
	}

}
