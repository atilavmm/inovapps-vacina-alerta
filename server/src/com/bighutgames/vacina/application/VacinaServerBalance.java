package com.bighutgames.vacina.application;

import java.io.IOException;
import java.util.HashMap;

import javax.annotation.Nullable;
import javax.cache.CacheException;
import javax.inject.Named;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import com.bighutgames.vacina.application.util.GenericMessage;
import com.bighutgames.vacina.bll.GameDataManager;
import com.bighutgames.vacina.bll.UserManager;
import com.bighutgames.vacina.bo.Backup;
import com.bighutgames.vacina.bo.User;
import com.bighutgames.vacina.bo.game.Metadata;
import com.bighutgames.vacina.util.json.GsonService;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.gson.JsonSyntaxException;


@Api(
	    name = "vacinaserver",
	    version = "v1"
	)
public class VacinaServerBalance {

	private UserManager userManager;
	private GameDataManager gameDataManager;

	public VacinaServerBalance() {
		super();
		userManager = new UserManager();
		gameDataManager = new GameDataManager();
	}
	
	@ApiMethod(name="databalance.create",path="databalance/create", httpMethod = HttpMethod.POST)
	public void create(@Named("id") String key,@Named("metadata") String jsonMetadata) throws CacheException, JsonSyntaxException, EntityNotFoundException{
		this.gameDataManager.createMetadata(key, jsonMetadata);
	}
	
	@ApiMethod(name="databalance.list",path="databalance/list", httpMethod = HttpMethod.GET)
	public HashMap<String,Metadata> get(@Named("type") String type) throws JsonSyntaxException, CacheException{
		return this.gameDataManager.getListMetaData(type);
	}
	
	@ApiMethod(name="databalance.read",path="databalance/read", httpMethod = HttpMethod.GET)
	public Metadata read(@Named("id") String key) throws JsonSyntaxException, CacheException{
		return this.gameDataManager.readMetadata(key);
	}
	
	@ApiMethod(name="databalance.update",path="databalance/update", httpMethod = HttpMethod.POST)
	public void update(@Named("id") String key,@Named("metadata") String jsonMetadata) throws CacheException, EntityNotFoundException{
		this.gameDataManager.updateMetadata(key, jsonMetadata);
	}
	
	@ApiMethod(name="databalance.delete",path="databalance/delete", httpMethod = HttpMethod.POST)
	public void delete(@Named("id") String key) throws CacheException, JsonSyntaxException, EntityNotFoundException, FactoryConfigurationError, XMLStreamException, IOException{
		this.gameDataManager.deleteMetadata(key);
	}
	
	@ApiMethod(name="databalance.export",path="databalance/export", httpMethod = HttpMethod.GET)
	public GenericMessage exportData() throws CacheException, JsonSyntaxException, EntityNotFoundException{
		return new GenericMessage(this.gameDataManager.getGameData().serialize());
	}
	
	@ApiMethod(name="databalance.import",path="databalance/import", httpMethod = HttpMethod.POST)
	public void importData(@Named("gameData") String gameData) throws CacheException, JsonSyntaxException, EntityNotFoundException{
		this.gameDataManager.importMetadata(gameData);
	}
	
	@ApiMethod(name="user.get",path="user/get", httpMethod = HttpMethod.GET)
	public GenericMessage getUser(@Named("id") @Nullable String id, @Named("facebookId") @Nullable String facebookId) throws CacheException{
		Long longId = null;
		if(id != null)
			longId = Long.parseLong(id);
		User user = userManager.getUser(longId, facebookId,0l);
		if(user == null)
			return new GenericMessage("");
		return new GenericMessage(user.serialize());
	}

	@ApiMethod(name="user.set",path="user/set", httpMethod = HttpMethod.POST)
	public void setUser(@Named("user") String raw){
		User user = User.deserialize(raw);
		if(user != null)
			userManager.setUser(user);
	}
	
	@ApiMethod(name="compressedbackup.get",path="compressedbackup/get", httpMethod = HttpMethod.GET)
	public Backup getCompressedBackup() throws CacheException, FactoryConfigurationError, XMLStreamException, IOException, EntityNotFoundException {
		return this.gameDataManager.getCompressedBackup();
	}	
	
	@ApiMethod(name="backup.get",path="backup/get", httpMethod = HttpMethod.GET)
	public Backup getBackup() throws CacheException, FactoryConfigurationError, XMLStreamException, IOException, EntityNotFoundException {
		return this.gameDataManager.getBackup();
	}

	@ApiMethod(name="backup.load",path="backup/load", httpMethod = HttpMethod.POST)
	public void loadBackup(@Named("backup") String jsonBackup) throws CacheException, FactoryConfigurationError, XMLStreamException, IOException, JsonSyntaxException, EntityNotFoundException{
		this.gameDataManager.loadBackup((Backup) GsonService.fromJson(jsonBackup,Backup.class));
	}
}
