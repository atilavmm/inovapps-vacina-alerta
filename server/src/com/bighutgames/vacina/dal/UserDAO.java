package com.bighutgames.vacina.dal;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.bighutgames.vacina.bo.User;
import com.bighutgames.vacina.bo.entities.Data;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Text;

public class UserDAO {
	
	public User retrieve(long id) throws EntityNotFoundException
	{
		Key key = KeyFactory.createKey("User", id);
		Entity userEntity = DatastoreServiceFactory.getDatastoreService().get(key);
		return getUser(userEntity);
	}
	
	public User retrieve(String facebookId) throws EntityNotFoundException
	{
		Query q = new Query("User");
		q.setFilter(new FilterPredicate("facebookId",Query.FilterOperator.EQUAL, facebookId));
		PreparedQuery pq = DatastoreServiceFactory.getDatastoreService().prepare(q);
		for (Entity result : pq.asIterable()) {
			return getUser(result);
		}
		throw new EntityNotFoundException(null);
	}
	
	public List<User> retrieve(Set<String> facebookIds){
		Query q = new Query("User");
		ArrayList<User> friends = new ArrayList<User>();
		if(facebookIds.size()>0)
		{
			q.setFilter(new FilterPredicate("facebookId", Query.FilterOperator.IN, facebookIds));
			PreparedQuery pq = DatastoreServiceFactory.getDatastoreService().prepare(q);
			for (Entity result : pq.asIterable()) {
				friends.add(getUser(result));
			}
		}
		return friends;
	}
	
	public void create(User user)
	{
		Entity userEntity = getEntity(user);
		DatastoreServiceFactory.getDatastoreService().put(userEntity);
		user.Id = userEntity.getKey().getId();
		DatastoreServiceFactory.getDatastoreService().put(userEntity);
	}
	
	public void update(User user)
	{
		
		Entity userEntity = getEntity(user, user.Id);
		DatastoreServiceFactory.getDatastoreService().put(userEntity);
	}

	public void delete(User user)
	{
		Entity userEntity = getEntity(user, user.Id);
		DatastoreServiceFactory.getDatastoreService().delete(userEntity.getKey());
	}
	
	private User getUser(Entity userEntity)
	{
		String facebookId = null;
		if(userEntity.getProperty("facebookId")!=null)
			facebookId = (String)userEntity.getProperty("facebookId");
		
		Long bighutId = null;
		if(userEntity.getProperty("bighutId")!=null)
			bighutId = (Long)userEntity.getProperty("bighutId");
		
		
		return new User(
				userEntity.getKey().getId(),
				facebookId,
				bighutId,
				Data.deserialize(((Text)userEntity.getProperty("data")).getValue())
				);
	}
	
	private Entity getEntity(User user)
	{
		return getEntity(user, null);
	}
	
	private Entity getEntity(User user,Long id)
	{
		Entity userEntity = null;
		if(id != null)
		{
			userEntity = new Entity("User", id);
			userEntity.setProperty("Id", id);
		}
		else
		{
			userEntity = new Entity("User");
			userEntity.setProperty("Id", user.Id);
		}
		userEntity.setProperty("bighutId",user.bighutId);
		userEntity.setProperty("facebookId",user.facebookId);
		userEntity.setProperty("data", new Text(user.data.serialize()));
		return userEntity;
	}

	public void reset() {
		Query q = new Query("User");
		PreparedQuery pq = DatastoreServiceFactory.getDatastoreService().prepare(q);

		for (Entity result : pq.asIterable()) {
			DatastoreServiceFactory.getDatastoreService().delete(result.getKey());
		}
	}
}
