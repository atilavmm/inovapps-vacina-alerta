package com.bighutgames.vacina.dal;

import javax.cache.CacheException;

import com.bighutgames.vacina.bo.GameData;
import com.bighutgames.vacina.util.cache.CacheService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.gson.JsonSyntaxException;

public class GameDataDAO {

	private long id = 1l;
	private static GameData instance;

	public void create() throws CacheException {
		if(!CacheService.getCache().containsKey("game_data")){
			try {
				Key key = KeyFactory.createKey("GameData", id);
				GameData data = getGameData(DatastoreServiceFactory.getDatastoreService().get(key));
				CacheService.getCache().put("game_data", data.serialize());
			} catch (EntityNotFoundException e) {
				save(new GameData());
			}
		}
	}

	public void update(GameData gamedata) throws CacheException {
		gamedata.version += 1;
		save(gamedata);	
	}

	public GameData retrieve() throws JsonSyntaxException, CacheException{
		if(instance==null)
		{
			create();
			instance = cacheLoad();
		}
		return instance;
	}

	private void save(GameData data) throws CacheException {
		DatastoreServiceFactory.getDatastoreService().put(getGameDataEntity(data));
		CacheService.getCache().put("game_data", data.serialize());
		instance = data;
	}

	private GameData cacheLoad() throws JsonSyntaxException, CacheException {
		GameData data = null;
		if(CacheService.getCache().containsKey("game_data"))
		{	
			data = GameData.deserialize(CacheService.getCache().get("game_data").toString());
		}
		return data;
	}

	private Entity getGameDataEntity(GameData data){
		Entity gameEntity = new Entity("GameData", id);
		gameEntity.setProperty("data",new Text(data.serialize()));
		return gameEntity;
	}

	private GameData getGameData(Entity entity){
		return GameData.deserialize(((Text)entity.getProperty("data")).getValue());
	}
	
}
