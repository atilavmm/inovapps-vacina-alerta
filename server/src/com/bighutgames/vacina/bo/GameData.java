package com.bighutgames.vacina.bo;

import java.util.HashMap;
import java.util.Hashtable;
import com.bighutgames.vacina.bo.game.Metadata;
import com.bighutgames.vacina.util.json.GsonService;
import com.google.gson.internal.LinkedTreeMap;

public class GameData  implements java.io.Serializable {
	
	public HashMap<String, Metadata> metadata;
	public int version;
	
	public GameData() {
		this.metadata = new HashMap<String, Metadata>();
		this.version = 0;
	}
	
	public static GameData deserialize(String json) {
		Hashtable rawData = (Hashtable) GsonService.fromJson(json, Hashtable.class);
		GameData gameData = (GameData) GsonService.fromJson(json, GameData.class);
	
		//Overriding Data
		LinkedTreeMap<String, Object> data = (LinkedTreeMap<String, Object>) rawData.get("metadata");
		for(String key : data.keySet()) {
			LinkedTreeMap<String, Object> elementHashMap = (LinkedTreeMap<String, Object>) data.get(key);
			String typeOf = (String)elementHashMap.get("class");
			Class<?> clazz;
			try {
				clazz = Class.forName(typeOf);
				gameData.metadata.put(key, (Metadata) GsonService.fromJson(GsonService.toJson(elementHashMap), clazz));
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return gameData;
	}
	
	public String serialize() {
		String json = GsonService.toJson(this);
		Hashtable rawData = (Hashtable) GsonService.fromJson(json, Hashtable.class);
		
		//Overriding Data
		LinkedTreeMap<String, Object> data = (LinkedTreeMap<String, Object>) rawData.get("metadata");
		for(String key : data.keySet()) {
			LinkedTreeMap<String, Object> elementHashMap = (LinkedTreeMap<String, Object>) data.get(key);
			elementHashMap.put("class", this.metadata.get(key).getClass().getName());
		}
		
		return GsonService.toJson(rawData);
	}

}
