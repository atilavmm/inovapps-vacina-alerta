package com.bighutgames.vacina.bo;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;

import com.bighutgames.vacina.bo.entities.Data;
import com.bighutgames.vacina.util.json.GsonService;

public class User {

	public long Id;
	public String facebookId;
	public long bighutId;
	public Data data;
	
	public long startTimeUTC;
	public long gameplayTime;
	
	public User(long Id,String facebookId,long bighutId,Data data)
	{
		this.Id = Id;
		this.facebookId = facebookId;
		this.bighutId = bighutId;
		this.data = data;
	}

	public User()
	{
		this.data = new Data();
	}
    
    public User(long Id) {
    	this.Id = Id;
    	this.data = new Data();
	}

	public static User deserialize(String json) {
		Hashtable rawData = (Hashtable) GsonService.fromJson(json, Hashtable.class);
		User user = (User) GsonService.fromJson(json, User.class);
		return user;

	}
	
	public String serialize() {
		
		String json = GsonService.toJson(this);
		Hashtable rawData = (Hashtable) GsonService.fromJson(json, Hashtable.class);
		return GsonService.toJson(rawData);
	}
	
	@Override
	public String toString()
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append("facebookId: ");
		buffer.append(this.facebookId);
		buffer.append(", Id: ");
		buffer.append(this.Id);
		buffer.append(", bighutId: ");
		buffer.append(this.bighutId);
		buffer.append(", data: [");
		buffer.append(this.data.toString());
		buffer.append("]");
		return buffer.toString().toUpperCase();
	}
	
	public String getCheckSum() throws NoSuchAlgorithmException
	{
		MessageDigest md5 = MessageDigest.getInstance("MD5");

        byte[] result = new byte[md5.getDigestLength()];
        md5.reset();
        md5.update(this.toString().getBytes());
        result = md5.digest();

        StringBuffer buf = new StringBuffer(result.length * 2);

        for (int i = 0; i < result.length; i++) {
            int intVal = result[i] & 0xff;
            if (intVal < 0x10) {
                buf.append("0");
            }
            buf.append(Integer.toHexString(intVal));
        }
		return buf.toString();
	}
}
