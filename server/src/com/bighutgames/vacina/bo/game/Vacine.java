package com.bighutgames.vacina.bo.game;

public class Vacine extends Metadata{
	
	public String name;
	public String description;
	public String prevents;
	public String indication;
	public String counterIndication;
	public String adverseEffect;
	
	public boolean male;
	public boolean female;
	
	public String fixedDosage;
	public int periodicDosage;
	public int start;
	public int end;
	
	public boolean pregnantAvailable;
}
