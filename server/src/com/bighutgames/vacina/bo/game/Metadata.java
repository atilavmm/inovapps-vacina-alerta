package com.bighutgames.vacina.bo.game;
import java.util.Hashtable;

import com.bighutgames.vacina.util.json.GsonService;

public class Metadata {
	
	public static Metadata deserialize(String json) {
		Hashtable rawData = (Hashtable) GsonService.fromJson(json, Hashtable.class);
		Metadata metaData = null;
		String typeOf = (String)rawData.get("class");
		Class<?> clazz;
		try {
			clazz = Class.forName(typeOf);
			metaData = (Metadata) GsonService.getSingleton().fromJson(json, clazz);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return metaData;

	}
	
	public String serialize() {
		String json = GsonService.toJson(this);
		Hashtable rawData = (Hashtable) GsonService.fromJson(json, Hashtable.class);
		rawData.put("class", this.getClass().getName());
		return GsonService.toJson(rawData);
	}

}