package com.bighutgames.vacina.bo.entities;

import java.util.HashMap;

import com.bighutgames.vacina.util.json.GsonService;

public class Data {
	
	public int currentRegister;
	public HashMap<String,Register> Registers;
	
	public Data() {
		this.currentRegister = 0;
    	this.Registers = new HashMap<String,Register>();
	}
	
	public void register(String name, long birth, boolean isMale,boolean isPregnant)
	{
		currentRegister++;
		this.Registers.put(currentRegister+"", new Register(name,birth,isMale,isPregnant));
	}
	
	public void remove(String Id)
	{
		this.Registers.remove(Id);
	}
	
	public static Data deserialize(String json) {
		return (Data) GsonService.fromJson(json, Data.class);

	}
	
	public String serialize() {
		return GsonService.toJson(this);
	}
	
	@Override
	public String toString()
	{
		StringBuffer buffer = new StringBuffer();
		return buffer.toString();
	}
    
}
