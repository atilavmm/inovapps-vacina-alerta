package com.bighutgames.vacina.bo.entities;

import java.util.HashMap;
import java.util.List;

public class Register {
	
	public String name;
	public long birth;
	public boolean isMale;
	public HashMap<String,List<Long>> doses;
	
	public boolean isPregnant;
	
	public Register()
	{
		this.doses = new HashMap<>();
	}
	
	public Register(String name, long birth, boolean isMale,boolean isPregnant)
	{
		this.name = name;
		this.birth = birth;
		this.isMale = isMale;
		this.isPregnant = isPregnant;
		this.doses = new HashMap<>();
	}
}
