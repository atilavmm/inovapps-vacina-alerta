package com.bighutgames.vacina.bo.util;


public class ActionData {
	
	public enum ActionType{
		DELETE_MEMBER,ADD_MEMBER,UPDATE_MEMBER,VACINE
	}
	
	public String type;
	public String data;
	public long time;
	public long gameplayGapTime;
	public String checkSumBefore;
	public String checkSum;
	
	public ActionData() {
	}
	
}
