var type = "sale";
var id = "";



function saleViewModel() 
{
	//Init
    var self = this;
    self.changed = ko.observable(false);
    var changeElement = function(){ self.changed(true); };
    self.id = ko.observable(id).extend({ prefixedString:"sale." });
    self.image =  ko.computed(function() {
		return GetSrcItem(self.id());
    }, this);
    self.sale = {
		monsterId: ko.observable(),
		levelMonster: ko.observable(0).extend({int:{}}),
		duration: ko.observable(0).extend({int:{}}),
		discount: ko.observable(0).extend({positiveFloat:{}}),
		extra: ko.observable(0).extend({int:{}}),
		storeId: ko.observable(),
		graphUrl: ko.observable(),
        price: new KoBank(0),
        amount: new KoBank(0)
	};
    self.isIAP = ko.computed(function() {
        var identifier = self.id();
		return identifier.indexOf("gold") != -1;
    }, this);
    
    function SetData(data)
    {
        if(data != undefined)
        {
            self.sale.monsterId(data.monsterId);
            self.sale.levelMonster(data.levelMonster);
            self.sale.duration(data.duration);
            self.sale.discount(data.discount);
            self.sale.extra(data.extra);
            self.sale.storeId(data.storeId);
            self.sale.graphUrl(data.graphUrl);
            self.sale.amount.set(data.amount.gold,data.amount.rum,data.amount.key);
            self.sale.price.set(data.price.gold,data.price.rum,data.price.key);
        }
        self.sale.monsterId.subscribe(changeElement);
        self.sale.levelMonster.subscribe(changeElement);
        self.sale.duration.subscribe(changeElement);
        self.sale.discount.subscribe(changeElement);
        self.sale.extra.subscribe(changeElement);
        self.sale.storeId.subscribe(changeElement);
        self.sale.graphUrl.subscribe(changeElement);
        self.sale.amount.subscribe(changeElement);
        self.sale.price.subscribe(changeElement);
    }
    $.getJSON("../_ah/api/vacinaserver/v1/databalance/read?id="+id, function(data) {
        SetData(data);
    });
   
    
   
    self.deleteElement = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
    	$.ajax({
            type: "POST",
            data: 
            { 
                id: self.id()
            },
            url: "/_ah/api/vacinaserver/v1/databalance/delete",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
                window.open("meta_list.jsp?type="+type, "_self");
                
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    };
    
    self.submit = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
        self.sale["class"] = "com.bighutgames.vacina.bo.game.meta.item.promotion.MetaSale";
    	$.ajax({
            type: "POST",
            data: 
            { 
                id: self.id(),
                metadata:ko.toJSON(self.sale)
            },
            url: "/_ah/api/vacinaserver/v1/databalance/create",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    }
}

$(document).ready(
	function()
	{
		id = getURLParameter('id');
        
		$('#game-elements-side-link').addClass("active");
		$('#game-elements-sale-list').parent().addClass("active");
		var temp = $('#game-elements-side-link').parent().find("ul");
		temp.show();

		//View Model
		ko.applyBindings(new saleViewModel());
	}
);