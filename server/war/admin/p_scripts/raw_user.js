var Id = "";
var facebookId = "";

function UserViewModel() 
{
	//Init
    var self = this;
    self.data = ko.observable("");
    self.changed = ko.observable(false);
    
    function getUser()
    {
        var baseURL = "../_ah/api/vacinaserver/v1/user/get?";
        if(!Id.isEmpty())
            baseURL += "id="+Id;
        if(!facebookId.isEmpty())
        {
            if(!facebookId.isEmpty())
                baseURL += "&";
            baseURL += "facebookId="+facebookId;
        }
            
        $.getJSON(baseURL, function(data) {
            var jsonData = jQuery.parseJSON( data.message );
            self.data(JSON.stringify(jsonData, null, "\t"));
            self.data.subscribe(function(){self.changed(true);});
        });
    }
   
    self.submit = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
    	$.ajax({
            
            type: "POST",
            data: 
            { 
            	user: self.data()
			},
            url: "/_ah/api/vacinaserver/v1/user/set",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    }
    
    getUser();
}

$(document).ready(
	function()
	{
		Id = getURLParameter('id');
        if(Id == null)
            Id = "";
        facebookId = getURLParameter('facebookId');
        if(facebookId == null)
            facebookId = "";
        
		$('#developer-side-link').addClass("active");
		$('#developer-user-list').parent().addClass("active");
		var temp = $('#developer-side-link').parent().find("ul");
		temp.show();

		//View Model
		ko.applyBindings(new UserViewModel());
	}
);