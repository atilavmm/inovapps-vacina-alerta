function MetaConstantsViewModel() 
{
    var self = this;
    self.started = false;
	//Changed
	self.changedGeneral = ko.observable(false);
	self.changedRewardDistribution = ko.observable(false);
	self.changedPowerup = ko.observable(false);
	self.changedCombos = ko.observable(false);
	self.changedRewards = ko.observable(false);
	self.changedFeed = ko.observable(false);
    
	self.rarities = function(i)
    {
        console.log(i+" "+RARITY_NAME);
        console.log(RARITY_NAME[i]);
        return RARITY_NAME[i]; 
    };
    
    self.levels = function(i)
    {
        var element = i;
        var gap = self.constants.evolveGap()-1;
        var min = (element+1)+i*gap;
        var max = min+gap;
        return min+"-"+max; 
    };
	//Data
    self.constants =
    {
        
        maxLives: ko.observable().extend({ int:{} }),
        rechargeLifeSeconds: ko.observable().extend({ int:{} }),
        evolveGap: ko.observable().extend({ int:{} }),
        gemToPowerUpBase: ko.observable().extend({ int:{} }),
        gemToCannonShot: ko.observable().extend({ int:{} }),
        cannonShotSize: ko.observable().extend({ int:{} }),
        endGameMultiplier: ko.observable().extend({ int:{} }),
        rumBountyParameter: ko.observable().extend({ positiveFloat:{} }),
        rumInBounty: ko.observable().extend({ int:{} }),
        timedRewardInMinutes: ko.observable().extend({ int:{} }),
        goldStart: ko.observable().extend({ int:{} }),
        rumStart: ko.observable().extend({ int:{} }),
        moreMoves: ko.observable().extend({ int:{} }),
        gemScore: ko.observable().extend({ int:{} }),
        isFacebookCanvasActive: ko.observable(),
        
        intervalVideoRewardBase: ko.observable().extend({ int:{} }),
        intervalVideoRewardFactor: ko.observable().extend({ positiveFloat:{} }),  
        
        intervalPromotionOfferBase: ko.observable().extend({ int:{} }),
        intervalPromotionOfferFactor: ko.observable().extend({ positiveFloat:{} }),  
        
        syncBufferSize: ko.observable().extend({ int:{} }),
        syncInterval: ko.observable().extend({ positiveFloat:{} }),  
                
                
        interstitialLevelGap: ko.observable().extend({ int:{} }),
        interstitialStartLevel: ko.observable().extend({ int:{} }),  

		attributesNormal: ko.observableArray(),
		attributesSuperNormal: ko.observableArray(),
		attributesRare: ko.observableArray(),
		attributesSuperRare: ko.observableArray(),
		attributesLegendary: ko.observableArray(),
        
        raiseTentacleInterval: ko.observable().extend({ int:{} }),

		levelsToLikeRate: ko.observable(),

        currentIOSVersion: ko.observable(),
        currentAndroidVersion: ko.observable(), 

        timedRewardBonus :
        {
            timeValueInit: ko.observable().extend({ int:{} }),
            timeValueInterval: ko.observable().extend({ int:{} }),
            timeValueFactor: ko.observable().extend({ positiveFloat:{} })
        }
        ,
        
        luckyDrawRewards: ko.observableArray(),
        dailyRewards: ko.observableArray(),  
        
        intervalVideoRewardFactor: ko.observableArray(),
        
        combos: 
        {
            score1: ko.observable().extend({ int:{} }),
            score2: ko.observable().extend({ int:{} }),
            score3: ko.observable().extend({ int:{} }),
            comboPowerup: ko.observable().extend({ int:{} })
        }
    };
    
    //Load Initial State
    $.getJSON("../_ah/api/vacinaserver/v1/dataconstants/get", function(constants) {
    	self.constants.maxLives(constants.maxLives);
    	self.constants.rechargeLifeSeconds(constants.rechargeLifeSeconds);
    	self.constants.evolveGap(constants.evolveGap);
    	self.constants.gemToPowerUpBase(constants.gemToPowerUpBase);
    	self.constants.gemToCannonShot(constants.gemToCannonShot);
    	self.constants.cannonShotSize(constants.cannonShotSize);
    	self.constants.endGameMultiplier(constants.endGameMultiplier);
    	self.constants.rumBountyParameter(constants.rumBountyParameter);
    	self.constants.rumInBounty(constants.rumInBounty);
    	self.constants.timedRewardInMinutes(constants.timedRewardInMinutes);
    	self.constants.goldStart(constants.goldStart);
    	self.constants.rumStart(constants.rumStart);
        self.constants.moreMoves(constants.moreMoves);
        self.constants.isFacebookCanvasActive(constants.isFacebookCanvasActive);

        self.constants.levelsToLikeRate(constants.levelsToLikeRate);
        
        self.constants.raiseTentacleInterval(constants.raiseTentacleInterval);
        
        self.constants.currentIOSVersion(constants.currentIOSVersion);
        self.constants.currentAndroidVersion(constants.currentAndroidVersion);
        
        self.constants.easier = constants.easier;
        
    	self.constants.timedRewardBonus.timeValueInit(constants.timedRewardBonus.timeValueInit);
        self.constants.timedRewardBonus.timeValueInterval(constants.timedRewardBonus.timeValueInterval);
        self.constants.timedRewardBonus.timeValueFactor(constants.timedRewardBonus.timeValueFactor);
        
    	self.constants.intervalVideoRewardBase(constants.intervalVideoRewardBase);
        self.constants.intervalVideoRewardFactor(constants.intervalVideoRewardFactor);
        
    	self.constants.syncBufferSize(constants.syncBufferSize);
        self.constants.syncInterval(constants.syncInterval);
        
    	self.constants.intervalPromotionOfferBase(constants.intervalPromotionOfferBase);
        self.constants.intervalPromotionOfferFactor(constants.intervalPromotionOfferFactor);
        
    	self.constants.interstitialLevelGap(constants.interstitialLevelGap);
        self.constants.interstitialStartLevel(constants.interstitialStartLevel);
        
    	self.constants.gemScore(constants.gemScore);
        
        if(constants.combos != undefined)
        {
            self.constants.combos.score1(constants.combos.score1);
            self.constants.combos.score2(constants.combos.score2);
            self.constants.combos.score3(constants.combos.score3);
            self.constants.combos.comboPowerup(constants.combos.comboPowerup);
        }
        
        if(constants.luckyDrawRewards != undefined)
        {
            for(var i = 0; i < constants.luckyDrawRewards.length;i++)
            {
                var rawReward = constants.luckyDrawRewards[i];
                self.addLuckyDrawRewards(rawReward.id, rawReward.type, rawReward.amount, rawReward.chance, rawReward.maxCount);
            }
        }
        
        if(constants.dailyRewards != undefined)
        {
            for(var i = 0; i < constants.dailyRewards.length;i++)
            {
                var rawReward = constants.dailyRewards[i];
                self.addDailyRewards(rawReward.id, rawReward.type, rawReward.amount, rawReward.chance, rawReward.maxCount);
            }
        }
        
        var feedTypes = ["attributesNormal", "attributesSuperNormal", "attributesRare", "attributesRare", "attributesSuperRare", "attributesLegendary"];

        for(var i = 0; i < feedTypes.length;i++)
        {
            var feedType = feedTypes[i];
            var remoteData = constants[feedType];
            
            var localArray = [];
            for(var c = 0; c < remoteData.length;c++)
            {
                var localData = 
                {
                    minRum:ko.observable().extend({ int:{} }),
                    maxRum:ko.observable().extend({ int:{} })
                };
                localData.minRum(remoteData[c].minRum);
                localData.maxRum(remoteData[c].maxRum);
                
                localData.minRum.subscribe(function(){self.changedFeed(true);});
                localData.maxRum.subscribe(function(){self.changedFeed(true);});
                
                localArray.push(localData);
            }
            
            self.constants[feedType](localArray);
        }
        
        self.constants.maxLives.subscribe(function(){self.changedGeneral(true);});
        self.constants.rechargeLifeSeconds.subscribe(function(){self.changedGeneral(true);});
        self.constants.evolveGap.subscribe(function(){self.changedGeneral(true);});
        self.constants.gemToPowerUpBase.subscribe(function(){self.changedGeneral(true);});
        self.constants.cannonShotSize.subscribe(function(){self.changedGeneral(true);});
        self.constants.endGameMultiplier.subscribe(function(){self.changedGeneral(true);});
        self.constants.rumBountyParameter.subscribe(function(){self.changedGeneral(true);});
        self.constants.rumInBounty.subscribe(function(){self.changedGeneral(true);});
        self.constants.gemToCannonShot.subscribe(function(){self.changedGeneral(true);});
        self.constants.timedRewardInMinutes.subscribe(function(){self.changedGeneral(true);});
        self.constants.gemScore.subscribe(function(){self.changedGeneral(true);});
        self.constants.gemScore.subscribe(function(){self.changedGeneral(true);});
        self.constants.goldStart.subscribe(function(){self.changedGeneral(true);});
        self.constants.rumStart.subscribe(function(){self.changedGeneral(true);});
        self.constants.moreMoves.subscribe(function(){self.changedGeneral(true);});
        self.constants.isFacebookCanvasActive.subscribe(function(){self.changedGeneral(true);});
        self.constants.levelsToLikeRate.subscribe(function(){self.changedGeneral(true);});
        self.constants.currentIOSVersion.subscribe(function(){self.changedGeneral(true);});
        self.constants.currentAndroidVersion.subscribe(function(){self.changedGeneral(true);});
        
        self.constants.raiseTentacleInterval.subscribe(function(){self.changedGeneral(true);});
        
        self.constants.intervalVideoRewardBase.subscribe(function(){self.changedGeneral(true);});
        self.constants.intervalVideoRewardFactor.subscribe(function(){self.changedGeneral(true);});
        
        self.constants.intervalPromotionOfferBase.subscribe(function(){self.changedGeneral(true);});
        self.constants.intervalPromotionOfferFactor.subscribe(function(){self.changedGeneral(true);});
        
        self.constants.syncBufferSize.subscribe(function(){self.changedGeneral(true);});
        self.constants.syncInterval.subscribe(function(){self.changedGeneral(true);});
        
    	self.constants.timedRewardBonus.timeValueInit.subscribe(function(){self.changedRewardDistribution(true);});
        self.constants.timedRewardBonus.timeValueInterval.subscribe(function(){self.changedRewardDistribution(true);});
        self.constants.timedRewardBonus.timeValueFactor.subscribe(function(){self.changedRewardDistribution(true);});
        
        
        self.constants.interstitialLevelGap.subscribe(function(){self.changedGeneral(true);});
        self.constants.interstitialStartLevel.subscribe(function(){self.changedGeneral(true);});

        self.constants.luckyDrawRewards.subscribe(function(){self.changedRewards(true);});
        self.constants.dailyRewards.subscribe(function(){self.changedRewards(true);});
        
        self.constants.combos.score1.subscribe(function(){self.changedCombos(true);});
        self.constants.combos.score2.subscribe(function(){self.changedCombos(true);});
        self.constants.combos.score3.subscribe(function(){self.changedCombos(true);});
        self.constants.combos.comboPowerup.subscribe(function(){self.changedCombos(true);});
        
        self.started = true
    });
    
    //Submit
    self.submit = function()
    {
        var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
        console.log(ko.toJSON(self.constants) );
    	$.ajax({
            
            type: "POST",
            data: 
            { 
            	metaConstants:ko.toJSON(self.constants) 
			},
             
            url: "/_ah/api/vacinaserver/v1/dataconstants/update",
            success: function(result)
            {
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
            	self.changedGeneral(false)
                self.changedRewardDistribution(false);
                self.changedPowerup(false);
                self.changedCombos(false);
                self.changedRewards(false);
                self.changedFeed(false);
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    	
    }
    
    self.addDailyRewards = function(id,type,amount,chance,maxCount)
    {
        if(id == undefined)
            id = "";
        if(type == undefined)
            type = "";

        if(amount == undefined)
            amount = 1;
        if(chance == undefined)
            chance = 1;
        if(maxCount == undefined)
            maxCount = 0;

        var dailyRewards = self.constants.dailyRewards();
        var dailyReward = 
            {
                id:ko.observable(id),
                type:ko.observable(type),
                amount:ko.observable(amount).extend({int:{}}),
                chance:ko.observable(chance).extend({int:{}}),
                maxCount:ko.observable(maxCount).extend({int:{}})
            };

        dailyRewards.push(dailyReward);
        self.constants.dailyRewards(dailyRewards);

        dailyReward.id.subscribe(function(){self.changedRewards(true);});
        dailyReward.type.subscribe(function(){self.changedRewards(true);});
        dailyReward.amount.subscribe(function(){self.changedRewards(true);});
        dailyReward.chance.subscribe(function(){self.changedRewards(true);});
        dailyReward.maxCount.subscribe(function(){self.changedRewards(true);});
    }
    self.removeDailyRewards = function()
    {
        var dailyRewards = self.constants.dailyRewards();
        dailyRewards.splice(dailyRewards.length-1,1);
        self.constants.dailyRewards(dailyRewards);
    }


    self.addLuckyDrawRewards = function(id,type,amount,chance,maxCount)
    {
        if(id == undefined)
            id = "";
        if(type == undefined)
            type = "";

        if(amount == undefined)
            amount = 1;
        if(chance == undefined)
            chance = 1;
        if(maxCount == undefined)
            maxCount = 0;

        var luckyDrawRewards = self.constants.luckyDrawRewards();
        var luckyDrawReward = 
            {
                id:ko.observable(id),
                type:ko.observable(type),
                amount:ko.observable(amount).extend({int:{}}),
                chance:ko.observable(chance).extend({int:{}}),
                maxCount:ko.observable(maxCount).extend({int:{}})
            };

        luckyDrawRewards.push(luckyDrawReward);
        self.constants.luckyDrawRewards(luckyDrawRewards);

        luckyDrawReward.id.subscribe(function(){self.changedRewards(true);});
        luckyDrawReward.type.subscribe(function(){self.changedRewards(true);});
        luckyDrawReward.amount.subscribe(function(){self.changedRewards(true);});
        luckyDrawReward.chance.subscribe(function(){self.changedRewards(true);});
        luckyDrawReward.maxCount.subscribe(function(){self.changedRewards(true);});
    }
    self.removeLuckyDrawRewards = function()
    {
        var luckyDrawRewards = self.constants.luckyDrawRewards();
        luckyDrawRewards.splice(luckyDrawRewards.length-1,1);
        self.constants.luckyDrawRewards(luckyDrawRewards);
    }
    
}


$(document).ready(
	function()
	{

		$('#general-side-link').addClass("active");
		$('#general-constants-link').parent().addClass("active");
		var temp = $('#general-side-link').parent().find("ul");
		temp.show();
		
		//Color Picker
		$('.colorpicker-rgba').colorpicker();
		
		//View Model
		ko.applyBindings(new MetaConstantsViewModel());
	}
);

