var type = "level";
var id = "";



function LevelViewModel() 
{
	//Init
    var self = this;
    self.changed = ko.observable(false);
    var changeElement = function(){ self.changed(true); };
    
    self.id = ko.observable(id).extend({ prefixedString:"level." });
    
    self.types = ko.observableArray(LEVEL_TYPES);
    self.difficultyTypes = ko.observableArray(DIFFICULTY_TYPES);
    
    self.level = {
		scoreTo3Star: ko.observable().extend({int:{}}),
		scoreTo2Star: ko.observable().extend({int:{}}),
		scoreTo1Star: ko.observable().extend({int:{}}),
		totalMoves: ko.observable().extend({int:{}}),
        type:ko.observable(""),
        tutorialId:ko.observable(""),
        
        difficulty:ko.observable(""),
        author: ko.observable(""),
        comment:ko.observable(""),
        
        gems: ko.observableArray(),
        
		unlockGateInSeconds: ko.observable().extend({int:{}}),
        price: new KoBank(0),
		version: ko.observable().extend({int:{}}),
        
        analytics:
        {
            fuuFactor: ko.observable().extend({float:{}}),
            moves: 
            {
                min: ko.observable().extend({float:{}}),
                max: ko.observable().extend({float:{}}),
                average: ko.observable().extend({float:{}}),
                variance: ko.observable().extend({float:{}}),
                sDev: ko.observable().extend({float:{}})
            },
            score: 
            {
                min: ko.observable().extend({float:{}}),
                max: ko.observable().extend({float:{}}),
                average: ko.observable().extend({float:{}}),
                variance: ko.observable().extend({float:{}}),
                sDev: ko.observable().extend({float:{}})
            },
            tries: 
            {
                min: ko.observable().extend({float:{}}),
                max: ko.observable().extend({float:{}}),
                average: ko.observable().extend({float:{}}),
                variance: ko.observable().extend({float:{}}),
                sDev: ko.observable().extend({float:{}})
            },
            heatmapCount:ko.observable(),
            logCount:ko.observable(),
            
            heatmapMatchRange:ko.observable(),
            heatmapDeathRange:ko.observable(),
            heatmapShipRange:ko.observable()
            
            
        }
	};

    self.addGem = function(t, p)
    {
        if(t == undefined)
            t = "";
        if(p == undefined)
            p = 1;
        
        var gems = self.level.gems();
        var gem = 
            { 
                gemType:ko.observable(t), 
                probability:ko.observable(p).extend({int:{}})
            };
        
        gem.icon = ko.computed(
                    function() {
                        return GetSrcGem(gem.gemType());
                    }, this);
        gems.push(gem);
        self.level.gems(gems);
        
        gem.gemType.subscribe(changeElement);
        gem.probability.subscribe(changeElement);
        
    }
    self.removeGem = function()
    {
        var gems = self.level.gems();
        gems.splice(gems.length-1,1);
        self.level.gems(gems);
    }
    
    self.raw = ko.observable("");
    
    self.size =  ko.computed(function() {
        
        var element = document.getElementById("canvas");
        if(element != null)
        {
            var parent = element.parentNode;
            parent.removeChild(element);
        }
        GenerateLevelImage(self.raw(), self.id(),200, false);
		return GetSrcItem(self.id());
    }, this);
    
    self.image =  ko.computed(function() {
        
        var element = document.getElementById("canvas");
        if(element != null)
        {
            var parent = element.parentNode;
            parent.removeChild(element);
        }
        GenerateLevelImage(self.raw(), self.id(),200, false);
		return GetSrcItem(self.id());
    }, this);
    
    self.typeIcon = ko.computed(function() {
        return 'p_icons/elements/'+self.level.type()+'.png';
    }, this);
    
    self.isGate = ko.computed(function() {
        var type = self.level.type();
		return type.indexOf("GATE") != -1;
    }, this);
    
    if(id == "new")
    {
        self.changed(true);
        SetData();
    }
    
    function SetData(data)
    {
        if(data != undefined)
        {
            self.level.scoreTo3Star(data.scoreTo3Star);
            self.level.scoreTo2Star(data.scoreTo2Star);
            self.level.scoreTo1Star(data.scoreTo1Star);
            
            self.level.difficulty(data.difficulty);
            self.level.comment(data.comment);
            self.level.author(data.author);

            
            self.level.totalMoves(data.totalMoves);
            
            self.level.tutorialId(data.tutorialId);
            
            self.level.type(data.type);
            self.level.unlockGateInSeconds(data.unlockGateInSeconds);
            self.level.price.set(data.price.gold);
                        
            if(data.gems != undefined)
            {
                for(var i = 0; i < data.gems.length;i++)
                {
                    var rawGem = data.gems[i];
                    self.addGem(rawGem.gemType, rawGem.probability);
                }
            }
            
            self.level.version(data.version);

        }
        self.level.scoreTo3Star.subscribe(changeElement);
        self.level.scoreTo2Star.subscribe(changeElement);
        self.level.scoreTo1Star.subscribe(changeElement);
        self.level.type.subscribe(changeElement);
        self.level.totalMoves.subscribe(changeElement);
        
        self.level.difficulty.subscribe(changeElement);
        self.level.comment.subscribe(changeElement);
        self.level.author.subscribe(changeElement);

        self.level.tutorialId.subscribe(changeElement);
        self.level.gems.subscribe(changeElement);
        self.level.unlockGateInSeconds.subscribe(changeElement);
        self.level.price.subscribe(changeElement);

        self.level.version.subscribe(changeElement);
    }
    $.getJSON("../_ah/api/vacinaserver/v1/databalance/read?id="+id, function(data) {
        SetData(data);
        RefreshAnalytics();
    });


    function SetAnalytics(data)
    {
        self.level.analytics.fuuFactor(data.fuuFactor);
        
        self.level.analytics.moves.min(data.moves.min);
        self.level.analytics.moves.max(data.moves.max);
        self.level.analytics.moves.average(data.moves.average);
        self.level.analytics.moves.variance(data.moves.variance);
        self.level.analytics.moves.sDev(data.moves.sDev);
        
        self.level.analytics.score.min(data.score.min);
        self.level.analytics.score.max(data.score.max);
        self.level.analytics.score.average(data.score.average);
        self.level.analytics.score.variance(data.score.variance);
        self.level.analytics.score.sDev(data.score.sDev);
        
        
        self.level.analytics.tries.min(data.tries.min);
        self.level.analytics.tries.max(data.tries.max);
        self.level.analytics.tries.average(data.tries.average);
        self.level.analytics.tries.variance(data.tries.variance);
        self.level.analytics.tries.sDev(data.tries.sDev);
        
        
        self.level.analytics.logCount(data.count);
        self.level.analytics.heatmapCount(data.heatCount);
        
        
        var levelSize = GetLevelSize(self.raw());
    
        var width = levelSize.width;
        var height = levelSize.height;
    
        var mask = GetArrayLevel(self.raw());
    
        SetHeatmap("heat_match", data.gemsHitArea, width, height, mask, self.level.analytics.heatmapMatchRange);
        SetHeatmap("heat_death", data.losePosition, width, height, mask, self.level.analytics.heatmapDeathRange);
        SetHeatmap("heat_ship", data.shipPositionArea, width, height, mask, self.level.analytics.heatmapShipRange);
    }
    
    function RefreshAnalytics()
    {
        $.getJSON("../_ah/api/vacinaserver/v1/analytic/level/get?id="+id, function(raw) {

            var data = JSON.parse(raw.message);
            SetAnalytics(data);
        });
    }

    $.ajax({
        type: "POST",
        data: 
        { 
            id: id
        },
        url: "/_ah/api/vacinaserver/v1/databalance/readlevel",
        success: function(result)
        {
            if(result.message != undefined)
                self.raw(result.message);
            self.raw.subscribe(changeElement);
            RefreshAnalytics();
        },
        error: function () 
        {
        }
    });
    self.deleteElement = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
    	$.ajax({
            type: "POST",
            data: 
            { 
                id: self.id()
            },
            url: "/_ah/api/vacinaserver/v1/databalance/delete",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
                window.open("meta_list.jsp?type="+type, "_self");
                
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    };
    
    self.submit = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
        self.level["class"] = "com.bighutgames.vacina.bo.game.meta.adventure.MetaLevel";
        if(self.id().length != "level.".length)
        {
	    	$.ajax({
	            type: "POST",
	            data: 
	            { 
	                id: self.id(),
	                metadata:ko.toJSON(self.level)
	            },
	            url: "/_ah/api/vacinaserver/v1/databalance/create",
	            success: function(result)
	            {
	            	self.changed(false);
	                var unique_id = $.gritter.add({title: 'Data Synced 1/2',text: 'Synced Meta Data', sticky: false, class_name: 'my-sticky-class'});
	                
	                $.ajax({
	                    type: "POST",
	                    data: 
	                    { 
	                        id: self.id(),
	                        levelsource:self.raw()
	                    },
	                    url: "/_ah/api/vacinaserver/v1/databalance/updatelevel",
	                    success: function(result)
	                    {
	                        self.changed(false);
	                        var unique_id = $.gritter.add({title: 'Data Synced 2/2',text: 'Synced Level XML', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
	                    },
	                    error: function () {
	                        var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	                    }
	                });
	            },
	            error: function () {
	                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
		        }
	        });
        }
    }
        
}

$(document).ready(
	function()
	{
		id = getURLParameter('id');
		$('#game-elements-side-link').addClass("active");
		$('#game-elements-'+type+'-list').parent().addClass("active");
		var temp = $('#game-elements-side-link').parent().find("ul");
		temp.show();

		//View Model
		ko.applyBindings(new LevelViewModel());
	}
);

//Loading HeatMaps
function SetHeatmap(id, img, width, height, mask, range)
{
    var element = document.getElementById(id);
    
    var parent = element.parentNode;
    parent.removeChild(element);
    
    var canvas = document.createElement("canvas");
    canvas.id = id;
    $(canvas).addClass("canvas-level");
    
    var ctx = canvas.getContext("2d");
    ctx.canvas.width  = width;
    ctx.canvas.height = height;
    
    $( canvas ).css( "width", 350, "height", 350);

    var max = Math.max.apply(null, img);
    var min = Math.min.apply(null, img);
    
    var rawImage = [];
    for(var i = 0; i < img.length;i++)
    {
        var value = parseInt(img[i]);
        var maskValue = parseInt(mask[i]);
        if(maskValue > 0 && mask != 212)
        {
            var color = jet_color2(value/max);
            if(max == min)
                color = jet_color2(0);
            rawImage.push(color.red*256);
            rawImage.push(color.green*256);
            rawImage.push(color.blue*256);
        }
        else
        {
            rawImage.push(0);
            rawImage.push(0);
            rawImage.push(0);
            
        }
        rawImage.push(255);
    }
    range("Range: ["+min+"-"+max+"]");
    
    var palette = ctx.getImageData(0,0,width,height);
    palette.data.set(new Uint8ClampedArray(rawImage));
    ctx.putImageData(palette,0,0);
    
    parent.appendChild(canvas);
}