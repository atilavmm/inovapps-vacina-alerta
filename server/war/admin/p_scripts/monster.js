var type = "monster";
var id = "";


function MonsterAttributeUpgrade(evolution, minRum, maxRum, subscribe)
{
    this.icon = GetSrcItem(id, evolution);
    
    this.minRum = ko.observable(minRum).extend({int:{}});
    this.maxRum = ko.observable(maxRum).extend({float:{}});
    
    this.subscribe = function(s)
    {
        this.minRum.subscribe(s);
        this.maxRum.subscribe(s);
    };
    
    this.subscribe(subscribe);
    
}

function MonsterViewModel() 
{
	//Init
    var self = this;
    self.changed = ko.observable(false);
    var changeElement = function(){ self.changed(true); };
    self.id = ko.observable(id).extend({ prefixedString:"monsters." });
    self.image =  ko.computed(function() {
		return GetSrcItem(self.id());
    }, this);
    self.elements = ko.observableArray(ELEMENTS);
    self.rarities = ko.observableArray(RARITY);
    self.powerups = ko.observableArray(POWERUPS);
    self.powerLevels = ko.observableArray(POWER_LEVEL);
    self.monster = {
		type:ko.observable(0),
		powerUpType:ko.observable(0),
		rarityType:ko.observable(0),
        
		price:new KoBank(0,0),
		sellValue:new KoBank(0,0),
		discount: ko.observable(0).extend({positiveFloat:{}})
	};
    self.typeIcon = ko.computed(function() {
        return 'p_icons/elements/'+self.monster.type()+'.png';
    }, this);
    self.powerUpTypeIcon = ko.computed(function() {
        return 'p_icons/powerups/'+self.monster.powerUpType()+'.png';
    }, this);
    self.rarityIcon = ko.computed(function() {
        return 'p_icons/rarity/'+self.monster.rarityType()+'.png';
    }, this);
    
    if(id == "new")
    {
        self.changed(true);
        SetData();
    }
    
    function SetData(data)
    {
        if(data != undefined)
        {
            
            
            self.monster.type(data.type);
            self.monster.powerUpType(data.powerUpType);
            self.monster.rarityType(data.rarityType);
            
            self.monster.price.set(data.price.gold,data.price.gem);
            self.monster.sellValue.set(data.sellValue.gold,data.sellValue.gem);

            self.monster.discount(data.discount);
        }
        self.monster.type.subscribe(changeElement);
        self.monster.powerUpType.subscribe(changeElement);
        self.monster.rarityType.subscribe(changeElement);
        
        self.monster.price.subscribe(changeElement);
        self.monster.sellValue.subscribe(changeElement);
        self.monster.discount.subscribe(changeElement);
    }
    $.getJSON("../_ah/api/vacinaserver/v1/databalance/read?id="+id, function(data) {
        SetData(data);
    });
   
    self.deleteElement = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
    	$.ajax({
            type: "POST",
            data: 
            { 
                id: self.id()
            },
            url: "/_ah/api/vacinaserver/v1/databalance/delete",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
                window.open("meta_list.jsp?type="+type, "_self");
                
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    };
    
    self.submit = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
        self.monster["class"] = "com.bighutgames.vacina.bo.game.meta.item.MetaMonster";
    	$.ajax({
            type: "POST",
            data: 
            { 
                id: self.id(),
                metadata:ko.toJSON(self.monster)
            },
            url: "/_ah/api/vacinaserver/v1/databalance/create",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    }
}

$(document).ready(
	function()
	{
		id = getURLParameter('id');
		$('#game-elements-side-link').addClass("active");
		$('#game-elements-monster-list').parent().addClass("active");
		var temp = $('#game-elements-side-link').parent().find("ul");
		temp.show();

		//View Model
		ko.applyBindings(new MonsterViewModel());
	}
);