function LevelEasierViewModel() 
{
    var self = this;
    self.started = false;
	//Changed
	self.changedTries = ko.observable(false);
	self.changedTime = ko.observable(false);
    
	//Data
    self.rawConstants = null;
    self.constants =
    {
        easier:
        {
            gemsParameter: ko.observableArray()
            ,
            gems: 
            {
                loseValueInit:
                {
                    min:ko.observable().extend({ int:{} }),
                    max:ko.observable().extend({ int:{} })
                }
                ,
                loseValueInterval:
                {
                    min:ko.observable().extend({ int:{} }),
                    max:ko.observable().extend({ int:{} })
                }
                ,
                loseValueFactor:
                {
                    min:ko.observable().extend({ positiveFloat:{} }),
                    max:ko.observable().extend({ positiveFloat:{} })
                }
                ,
                timeValueInit:
                {
                    min:ko.observable().extend({ int:{} }),
                    max:ko.observable().extend({ int:{} })
                }
                ,
                timeValueInterval:
                {
                    min:ko.observable().extend({ int:{} }),
                    max:ko.observable().extend({ int:{} })
                }
                ,
                timeValueFactor:
                {
                    min:ko.observable().extend({ positiveFloat:{} }),
                    max:ko.observable().extend({ positiveFloat:{} })
                }
            }
            ,
            maxTimeFactor:ko.observable().extend({ int:{} }),
            maxTryFactor:ko.observable().extend({ int:{} })
        }
    };
    
    //Load Initial State
    $.getJSON("../_ah/api/vacinaserver/v1/dataconstants/get", function(constants)
    {
        self.rawConstants = constants;
        
        if(constants.easier != undefined)
        {
            self.constants.easier.maxTimeFactor(constants.easier.maxTimeFactor);
            self.constants.easier.maxTimeFactor.subscribe(function(){self.changedTime(true);});
                
            self.constants.easier.maxTryFactor(constants.easier.maxTryFactor);
            self.constants.easier.maxTryFactor.subscribe(function(){self.changedTries(true);});
            
            var MAX = 5000000;
            var MIN = -MAX;
            for (var key in self.constants.easier.gems)
            {
                self.constants.easier.gems[key].min(MAX);
                self.constants.easier.gems[key].max(MIN);
            }
            
            //Process Gem Parameters
            if(constants.easier.gemsParameter != undefined)
            {
                
                for (var key in self.constants.easier.gems)
                {
                    self.constants.easier.gems[key].min(MAX);
                    self.constants.easier.gems[key].max(MIN);
                    
                    for(var i = 0; i < constants.easier.gemsParameter.length; i++)
                    {
                        var gem = constants.easier.gemsParameter[i];
                        
                        var min = Math.min(self.constants.easier.gems[key].min(), gem[key]);
                        self.constants.easier.gems[key].min(min);   

                        var max = Math.max(self.constants.easier.gems[key].max(), gem[key]);
                        self.constants.easier.gems[key].max(max);
                    }
                    

                    if(key.indexOf("time") != -1)
                    {
                        self.constants.easier.gems[key].min.subscribe(function(){self.changedTime(true);});
                        self.constants.easier.gems[key].max.subscribe(function(){self.changedTime(true);});
                    }
                    else
                    {
                        self.constants.easier.gems[key].min.subscribe(function(){self.changedTries(true);});
                        self.constants.easier.gems[key].max.subscribe(function(){self.changedTries(true);});
                    }
                    
                }
            }
        }
        
        
    });
    
    //Submit
    self.submit = function()
    {
        var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
        
        //Pass Info
        if(self.rawConstants.easier == undefined)
            self.rawConstants.easier = {};
        
        self.rawConstants.easier.maxTimeFactor = self.constants.easier.maxTimeFactor();
        self.rawConstants.easier.maxTryFactor = self.constants.easier.maxTryFactor();
        
        self.rawConstants.easier.gemsParameter = [];
        for(var i = 0; i < 5;i++)
        {
            var value = (i/4.0);
            var alpha = 1-(value * value * (3.0 - 2.0 * value));
            
            var parameter = {};
            for(var key in self.constants.easier.gems)
            {
                var min = self.constants.easier.gems[key].min();
                var max = self.constants.easier.gems[key].max();
                if(key.indexOf("Factor") != -1)
                    parameter[key] = (max-min)*alpha+min;
                else
                    parameter[key] = Math.floor((max-min)*alpha+min);
            }
            
            self.rawConstants.easier.gemsParameter.push(parameter);
        }
        
        console.log(ko.toJSON(self.rawConstants.easier));
        
    	$.ajax({
            
            type: "POST",
            data: 
            { 
            	metaConstants:ko.toJSON(self.rawConstants) 
			},
             
            url: "/_ah/api/vacinaserver/v1/dataconstants/update",
            success: function(result)
            {
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
            	self.changedTries(false)
                self.changedTime(false);
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    	
    }
    
}

$(document).ready(
	function()
	{

		$('#general-side-link').addClass("active");
		$('#general-leveleasier-link').parent().addClass("active");
		var temp = $('#general-side-link').parent().find("ul");
		temp.show();
		
		//Color Picker
		$('.colorpicker-rgba').colorpicker();
		
		//View Model
		ko.applyBindings(new LevelEasierViewModel());
	}
);