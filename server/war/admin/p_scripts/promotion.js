var type = "promotion";
var id = "";



function promotionViewModel() 
{
	//Init
    var self = this;
    self.changed = ko.observable(false);
    var changeElement = function(){ self.changed(true); };
    self.id = ko.observable(id).extend({ prefixedString:"promotion." });
    self.image =  ko.computed(function() {
		return GetSrcItem(self.id());
    }, this);
    self.promotion = {
		monsterId: ko.observable(),
		levelMonster: ko.observable(0).extend({int:{}}),
		duration: ko.observable(0).extend({int:{}}),
		discount: ko.observable(0).extend({positiveFloat:{}}),
		extra: ko.observable(0).extend({int:{}}),
		storeId: ko.observable(),
		graphUrl: ko.observable(),
        price: new KoBank(0),
        amount: new KoBank(0)
	};
    self.isIAP = ko.computed(function() {
        var identifier = self.id();
		return identifier.indexOf("gold") != -1;
    }, this);
    
    function SetData(data)
    {
        if(data != undefined)
        {
            self.promotion.monsterId(data.monsterId);
            self.promotion.levelMonster(data.levelMonster);
            self.promotion.duration(data.duration);
            self.promotion.discount(data.discount);
            self.promotion.extra(data.extra);
            self.promotion.storeId(data.storeId);
            self.promotion.graphUrl(data.graphUrl);
            self.promotion.amount.set(data.amount.gold,data.amount.rum,data.amount.key);
            self.promotion.price.set(data.price.gold,data.price.rum,data.price.key);
        }
        self.promotion.monsterId.subscribe(changeElement);
        self.promotion.levelMonster.subscribe(changeElement);
        self.promotion.duration.subscribe(changeElement);
        self.promotion.discount.subscribe(changeElement);
        self.promotion.extra.subscribe(changeElement);
        self.promotion.storeId.subscribe(changeElement);
        self.promotion.graphUrl.subscribe(changeElement);
        self.promotion.amount.subscribe(changeElement);
        self.promotion.price.subscribe(changeElement);
    }
    $.getJSON("../_ah/api/vacinaserver/v1/databalance/read?id="+id, function(data) {
        SetData(data);
    });
   
    
   
    self.deleteElement = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
    	$.ajax({
            type: "POST",
            data: 
            { 
                id: self.id()
            },
            url: "/_ah/api/vacinaserver/v1/databalance/delete",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
                window.open("meta_list.jsp?type="+type, "_self");
                
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    };
    
    self.submit = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
        self.promotion["class"] = "com.bighutgames.vacina.bo.game.meta.item.promotion.MetaPromotion";
    	$.ajax({
            type: "POST",
            data: 
            { 
                id: self.id(),
                metadata:ko.toJSON(self.promotion)
            },
            url: "/_ah/api/vacinaserver/v1/databalance/create",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    }
}

$(document).ready(
	function()
	{
		id = getURLParameter('id');
        
		$('#game-elements-side-link').addClass("active");
		$('#game-elements-promotion-list').parent().addClass("active");
		var temp = $('#game-elements-side-link').parent().find("ul");
		temp.show();

		//View Model
		ko.applyBindings(new promotionViewModel());
	}
);