function UserFindViewModel() 
{
	//Init
    var self = this;
    self.facebookId = ko.observable("");
    self.hasFacebook = ko.computed(function() {
		return self.facebookId().isEmpty();
    }, this);
    self.id = ko.observable("");
    self.hasId = ko.computed(function() {
		return self.id().isEmpty();
    }, this);
    
    self.openFacebookUser = function()
    {
        window.open('user.jsp?facebookId='+self.facebookId(),'_self');
    }
    
    self.openGeneralUser = function()
    {
        window.open('user.jsp?id='+self.id(),'_self');
    }
}

$(document).ready(
	function()
	{

		$('#developer-side-link').addClass("active");
		$('#developer-user-list').parent().addClass("active");
		var temp = $('#developer-side-link').parent().find("ul");
		temp.show();

		//View Model
		ko.applyBindings(new UserFindViewModel());
	}
);