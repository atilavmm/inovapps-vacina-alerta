var type = null;


function GetURL(section, id)
{
    return section+".jsp?id="+id;
}

function FormatZero(value)
{
    value = value.replace("level.", "");
    return parseInt(value);
}


function SetImage(element, id)
{
    $.ajax({
        type: "POST",
        data: 
        { 
            id: id
        },
        url: "/_ah/api/vacinaserver/v1/databalance/readlevel",
        success: function(result)
        {
            element(GenerateLevelImage(result.message, id));
        },
        error: function () 
        {
        }
    });
}

function MetaListViewModel() 
{
	//Init
    var self = this;
    self.list = ko.observableArray([]);
    self.section = ko.observable(type);
    self.sectionName = ko.computed(function() {
    	var name = self.section();
        return toTitleCase(name)+" List";
    }, this);
    self.rawElements = ko.observable({});
    self.elements = ko.computed(function() {
    	var data = self.rawElements();
    	var computed = [];
        var keys = Object.keys(data);
    	for(var i = 0; i < keys.length;i++)
    	{
    		var elementId = keys[i];
            var elementData = data[elementId];
    		var dotIndex = elementId.indexOf(".")+1;
    		var name = toTitleCase(elementId.substring(dotIndex, elementId.length));
    		var src = GetSrcItem(elementId);
    		var href = "";
    		var click = (function(id) {return function() { window.open(GetURL(self.section(), id),'_self'); }})(elementId);
            
    		if(elementId.indexOf("level") != -1)
            {
                src = ko.observable("");
                SetImage(src, elementId);
            }
    		computed.push
    		(
				{
					id:elementId,
					name:name,
					difficulty:elementData.difficulty,
					author: elementData.author,
					comment:elementData.comment,
					src:src,
					href:href,
					click:click
				}
			);
    	}
        computed.sort(function(a, b){ return (FormatZero(a.id) - FormatZero(b.id));});
    	return computed;
    }, this);
    self.hasCreate = ko.computed(function() {
    	var name = self.section();
        return name.indexOf("building") == -1;
    }, this);
    
    self.create = function()
    {
    	window.open(self.section()+".jsp?id=new","_self"); 
    };
    
    
    $.getJSON("../_ah/api/vacinaserver/v1/databalance/list?type="+self.section(), 
		function(metaLevels)
		{
    		delete metaLevels["etag"];
    		delete metaLevels["kind"];
    		self.rawElements(metaLevels);
		}
    );
   
}

$(document).ready(
	function()
	{
		type = getURLParameter('type');
		$('#game-elements-side-link').addClass("active");
		$('#game-elements-'+type+'-list').parent().addClass("active");
		var temp = $('#game-elements-side-link').parent().find("ul");
		temp.show();

		//View Model
		ko.applyBindings(new MetaListViewModel());
	}
);