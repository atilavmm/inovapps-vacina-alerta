function ConvertToListItem(list)
{
	var computed = [];
	for(var i = 0; i < list.length;i++)
	{
		var currentElement = list[i];
		
	}
	return computed;
}





function StoreListViewModel() 
{
	//Init
    var self = this;
	
	//Methods
	function MoveItemLeft(section,id)
	{
		self.currentSection(section);
		
		var rawData = self.data();
		var rawListItem = rawData.sections[section];
		var index = rawListItem.indexOf(id);
		if(index > 0)
		{
			var tmp = rawListItem[index];
			rawListItem[index] = rawListItem[index-1];
			rawListItem[index-1] = tmp;
		}
		rawData.sections[section] = rawListItem;
		self.data(rawData);
		self.changed(true);
	}
	function MoveItemRight(section,id)
	{
		self.currentSection(section);
		
		var rawData = self.data();
		var rawListItem = rawData.sections[section];
		var index = rawListItem.indexOf(id);
		if(index < rawListItem.length-1)
		{
			var tmp = rawListItem[index];
			rawListItem[index] = rawListItem[index+1];
			rawListItem[index+1] = tmp;
		}
		rawData.sections[section] = rawListItem;
		self.data(rawData);
		self.changed(true);
	}
	
	function RemoveItem(section,id)
	{
		self.currentSection(section);
		
		//Remove from Store
		var rawData = self.data();
		var rawListItem = rawData.sections[section];
		var index = rawListItem.indexOf(id);
		if(index >= 0)
		{
			rawListItem.splice(index, 1);
		}
		rawData.sections[section] = rawListItem;
		self.data(rawData);
		
		//Add to Trash
		var idleItems = self.rawOutOfStoreItems();
		idleItems.push(id);
		self.rawOutOfStoreItems(idleItems);
		self.changed(true);
	}
	
	function AddItem(section,id)
	{
		//Add to Section
		self.currentSection(section);
		var rawData = self.data();
		var rawListItem = rawData.sections[section];
		rawListItem.push(id);
		rawData.sections[section] = rawListItem;
		self.data(rawData);
		
		//Remove from Trash
		var idleItems = self.rawOutOfStoreItems();
		var index = idleItems.indexOf(id);
		if(index >= 0)
			idleItems.splice(index, 1);
		self.rawOutOfStoreItems(idleItems);
		self.changed(true);
	}
	
	//Changed
    self.data = ko.observable({sections:[]});
    self.currentSection = ko.observable("");
    self.sections = ko.computed(function() {
    	var keys = Object.keys(self.data().sections);
    	var computed = [];
        for(var i = 0; i < keys.length;i++)
        {
        	var name = keys[i];
    		var openSection = (function(m) {return function() { self.currentSection(m); }})(name);
        	computed.push({ name:name, ref:"#"+name, isActive:(name==self.currentSection()), openSection:openSection });
        }
        return computed;
    }, this);

    self.itemsBySection = ko.computed(function() {
    	var keys = Object.keys(self.data().sections);
    	var computed = [];
        for(var i = 0; i < keys.length;i++)
        {
        	var id = keys[i];
        	var elements = self.data().sections[id];
        	var computedElements = [];
        	for(var j = 0; j < elements.length;j++)
        	{
        		var elementId = elements[j];
        		var src = GetSrcItem(elementId);
        		var moveLeft = (function(s,m) {return function() { MoveItemLeft(s,m) }})(id,elementId);
        		var moveRight = (function(s,m) {return function() { MoveItemRight(s,m) }})(id,elementId);
        		var remove = (function(s,m) {return function() { RemoveItem(s,m) }})(id,elementId);
        		computedElements.push({id:elementId, src:src, moveLeft:moveLeft, moveRight:moveRight, remove:remove})
        	}
        	computed.push({name:id, elements:computedElements, isActive:(id==self.currentSection())});
        }
        return computed;
    }, this);
    
    
    
    self.rawOutOfStoreItems = ko.observableArray([]);
    self.outOfStoreItems = ko.computed(function() {
    	var elements = self.rawOutOfStoreItems();
    	var computedElements = [];
    	for(var j = 0; j < elements.length;j++)
    	{
    		var elementId = elements[j];
    		var src = GetSrcItem(elementId);
    		var addItem = (function(m) {return function() { AddItem(self.currentSection(),m) }})(elementId);
    		computedElements.push({id:elementId, src:src, addItem:addItem})
    	}
        return computedElements;
    }, this);
    self.changed = ko.observable(false);
    
    $.getJSON("../_ah/api/vacinaserver/v1/databalance/read?id=store", function(store) {
    	self.data(store);
    });
    
    $.getJSON("../_ah/api/vacinaserver/v1/databalance/outofstoreitems", function(trashItems) {
    	self.rawOutOfStoreItems(trashItems.items);
    });

    self.submit = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
        var data = self.data();
        data["class"] = "com.bighutgames.vacina.bo.game.meta.MetaStore";
    	$.ajax({
            
            type: "POST",
            data: 
            { 
            	id: "store",
            	metadata:ko.toJSON(data)
			},
            url: "/_ah/api/vacinaserver/v1/databalance/update",
            success: function(result)
            {
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
                self.changed(false);
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    }
}

$(document).ready(
	function()
	{

		$('#general-side-link').addClass("active");
		$('#general-store-link').parent().addClass("active");
		var temp = $('#general-side-link').parent().find("ul");
		temp.show();
		
		ko.applyBindings(new StoreListViewModel());
	}
);