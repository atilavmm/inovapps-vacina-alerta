var type = "vacine";
var id = "";

function VacineViewModel() 
{
	//Init
    var self = this;
    self.changed = ko.observable(false);
    var changeElement = function(){ self.changed(true); };
    self.id = ko.observable(id).extend({ prefixedString:"vacine." });
    

    self.vacine = {
		name:ko.observable(""),
		description:ko.observable(""),
		prevents:ko.observable(""),
		indication:ko.observable(""),
		counterIndication:ko.observable(""),
		adverseEffect:ko.observable(""),
        
		male:ko.observable(false),
		female:ko.observable(false),
        
		fixedDosage:ko.observable("[]"),
		periodicDosage:ko.observable(-1),
        
		start:ko.observable(0),
		end:ko.observable(0),
        
		pregnantAvailable:ko.observable(true)
	};
    
    if(id == "new")
    {
        self.changed(true);
        SetData();
    }
    
    function SetData(data)
    {
        if(data != undefined)
        { 
            self.vacine.name(data.name); 
            self.vacine.description(data.description);
            self.vacine.prevents(data.prevents);
            self.vacine.indication(data.indication);
            self.vacine.counterIndication(data.counterIndication);
            self.vacine.adverseEffect(data.adverseEffect);
            
            self.vacine.male(data.male);
            self.vacine.female(data.female);
            
            self.vacine.fixedDosage(data.fixedDosage);
            self.vacine.periodicDosage(data.periodicDosage);
            
            self.vacine.start(data.start);
            self.vacine.end(data.end);
            
            self.vacine.pregnantAvailable(data.pregnantAvailable);
        }
        self.vacine.name.subscribe(changeElement);
        self.vacine.description.subscribe(changeElement);
        self.vacine.prevents.subscribe(changeElement);
        self.vacine.indication.subscribe(changeElement);
        self.vacine.counterIndication.subscribe(changeElement);
        self.vacine.adverseEffect.subscribe(changeElement);
        
        self.vacine.male.subscribe(changeElement);
        self.vacine.female.subscribe(changeElement);
        
        self.vacine.fixedDosage.subscribe(changeElement);
        self.vacine.periodicDosage.subscribe(changeElement);
        
        self.vacine.start.subscribe(changeElement);
        self.vacine.end.subscribe(changeElement);
            
        self.vacine.pregnantAvailable.subscribe(changeElement);
    }
    $.getJSON("../_ah/api/vacinaserver/v1/databalance/read?id="+id, function(data) {
        SetData(data);
    });
   
    self.deleteElement = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
    	$.ajax({
            type: "POST",
            data: 
            { 
                id: self.id()
            },
            url: "/_ah/api/vacinaserver/v1/databalance/delete",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
                window.open("meta_list.jsp?type="+type, "_self");
                
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    };
    
    self.submit = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
        self.vacine["class"] = "com.bighutgames.vacina.bo.game.Vacine";
    	$.ajax({
            type: "POST",
            data: 
            { 
                id: self.id(),
                metadata:ko.toJSON(self.vacine)
            },
            url: "/_ah/api/vacinaserver/v1/databalance/create",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    }
}

$(document).ready(
	function()
	{
		id = getURLParameter('id');
		$('#game-elements-side-link').addClass("active");
		$('#game-elements-vacine-list').parent().addClass("active");
		var temp = $('#game-elements-side-link').parent().find("ul");
		temp.show();

		//View Model
		ko.applyBindings(new VacineViewModel());
	}
);