

function GameDataViewModel() 
{
	//Init
    var self = this;
    self.data = ko.observable("");
    self.changed = ko.observable(false);
    
    self.isLive = ko.observable(isLiveServer());
    
    $.getJSON("../_ah/api/vacinaserver/v1/databalance/export", function(data) {
        var jsonData = jQuery.parseJSON( data.message );
    	self.data(JSON.stringify(jsonData, null, "\t"));
        self.data.subscribe(function(){self.changed(true);});
    });
   
    self.submit = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
    	$.ajax({
            
            type: "POST",
            data: 
            { 
            	gameData: self.data()
			},
            url: "/_ah/api/vacinaserver/v1/databalance/import",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    }
}

$(document).ready(
	function()
	{

		$('#developer-side-link').addClass("active");
		$('#developer-game-data-list').parent().addClass("active");
		var temp = $('#developer-side-link').parent().find("ul");
		temp.show();

		//View Model
		ko.applyBindings(new GameDataViewModel());
	}
);