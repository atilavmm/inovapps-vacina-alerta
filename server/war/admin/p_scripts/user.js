var id = "";
var facebookId = "";
var serverUser;

function UserViewModel() 
{
	//Init
    var self = this;
    self.changed = ko.observable(false);
    var changeElement = function(){ self.changed(true); };
   
    self.user = 
    {
        Id: ko.observable(id),
        facebookId: ko.observable(facebookId),
        bighutId :ko.observable(""),
        data: 
        {
            bank: new KoBank(0,0)
            ,
            saga: 
            {
                levels: ko.observableArray()
            }
        }
    };
    
    
    function getUser()
    {
        var baseURL = "../_ah/api/vacinaserver/v1/user/get?";
        if(!self.user.Id().isEmpty())
            baseURL += "id="+self.user.Id();
        if(!self.user.facebookId().isEmpty())
        {
            if(!self.user.Id().isEmpty())
                baseURL += "&";
            baseURL += "facebookId="+self.user.facebookId();
        }
            
        $.getJSON(baseURL, function(data) {
            var raw = data.message;
            data = jQuery.parseJSON( raw );
            serverUser = data;
            self.user.Id(data.Id);
            self.user.facebookId(data.facebookId);
            self.user.bighutId(data.bighutId);

            self.user.data.bank.set(data.data.bank.gold, data.data.bank.rum, changeElement);
            
            self.user.data.saga.levels(data.data.saga.levels);
            
            
            self.user.Id.subscribe(changeElement);
            self.user.data.bank.subscribe(changeElement);
        });
    }
    self.submit = function()
    {

        var changedUser = jQuery.parseJSON(ko.toJSON(self.user));
        
        serverUser.Id = changedUser.Id;
        serverUser.facebookId = changedUser.facebookId;
        serverUser.bighutId = changedUser.bighutId;
        serverUser.data.bank = changedUser.data.bank;
        serverUser.data.saga.levels = changedUser.data.saga.levels;
    	
        var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
    	$.ajax({
            
            type: "POST",
            data: 
            { 
            	user: ko.toJSON(self.user)
			},
            url: "/_ah/api/vacinaserver/v1/user/set",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    };
    
    self.rawEditor = function()
    {
        if(!id.isEmpty())
    	   window.open("raw_user.jsp?id="+id,"_self"); 
        if(!facebookId.isEmpty())
    	   window.open("raw_user.jsp?facebookId="+facebookId,"_self"); 
    };
    
    getUser();
}

$(document).ready(
	function()
	{
		id = getURLParameter('id');
        if(id == null)
            id = "";
        facebookId = getURLParameter('facebookId');
        if(facebookId == null)
            facebookId = "";
        
		$('#developer-side-link').addClass("active");
		$('#developer-user-list').parent().addClass("active");
		var temp = $('#developer-side-link').parent().find("ul");
		temp.show();

		//View Model
		ko.applyBindings(new UserViewModel());
	}
);