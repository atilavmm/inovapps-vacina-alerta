var type = "item";
var id = "";



function ItemViewModel() 
{
	//Init
    var self = this;
    self.changed = ko.observable(false);
    var changeElement = function(){ self.changed(true); };
    self.id = ko.observable(id).extend({ prefixedString:"item." });
    self.image =  ko.computed(function() {
		return GetSrcItem(self.id());
    }, this);
    self.item = {
		discount: ko.observable(0).extend({positiveFloat:{}}),
		storeId: ko.observable(),
		graphUrl: ko.observable(),
        price: new KoBank(0)
	};
    self.isIAP = ko.computed(function() {
        var identifier = self.id();
		return identifier.indexOf("gold") != -1;
    }, this);
    
    function SetData(data)
    {
        if(data != undefined)
        {
            self.item.discount(data.discount);
            self.item.storeId(data.storeId);
            self.item.graphUrl(data.graphUrl);
            self.item.price.set(data.price.gold);
        }
        self.item.discount.subscribe(changeElement);
        self.item.storeId.subscribe(changeElement);
        self.item.graphUrl.subscribe(changeElement);
        self.item.price.subscribe(changeElement);
    }
    $.getJSON("../_ah/api/vacinaserver/v1/databalance/read?id="+id, function(data) {
        SetData(data);
    });
    
    self.deleteElement = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
    	$.ajax({
            type: "POST",
            data: 
            { 
                id: self.id()
            },
            url: "/_ah/api/vacinaserver/v1/databalance/delete",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
                window.open("meta_list.jsp?type="+type, "_self");
                
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    };
    
    self.submit = function()
    {
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
        self.item["class"] = "com.bighutgames.vacina.bo.game.meta.item.MetaItem";
    	$.ajax({
            type: "POST",
            data: 
            { 
                id: self.id(),
                metadata:ko.toJSON(self.item)
            },
            url: "/_ah/api/vacinaserver/v1/databalance/create",
            success: function(result)
            {
            	self.changed(false);
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    }
}

$(document).ready(
	function()
	{
		id = getURLParameter('id');
        
		$('#game-elements-side-link').addClass("active");
		$('#game-elements-item-list').parent().addClass("active");
		var temp = $('#game-elements-side-link').parent().find("ul");
		temp.show();

		//View Model
		ko.applyBindings(new ItemViewModel());
	}
);