var rawMetaConstants;

function MetaConstantsViewModel() 
{
    var self = this;
	//Changed
	self.powerupCannonShot = { 
		level1: ko.observable(0).extend({ int:{} }),
		level2: ko.observable(0).extend({ int:{} }),
		level3: ko.observable(0).extend({ int:{} }),
		level4: ko.observable(0).extend({ int:{} }),
		level5: ko.observable(0).extend({ int:{} }),
		changed: ko.observable(false)
	};
	self.powerupFoeHunt = { 
		chooseSandTileChance: ko.observable(0).extend({ positiveFloat:{} }),
		level1: ko.observable(0).extend({ positiveFloat:{} }),
		level2: ko.observable(0).extend({ positiveFloat:{} }),
		level3: ko.observable(0).extend({ positiveFloat:{} }),
		level4: ko.observable(0).extend({ positiveFloat:{} }),
		level5: ko.observable(0).extend({ positiveFloat:{} }),
		changed: ko.observable(false)
	};
	self.powerupPirateRum = { 
		level1: ko.observable(0).extend({ positiveFloat:{} }),
		level2: ko.observable(0).extend({ positiveFloat:{} }),
		level3: ko.observable(0).extend({ positiveFloat:{} }),
		level4: ko.observable(0).extend({ positiveFloat:{} }),
		level5: ko.observable(0).extend({ positiveFloat:{} }),
		changed: ko.observable(false)
	};

	self.powerupPointBlankShot = { 
		level1: ko.observable(0).extend({ int:{} }),
		level2: ko.observable(0).extend({ int:{} }),
		level3: ko.observable(0).extend({ int:{} }),
		level4: ko.observable(0).extend({ int:{} }),
		level5: ko.observable(0).extend({ int:{} }),
		changed: ko.observable(false)
	};

	self.powerupThePlank = { 
		level1: ko.observable(0).extend({ int:{} }),
		level2: ko.observable(0).extend({ int:{} }),
		level3: ko.observable(0).extend({ int:{} }),
		level4: ko.observable(0).extend({ int:{} }),
		level5: ko.observable(0).extend({ int:{} }),
		changed: ko.observable(false)
	};
	
	self.powerupTreasureX = { 
		level1: ko.observable(0).extend({ int:{} }),
		level2: ko.observable(0).extend({ int:{} }),
		level3: ko.observable(0).extend({ int:{} }),
		level4: ko.observable(0).extend({ int:{} }),
		level5: ko.observable(0).extend({ int:{} }),
		changed: ko.observable(false)
	};
	
    
    //Load Initial State
    $.getJSON("../_ah/api/vacinaserver/v1/dataconstants/get", function(constants) {
    	rawMetaConstants = constants;
    	
    	var mapping =
    	[
    		{ko:self.powerupCannonShot,raw:constants.powerupCannonShot},
    		{ko:self.powerupFoeHunt,raw:constants.powerupFoeHunt},
    		{ko:self.powerupPirateRum,raw:constants.powerupPirateRum},
    		{ko:self.powerupPointBlankShot,raw:constants.powerupPointBlankShot},
    		{ko:self.powerupThePlank,raw:constants.powerupThePlank},
    		{ko:self.powerupTreasureX,raw:constants.powerupTreasureX}
    	];
    	
    	for(var i = 0; i < mapping.length;i++)
    	{
    		var element = mapping[i];
    		element.ko.level1(element.raw.level1);
    		element.ko.level2(element.raw.level2);
    		element.ko.level3(element.raw.level3);
    		element.ko.level4(element.raw.level4);
    		element.ko.level5(element.raw.level5);
    		
    		element.ko.level1.subscribe((function(m) {return function() { m.ko.changed(true);}})(element));
    		element.ko.level2.subscribe((function(m) {return function() { m.ko.changed(true);}})(element));
    		element.ko.level3.subscribe((function(m) {return function() { m.ko.changed(true);}})(element));
    		element.ko.level4.subscribe((function(m) {return function() { m.ko.changed(true);}})(element));
    		element.ko.level5.subscribe((function(m) {return function() { m.ko.changed(true);}})(element));
    	}
    	
    	self.powerupFoeHunt.chooseSandTileChance(constants.powerupFoeHunt.chooseSandTileChance);
    	self.powerupFoeHunt.chooseSandTileChance.subscribe((function(m) {return function() { m.changed(true);}})(self.powerupFoeHunt));
    });
    
    //Submit
    self.submit = function()
    {
    	rawMetaConstants.powerupCannonShot = self.powerupCannonShot;
    	rawMetaConstants.powerupFoeHunt = self.powerupFoeHunt;
    	rawMetaConstants.powerupPirateRum = self.powerupPirateRum;
    	rawMetaConstants.powerupPointBlankShot = self.powerupPointBlankShot;
    	rawMetaConstants.powerupThePlank = self.powerupThePlank;
    	rawMetaConstants.powerupTreasureX = self.powerupTreasureX;
    	
    	var unique_id = $.gritter.add({title: 'Submitting Data',text: 'Syncing data with server',class_name: 'my-sticky-class'});
        
    	$.ajax({
            
            type: "POST",
            data: 
            { 
            	metaConstants:ko.toJSON(rawMetaConstants) 
			},
             
            url: "/_ah/api/vacinaserver/v1/dataconstants/update",
            success: function(result)
            {
            	self.powerupCannonShot.changed(false);
            	self.powerupFoeHunt.changed(false);
            	self.powerupPirateRum.changed(false);
            	self.powerupPointBlankShot.changed(false);
            	self.powerupThePlank.changed(false);
            	self.powerupTreasureX.changed(false);
            	
                var unique_id = $.gritter.add({title: 'Data Synced',text: 'Data was successful synced', image: 'p_images/yes.png', sticky: false, class_name: 'my-sticky-class'});
            },
            error: function () {
                var unique_id = $.gritter.add({title: 'ERROR',text: 'Data was successful synced', image: 'p_images/no.png', sticky: false, class_name: 'my-sticky-class'});
	        }
        });
    }
    
}

$(document).ready(
	function()
	{

		$('#general-side-link').addClass("active");
		$('#general-powerups-link').parent().addClass("active");
		var temp = $('#general-side-link').parent().find("ul");
		temp.show();
		
		//View Model
		ko.applyBindings(new MetaConstantsViewModel());
	}
);