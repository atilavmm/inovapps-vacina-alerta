<jsp:include page="template_before.jsp" />
<!--main content start-->
<script src="p_scripts/metalevel_list.js"></script>
<link rel="stylesheet" type="text/css" href="p_css/meta_list.css" />
<link rel="stylesheet" type="text/css" href="p_css/level.css" />
<section id="main-content">
    <section class="wrapper">
    	<div class="row">
		    <div class="col-lg-12" style="float: none;">
		    <section class="panel">
		            <header class="panel-heading" data-bind="text: sectionName">
		            </header>
		            <div class="panel-body">
							<table class="table table-bordered table-condensed">
	                            <thead>
	                             <tr>
	                                 <th>Icon</th>
	                                 <th>Name</th>
	                                 <th>Difficulty</th>
	                                 <th>Author</th>
	                                 <th>Comment</th>
	                             </tr>
	                            </thead>
	                            <tbody data-bind="foreach: elements">
	                             <tr class="clickable-row" data-bind="click: click">
	                                 <td class="clickable-column"><img style="width:50px; height:50px;" data-bind="attr:{src:src, id: id}"></td>
	                                 <td class="clickable-column" data-bind="text: name"></td>
	                                 <td class="clickable-column" data-bind="style:{'background-color':difficulty}"></td>
	                                 <td class="clickable-column" data-bind="text: author"></td>
	                                 <td class="clickable-column" data-bind="text: comment"></td>
	                             </tr>
	                            </tbody>
	                        </table>
                        
                        <!-- ko if: hasCreate -->
               	  		<button type="button" class="btn btn-success btn-lg btn-block" data-bind="click: create" style="margin-top:10px;">Create New</button>
                        <!-- /ko -->
		            </div>
		        </section>		
		    </div>
		</div>
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />