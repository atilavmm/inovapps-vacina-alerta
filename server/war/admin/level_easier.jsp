<jsp:include page="template_before.jsp" />
<!--main content start-->
<script type="text/javascript" src="p_scripts/level_easier.js"></script>

<section id="main-content">
    <section class="wrapper">
        

       <!-- Starting Gem Distribution by Tries -->
        <div class="col-lg-6">
            <section class="panel">
                <header class="panel-heading">
                    Gem Distribution by Tries
                </header>
                <div class="panel-body">
                    <div class="form-horizontal tasi-form">
                        <div class="row">
                            <table class="table" style="width:100%; padding:20px 20px 20px 20px;">
                              <thead>
                                  <tr>
                                      <th>Parameter</th>
                                      <th>Min</th>
                                      <th>Max</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td>
                                          Tries to Start Easier(Hours):
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.loseValueInit.min" type="text" class="form-control">
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.loseValueInit.max" type="text" class="form-control">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          Gap of Tries to Increase the Easier(Hours):
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.loseValueInterval.min" type="text" class="form-control">
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.loseValueInterval.max" type="text" class="form-control">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          Increase Factor:
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.loseValueFactor.min" type="text" class="form-control">
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.loseValueFactor.max" type="text" class="form-control">
                                      </td>
                                  </tr>
                              </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 col-lg-4 control-label">Max Try Factor:</label>
                            <div class="col-lg-8">
                                <input data-bind="value: constants.easier.maxTryFactor" type="text" class="form-control">
                            </div>
                        </div>
                        <!-- ko if: changedTries -->
                            <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                        <!-- /ko -->
                    </div>
                </div>
            </section>	
        </div>
       <!-- Ending Gem Distribution by Tries  -->


       <!-- Starting Gem Distribution by Time -->
        <div class="col-lg-6">
            <section class="panel">
                <header class="panel-heading">
                    Gem Distribution by Time
                </header>
                <div class="panel-body">
                    <div class="form-horizontal tasi-form">
                        <div class="row">
                            <table class="table" style="width:100%; padding:20px 20px 20px 20px;">
                              <thead>
                                  <tr>
                                      <th>Parameter</th>
                                      <th>Min</th>
                                      <th>Max</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td>
                                          Time to Start Easier(Hours):
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.timeValueInit.min" type="text" class="form-control">
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.timeValueInit.max" type="text" class="form-control">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          Gap of Time to Increase the Easier(Hours):
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.timeValueInterval.min" type="text" class="form-control">
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.timeValueInterval.max" type="text" class="form-control">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          Increase Factor:
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.timeValueFactor.min" type="text" class="form-control">
                                      </td>
                                      <td>
                                          <input data-bind="value: constants.easier.gems.timeValueFactor.max" type="text" class="form-control">
                                      </td>
                                  </tr>
                              </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 col-lg-4 control-label">Max Time Factor:</label>
                            <div class="col-lg-8">
                                <input data-bind="value: constants.easier.maxTimeFactor" type="text" class="form-control">
                            </div>
                        </div>
                        <!-- ko if: changedTime -->
                            <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                        <!-- /ko -->
                    </div>
                </div>
            </section>	
        </div>
       <!-- Ending Gem Distribution by Time  -->
        
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />