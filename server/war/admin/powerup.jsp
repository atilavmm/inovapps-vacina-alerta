<jsp:include page="template_before.jsp" />
<!--main content start-->
<script type="text/javascript" src="p_scripts/powerup.js"></script>

<section id="main-content">
    <section class="wrapper">
	    <div class="row">
    	    <!-- Starting Cannon Shot -->
		    <div class="col-lg-6">
		        <section class="panel">
		            <header class="panel-heading">
		                <img class="field_element_icon" src="p_icons/powerups/cannonshot.png">Cannon Shot
		            </header>
		            <div class="panel-body">
			            <div class="form-horizontal tasi-form">
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 1:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupCannonShot.level1" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 2:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupCannonShot.level2" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 3:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupCannonShot.level3" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 4:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupCannonShot.level4" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 5:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupCannonShot.level5" type="text" class="form-control"></div>
						    </div>
    					  <!-- ko if: powerupCannonShot.changed -->
                          <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                          <!-- /ko -->
						</div>
		            </div>
		        </section>		
		    </div>
	    	<!-- Ending Cannon Shot -->
	    	
	    	<!-- Starting Foe Hunt -->
		    <div class="col-lg-6">
		        <section class="panel">
		            <header class="panel-heading">
		                <img class="field_element_icon" src="p_icons/powerups/foehunt.png">Foe Hunt
		            </header>
		            <div class="panel-body">
			            <div class="form-horizontal tasi-form">
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Chance Choose Sand Tile:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupFoeHunt.chooseSandTileChance" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 1:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupFoeHunt.level1" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 2:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupFoeHunt.level2" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 3:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupFoeHunt.level3" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 4:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupFoeHunt.level4" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 5:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupFoeHunt.level5" type="text" class="form-control"></div>
						    </div>
    					  <!-- ko if: powerupFoeHunt.changed -->
                          <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                          <!-- /ko -->
						</div>
		            </div>
		        </section>		
		    </div>
	    	<!-- Ending Foe Hunt -->
	    	
	    	<!-- Starting Pirate Rum -->
		    <div class="col-lg-6">
		        <section class="panel">
		            <header class="panel-heading">
		                <img class="field_element_icon" src="p_icons/powerups/piraterum.png">Pirate Rum
		            </header>
		            <div class="panel-body">
			            <div class="form-horizontal tasi-form">
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 1:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupPirateRum.level1" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 2:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupPirateRum.level2" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 3:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupPirateRum.level3" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 4:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupPirateRum.level4" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 5:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupPirateRum.level5" type="text" class="form-control"></div>
						    </div>
    					  <!-- ko if: powerupPirateRum.changed -->
                          <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                          <!-- /ko -->
						</div>
		            </div>
		        </section>		
		    </div>
	    	<!-- Ending Pirate Rum -->
	    	
	    	<!-- Starting Point Black Shot -->
		    <div class="col-lg-6">
		        <section class="panel">
		            <header class="panel-heading">
		                <img class="field_element_icon" src="p_icons/powerups/pointblankshot.png">Point Blank Shot
		            </header>
		            <div class="panel-body">
			            <div class="form-horizontal tasi-form">
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 1:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupPointBlankShot.level1" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 2:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupPointBlankShot.level2" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 3:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupPointBlankShot.level3" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 4:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupPointBlankShot.level4" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 5:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupPointBlankShot.level5" type="text" class="form-control"></div>
						    </div>
    					  <!-- ko if: powerupPointBlankShot.changed -->
                          <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                          <!-- /ko -->
						</div>
		            </div>
		        </section>		
		    </div>
	    	<!-- Ending Point Black Shot -->
	    	
	    	<!-- Starting The Plank -->
		    <div class="col-lg-6">
		        <section class="panel">
		            <header class="panel-heading">
		                <img class="field_element_icon" src="p_icons/powerups/theplank.png">The Plank
		            </header>
		            <div class="panel-body">
			            <div class="form-horizontal tasi-form">
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 1:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupThePlank.level1" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 2:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupThePlank.level2" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 3:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupThePlank.level3" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 4:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupThePlank.level4" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 5:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupThePlank.level5" type="text" class="form-control"></div>
						    </div>
    					  <!-- ko if: powerupThePlank.changed -->
                          <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                          <!-- /ko -->
						</div>
		            </div>
		        </section>		
		    </div>
	    	<!-- Ending The Plank -->
	    	
	    	<!-- Starting Treasure X -->
		    <div class="col-lg-6">
		        <section class="panel">
		            <header class="panel-heading">
		               <img class="field_element_icon" src="p_icons/powerups/treasurex.png"> Treasure X
		            </header>
		            <div class="panel-body">
			            <div class="form-horizontal tasi-form">
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 1:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupTreasureX.level1" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 2:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupTreasureX.level2" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 3:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupTreasureX.level3" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 4:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupTreasureX.level4" type="text" class="form-control"></div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label" >Level 5:</label>
						        <div class="col-lg-8"><input data-bind="value: powerupTreasureX.level5" type="text" class="form-control"></div>
						    </div>
    					  <!-- ko if: powerupTreasureX.changed -->
                          <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                          <!-- /ko -->
						</div>
		            </div>
		        </section>		
		    </div>
	    	<!-- Ending Treasure X -->
	    </div>
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />