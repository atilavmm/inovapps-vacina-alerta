<jsp:include page="template_before.jsp" />
<!--main content start-->

<script type="text/javascript" src="p_scripts/store.js"></script>

<section id="main-content">
    <section class="wrapper">
	    <div class="row">
		    <div class="col-lg-12">
		    <section class="panel">
		            <header class="panel-heading">
		                Store
		            </header>
		            <div class="panel-body">
						<section class="panel">
                          <header class="panel-heading tab-bg-dark-navy-blue ">
                              <ul class="nav nav-tabs" data-bind="foreach: sections">
                                  <li data-bind="css:{active:isActive}">
                                      <a data-toggle="tab" href="#home" data-bind="text: name, attr:{href: ref}, click: openSection"></a>
                                  </li>
                              </ul>
                          </header>
                          <div class="panel-body">
                              <div class="tab-content" data-bind="foreach: itemsBySection">
                                  <div id="home" data-bind="attr:{id: name}, css:{active:isActive}"  class="tab-pane">
    					  			<!-- ko foreach: elements-->
    					  			<div data-bind="attr:{id: id}" style="float:left; width:190px; height:145px; background-color:#E4EEFA; position:relative; margin:5px 5px 5px 5px;" >
                                  		<img style="width:135px; height:135px;" data-bind="attr:{src:src}">
                                  		<button class="btn btn-round btn-primary" data-bind="click: moveLeft" style="position:absolute; margin:20px 5px;">&Lt;</button>
                                  		<button class="btn btn-round btn-success" data-bind="click: moveRight" style="position:absolute; margin:55px 5px;">&Gt;</button>
                                  		<button class="btn btn-round btn-danger" data-bind="click: remove" style="position:absolute; margin:90px 5px;">x</button>
                                  	</div>
                                  	<!-- /ko -->
	                                <div style="clear:both;"></div>
	                                <br>
	                                <br>
	                                <p class="text-muted" style="width:100%;">Idle Items</p>
    					  			<!-- ko foreach: $parent.outOfStoreItems-->
	    					  			<div data-bind="attr:{id: id}" style="float:left; width:190px; height:145px; background-color:#E4EEFA; position:relative; margin:5px 5px 5px 5px;" >
	                                  		<img style="width:135px; height:135px;" data-bind="attr:{src:src}">
	                                  		<button class="btn btn-round btn-primary" data-bind="click: addItem" style="position:absolute; margin:20px 5px;">+</button>
	                                  	</div>
                                  	<!-- /ko -->
                                  </div>
                              </div>
                              <!-- ko if: changed-->
                         	  <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                              <!-- /ko -->
                          </div>
                      </section>
		            </div>
		        </section>		
		    </div>
		</div>
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />