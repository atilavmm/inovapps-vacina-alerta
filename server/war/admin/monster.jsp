<jsp:include page="template_before.jsp" />
<!--main content start-->
<script src="p_scripts/monster.js"></script>
<link rel="stylesheet" type="text/css" href="p_css/meta_list.css" />
<section id="main-content">
<section class="wrapper">
<div class="row">
	<div class="col-lg-12" style="float: none;">
		<section class="panel">
			<header class="panel-heading">
				Monster
			</header>
			<div class="panel-body">
				<div class="row">
					<div class="form-horizontal tasi-form">
						<div class="col-lg-2">
							<img data-bind="attr:{src: image}"/>
							<a class="btn btn-danger btn-block" data-toggle="modal" href="#deleteModel">Delete Monster</a>
							<div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title">Delete Element Confirmation</h4>
										</div>
										<div class="modal-body">
											Are you sure to delete this entity?
										</div>
										<div class="modal-footer">
											<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
											<button class="btn btn-warning" type="button" data-bind="click: deleteElement"> Confirm</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-10">
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Id:</label>
								<div class="col-sm-10">
									<input type="text" data-bind="value: id" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Type:</label>
								<div class="col-sm-10">
                                    <div class="col-sm-6 col-sm-6">
                                        <div class="col-sm-2 col-sm-2">
                                            <img class="big_field_element_icon" data-bind="attr:{src: typeIcon}"/>
                                        </div>
                                        <div class="col-sm-10">
                                           <select data-bind="options: elements, optionsText: function(item) {return ELEMENTS_NAME[elements().indexOf(item)];}, value: monster.type" class="form-control m-bot15"></select>
                                        </div>
                                    </div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">PowerUp Type:</label>
								<div class="col-sm-10">
									<div class="col-sm-1 col-sm-1">
										<img class="big_field_element_icon" data-bind="attr:{src: powerUpTypeIcon}"/>
									</div>
									<div class="col-sm-11">
									   <select data-bind="options: powerups, optionsText: function(item) {return POWERUPS_NAME[powerups().indexOf(item)];}, value: monster.powerUpType" class="form-control m-bot15"></select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Rarity Type:</label>
								<div class="col-sm-10">
									<div class="col-sm-1 col-sm-1">
										<img class="big_field_element_icon" data-bind="attr:{src: rarityIcon}"/>
									</div>
									<div class="col-sm-11">
										<select data-bind="options: rarities, optionsText: function(item) {return RARITY_NAME[rarities().indexOf(item)];}, value: monster.rarityType" class="form-control m-bot15"></select>
									</div>
								</div>
							</div>
							<!--
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Price:</label>
								<div class="col-sm-5">
									<div class="form-group">
										<div class="col-sm-2 col-sm-2">
											<img class="field_element_icon" src="p_icons/gold.png"/>
										</div>
										<div class="col-sm-10">
											<input type="text" data-bind="value: monster.price.gold" class="form-control">
										</div>
									</div>
								</div>
								<div class="col-sm-5">
									<div class="form-group">
										<div class="col-sm-2 col-sm-2">
											<img class="field_element_icon" src="p_icons/rum.png"/>
										</div>
										<div class="col-sm-10">
											<input type="text" data-bind="value: monster.price.rum" class="form-control">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Sell Value:</label>
								<div class="col-sm-5">
									<div class="form-group">
										<div class="col-sm-2 col-sm-2">
											<img class="field_element_icon" src="p_icons/gold.png"/>
										</div>
										<div class="col-sm-10">
											<input type="text" data-bind="value: monster.sellValue.gold" class="form-control">
										</div>
									</div>
								</div>
								<div class="col-sm-5">
									<div class="form-group">
										<div class="col-sm-2 col-sm-2">
											<img class="field_element_icon" src="p_icons/rum.png"/>
										</div>
										<div class="col-sm-10">
											<input type="text" data-bind="value: monster.sellValue.rum" class="form-control">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Discount:</label>
								<div class="col-sm-10">
									<input type="text" data-bind="value: monster.discount" class="form-control">
								</div>
							</div>
							-->
						</div>
					</div>
				</div>
				<br>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-horizontal tasi-form">
							<!-- ko if: changed -->
							<button type="button" class="btn btn-success btn-warning btn-block"  data-bind="click: submit" style="margin-top:10px;">Submit</button>
							<!-- /ko -->
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
</section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />