<jsp:include page="template_before.jsp" />
<!--main content start-->
<script src="p_scripts/game_data.js"></script>

<section id="main-content">
    <section class="wrapper">
        <!-- ko if: isLive-->
        <div class="row">
		    <div class="col-lg-12">
		      <section class="panel">
		            <header class="panel-heading">
		            	Game Data not available
		            </header>
		            <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                It's not available in the Live Server
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /ko -->
        <!-- ko ifnot: isLive-->
    	<div class="row">
		    <div class="col-lg-12">
		    <section class="panel">
		            <header class="panel-heading">
		                GameData
		            </header>
		            <div class="panel-body">
		            <textarea data-bind="value:data" id="gameDataJson" style="width:100%;" rows="30" cols="1000"></textarea>
                    <!-- ko if: changed-->
               	  	<button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                    <!-- /ko -->
		            </div>
		        </section>		
		    </div>
		</div>
        <!-- /ko -->
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />