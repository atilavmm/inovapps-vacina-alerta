<jsp:include page="template_before.jsp" />
<!--main content start-->
<script type="text/javascript" src="p_scripts/constants.js"></script>

<section id="main-content">
    <section class="wrapper">
	    <div class="row">
	    	<!-- Starting General Panel -->
		    <div class="col-lg-6">
		        <section class="panel">
		            <header class="panel-heading">
		                General
		            </header>
		            <div class="panel-body">
			            <div class="form-horizontal tasi-form">
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">
						        	<i class="cicon-gem"></i>
						        	Max Lives
						        </label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.maxLives" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Recharge Life Seconds:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.rechargeLifeSeconds" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Initial Gold:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.goldStart" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Initial rum:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.rumStart" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">More Moves Amount:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.moreMoves" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Gem Score:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.gemScore" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Evolve Gap:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.evolveGap" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Facebook Canvas Active:</label>
						        <div class="col-lg-8">
                                    <input type="checkbox" value="value: constants.isFacebookCanvasActive" data-bind="checked: constants.isFacebookCanvasActive" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Incentivized Video:</label>
						        <div class="col-lg-8">
                                     <label class="col-lg-3">Base:</label>
						            <input class="col-lg-3" data-bind="value: constants.intervalVideoRewardBase" type="text" class="form-control">
                                     <label class="col-lg-3">Factor:</label>
						            <input class="col-lg-3" data-bind="value: constants.intervalVideoRewardFactor" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Show Promotion Offer:</label>
						        <div class="col-lg-8">
                                     <label class="col-lg-3">Base:</label>
						            <input class="col-lg-3" data-bind="value: constants.intervalPromotionOfferBase" type="text" class="form-control">
                                     <label class="col-lg-3">Factor:</label>
						            <input class="col-lg-3" data-bind="value: constants.intervalPromotionOfferFactor" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Interstitial Ads:</label>
						        <div class="col-lg-8">
                                     <label class="col-lg-3">Start Level:</label>
						            <input class="col-lg-3" data-bind="value: constants.interstitialStartLevel" type="text" class="form-control">
                                     <label class="col-lg-3">Level Gap:</label>
						            <input class="col-lg-3" data-bind="value: constants.interstitialLevelGap" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Gems To Power Up Base:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.gemToPowerUpBase" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Gems To Cannon Shot:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.gemToCannonShot" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Cannon Shot Size:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.cannonShotSize" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">End Game Score Mutiplier Factor:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.endGameMultiplier" type="text" class="form-control">
						        </div>
						    </div>
                            <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Rum Bounty Parameter:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.rumBountyParameter" type="text" class="form-control">
						        </div>
						    </div>
                            <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Rum In Bounty:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.rumInBounty" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Kraken Attack Interval:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.raiseTentacleInterval" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Time to get timed Reward in minutes:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.timedRewardInMinutes" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Levels To Like and Rate(Comma Separated):</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.levelsToLikeRate" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">
						        	Sync - Buffer Size
						        </label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.syncBufferSize" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">
                                    Sync - Interval
                                </label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.syncInterval" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">IOS Version:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.currentIOSVersion" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Android Version:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.currentAndroidVersion" type="text" class="form-control">
						        </div>
						    </div>
                            
    					  <!-- ko if: changedGeneral -->
                          <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                          <!-- /ko -->
						</div>
		            </div>
		        </section>	
		    </div>
	    	<!-- Ending General Panel -->
            
	    	<!-- Starting Unlocks Panel -->
		    <div class="col-lg-6">
		        <section class="panel">
		            <header class="panel-heading">
		                Combos
		            </header>
		            <div class="panel-body">
			            <div class="form-horizontal tasi-form">
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Score - Uh hah haa:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.combos.score1" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Score - Ship Ahoy:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.combos.score2" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Score - Jolly Roger!:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.combos.score3" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Score - Combo Powerup!:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.combos.comboPowerup" type="text" class="form-control">
						        </div>
						    </div>
    					  <!-- ko if: changedCombos -->
                          <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                          <!-- /ko -->
						</div>
		            </div>
		        </section>	
		    </div>
	    	<!-- Ending Unlocks Panel -->
        </div>

	    	<!-- Starting Monsters Feed Panel -->
		    <div class="col-lg-6">
		        <section class="panel">
		            <header class="panel-heading">
		                Monsters Feed
		            </header>
		            <div class="panel-body">
			            <div class="form-horizontal tasi-form">
                            <div class="col-sm-12">
                                <div class="row" data-bind="foreach: [constants.attributesNormal,constants.attributesSuperNormal,constants.attributesRare,constants.attributesSuperRare,constants.attributesLegendary]">
                                    <label class="col-sm-12 col-sm-12 panel-heading" data-bind="text: $root.rarities($index());"></label>
                                    </br>
                                    <table class="table" style="width:100%; padding:20px 20px 20px 20px;">
                                          <thead>
                                              <tr>
                                                  <th>Index</th>
                                                  <th>Min</th>
                                                  <th>Max</th>
                                              </tr>
                                          </thead>
                                          <tbody data-bind="foreach: $data">
                                              <tr>
                                                  <td>
                                                      <label type="text" data-bind="text: $root.levels($index());"></label>
                                                  </td>
                                                  <td>
                                                     <input type="text" data-bind="value: minRum" class="form-control">
                                                  </td>
                                                  <td>
                                                     <input type="text" data-bind="value: maxRum" class="form-control">
                                                  </td>
                                              </tr>
                                          </tbody>
                                    </table>
                                </div>
                            </div>
						</div>
		            </div>
                  <!-- ko if: changedFeed -->
                  <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                  <!-- /ko -->
		        </section>	
		    </div>
	    	<!-- Ending Monsters Feed Panel -->
    
    	   <!-- Starting Time Reward Chance Panel -->
		    <div class="col-lg-6">
		        <section class="panel">
		            <header class="panel-heading">
		                Reward Distribution
		            </header>
		            <div class="panel-body">
			            <div class="form-horizontal tasi-form">
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Time to Start Increase Reward Chance(Hours):</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.timedRewardBonus.timeValueInit" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Gap of Time to Increase the Reward Chance(Hours):</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.timedRewardBonus.timeValueInterval" type="text" class="form-control">
						        </div>
						    </div>
						    <div class="form-group">
						        <label class="col-lg-4 col-lg-4 control-label">Increase Factor:</label>
						        <div class="col-lg-8">
						            <input data-bind="value: constants.timedRewardBonus.timeValueFactor" type="text" class="form-control">
						        </div>
						    </div>
    					  <!-- ko if: changedRewardDistribution -->
                          <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                          <!-- /ko -->
						</div>
		            </div>
		        </section>	
		    </div>
    	   <!-- Ending Time Reward Chance Panel -->
	    	
	    	
	    	            
	    	<!-- Starting Unlocks Panel -->
		    <div class="col-lg-6">
		        <section class="panel">
		            <header class="panel-heading">
		                Rewards
		            </header>
		            <div class="panel-body">
			            <div class="form-horizontal tasi-form">
						    <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Luck Draw Rewards:</label>
                                <div class="col-sm-10">
                                  <div class="row">
                                        <table class="table" style="width:100%; padding:20px 20px 20px 20px;">
                                              <thead>
                                                  <tr>
                                                      <th>Type</th>
                                                      <th>Id</th>
                                                      <th>Amount</th>
                                                      <th>Chance</th>
                                                      <th>maxCount</th>
                                                  </tr>
                                              </thead>
                                              <tbody data-bind="foreach: constants.luckyDrawRewards">
                                                  <tr>
                                                      <td>
                                                          <select data-bind="options: REWARD_TYPES, optionsText: function(item) {return REWARD_TYPES_NAMES[REWARD_TYPES.indexOf(item)];}, value: type" class="form-control m-bot15"></select>
                                                      </td>
                                                      <td>
                                                          <select data-bind="options: REWARD_ID, optionsText: function(item) {return REWARD_ID_NAMES[REWARD_ID.indexOf(item)];}, value: id" class="form-control m-bot15"></select>
                                                      </td>
                                                      <td>
                                                         <input type="text" data-bind="value: amount" class="form-control">
                                                      </td>
                                                      <td>
                                                         <input type="text" data-bind="value: chance" class="form-control">
                                                      </td>
                                                      <td>
                                                         <input type="text" data-bind="value: maxCount" class="form-control">
                                                      </td>
                                                  </tr>
                                              </tbody>
                                        </table>
                                    </div>
                                    </br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-horizontal tasi-form">
                                                <button type="button" class="btn btn-success" data-bind="click: function() { addLuckyDrawRewards(); }">Add</button>
                                                <button type="button" class="btn btn-danger" data-bind="click: removeLuckyDrawRewards">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                    </br>
                                </div>
                            </div>
						    
						    <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Daily Rewards:</label>
                                <div class="col-sm-10">
                                  <div class="row">
                                        <table class="table" style="width:100%; padding:20px 20px 20px 20px;">
                                              <thead>
                                                  <tr>
                                                      <th>Type</th>
                                                      <th>Id</th>
                                                      <th>Amount</th>
                                                      <th>Chance</th>
                                                      <th>maxCount</th>
                                                  </tr>
                                              </thead>
                                              <tbody data-bind="foreach: constants.dailyRewards">
                                                  <tr>
                                                      <td>
                                                          <select data-bind="options: REWARD_TYPES, optionsText: function(item) {return REWARD_TYPES_NAMES[REWARD_TYPES.indexOf(item)];}, value: type" class="form-control m-bot15" disabled></select>
                                                      </td>
                                                      <td>
                                                          <select data-bind="options: REWARD_ID, optionsText: function(item) {return REWARD_ID_NAMES[REWARD_ID.indexOf(item)];}, value: id" class="form-control m-bot15" disabled></select>
                                                      </td>
                                                      <td>
                                                         <input type="text" data-bind="value: amount" class="form-control">
                                                      </td>
                                                      <td>
                                                         <input type="text" data-bind="value: chance" class="form-control">
                                                      </td>
                                                      <td>
                                                         <input type="text" data-bind="value: maxCount" class="form-control">
                                                      </td>
                                                  </tr>
                                              </tbody>
                                        </table>
                                    </div>
                                    </br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-horizontal tasi-form">
                                                <button type="button" class="btn btn-success" data-bind="click: function() { addDailyRewards(); }">Add</button>
                                                <button type="button" class="btn btn-danger" data-bind="click: removeDailyRewards">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                    </br>
                                </div>
                            </div>
    					  <!-- ko if: changedRewards -->
                          <button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                          <!-- /ko -->
						</div>
		            </div>
		        </section>	
		    </div>
	    	<!-- Ending Unlocks Panel -->
	   </div>
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />