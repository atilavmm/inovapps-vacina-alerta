<jsp:include page="template_before.jsp" />
<!--main content start-->
<link rel="stylesheet" type="text/css" href="p_css/meta_list.css" />
<script src="p_scripts/user.js"></script>
<section id="main-content">
    <section class="wrapper">
    	<div class="row">
            <!--- General BH Account -->
		    <div class="col-lg-6">
		      <section class="panel">
		            <header class="panel-heading">
		            	General BH
		            </header>
		            <div class="panel-body">
						<div class="row">
                            <div class="form-horizontal tasi-form">
								<div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="col-sm-2 col-sm-2 control-label">Id:</label>
                                        <div class="col-sm-10">
                                            <input type="text" data-bind="value: user.Id" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-sm-2 control-label">Facebook Id:</label>
                                        <div class="col-sm-10">
                                            <input type="text" data-bind="value: user.facebookId" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-sm-2 control-label">Big Hut Id:</label>
                                        <div class="col-sm-10">
                                            <input type="text" data-bind="value: user.bighutId" class="form-control">
                                        </div>
                                    </div>
								</div>
                            </div>
                        </div>
		            </div>
		        </section>		
		    </div>
            
            
            <!--- General User -->
		    <div class="col-lg-6">
		      <section class="panel">
		            <header class="panel-heading">
		            	General User
		            </header>
		            <div class="panel-body">
						<div class="row">
                            <div class="form-horizontal tasi-form">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                          <label class="col-sm-2 col-sm-2 control-label">Gold:</label>
                                          <div class="col-sm-10">
                                              <div class="form-group">
                                                <div class="col-sm-2 col-sm-2">
                                                    <img class="field_element_icon" src="p_icons/gold.png"/>
                                                </div>
                                                <div class="col-sm-10">
                                                    <input type="text" data-bind="value: user.data.bank.gold" class="form-control">
                                                </div>
                                              </div>
                                          </div>
                                    </div>
                                </div>
                            </div>
                        </div>
		            </div>
		            <div class="panel-body">
						<div class="row">
                            <div class="form-horizontal tasi-form">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                          <label class="col-sm-2 col-sm-2 control-label">Rum:</label>
                                          <div class="col-sm-10">
                                              <div class="form-group">
                                                <div class="col-sm-2 col-sm-2">
                                                    <img class="field_element_icon" src="p_icons/rum.png"/>
                                                </div>
                                                <div class="col-sm-10">
                                                    <input type="text" data-bind="value: user.data.bank.rum" class="form-control">
                                                </div>
                                              </div>
                                          </div>
                                    </div>
                                </div>
                            </div>
                        </div>
		            </div>
		        </section>		
		    </div>
            
            <!--- Manager -->
		    <div class="col-lg-6">
		      <section class="panel">
		            <header class="panel-heading">
		            	Manager
		            </header>
		            <div class="panel-body">
						<div class="row">
                            <div class="form-horizontal tasi-form">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-success btn-block"  data-bind="click: rawEditor" style="margin-top:10px;">Open Raw Editor</button>
                                </div>
                            </div>
                        </div>
                        <!-- ko if: changed -->
						<div class="row">
                            <div class="form-horizontal tasi-form">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-warning btn-block"  data-bind="click: submit" style="margin-top:10px;">Submit</button>
                                </div>
                            </div>
                        </div>
                        <!-- /ko -->
		            </div>
		        </section>		
		    </div>
            
		</div>
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />