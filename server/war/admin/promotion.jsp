<jsp:include page="template_before.jsp" />
<!--main content start-->
<script src="p_scripts/promotion.js"></script>
<link rel="stylesheet" type="text/css" href="p_css/meta_list.css" />
<section id="main-content">
    <section class="wrapper">
    	<div class="row">
		    <div class="col-lg-12" style="float: none;">
		    <section class="panel">
		            <header class="panel-heading">
		            	promotion
		            </header>
		            <div class="panel-body">
						<div class="row">
			            	<div class="form-horizontal tasi-form">
								<div class="col-lg-2">
									<img data-bind="attr:{src: image}"/>
                                    <a class="btn btn-danger btn-block" data-toggle="modal" href="#deleteModel">Delete promotion</a>
                                    <div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                  <h4 class="modal-title">Delete Element Confirmation</h4>
                                              </div>
                                              <div class="modal-body">
                                                  Are you sure to delete this entity?
                                              </div>
                                              <div class="modal-footer">
                                                  <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                                  <button class="btn btn-warning" type="button" data-bind="click: deleteElement"> Confirm</button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
								</div> 
								<div class="col-lg-10">
									<div class="form-group">
	                                      <label class="col-sm-2 col-sm-2 control-label">Id:</label>
	                                      <div class="col-sm-10">
	                                          <input type="text" data-bind="value: id" class="form-control">
	                                      </div>
	                                  </div>
									
                                    <div class="form-group">
	                                      <label class="col-sm-2 col-sm-2 control-label">Amount:</label>
                                          <div class="col-sm-10">
                                              <div class="form-group">
                                                <div class="col-sm-2 col-sm-2">
                                                    <img class="field_element_icon" src="p_icons/gold.png"/>
                                                </div>
                                                <div class="col-sm-10">
                                                    <input type="text" data-bind="value: promotion.amount.gold" class="form-control">
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <div class="col-sm-2 col-sm-2">
                                                    <img class="field_element_icon" src="p_icons/rum.png"/>
                                                </div>
                                                <div class="col-sm-10">
                                                    <input type="text" data-bind="value: promotion.amount.rum" class="form-control">
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <div class="col-sm-2 col-sm-2">
                                                    <img class="field_element_icon" src="p_icons/key.png"/>
                                                </div>
                                                <div class="col-sm-10">
                                                    <input type="text" data-bind="value: promotion.amount.key" class="form-control">
                                                </div>
                                              </div>
                                          </div>
	                               </div>
                                    
                                    <div class="form-group">
                                          <label class="col-sm-2 col-sm-2 control-label">Monster ID:</label>
                                          <div class="col-sm-10">
                                          	  <select data-bind="options: MONSTER_ID, optionsText: function(item) {return MONSTER_ID_NAMES[MONSTER_ID.indexOf(item)];}, value: promotion.monsterId" class="form-control m-bot15"></select>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label class="col-sm-2 col-sm-2 control-label">Monster Level:</label>
                                          <div class="col-sm-10">
                                              <input type="text" data-bind="value: promotion.levelMonster" class="form-control">
                                          </div>
                                      </div>
                                      
                                    <div class="form-group">
                                          <label class="col-sm-2 col-sm-2 control-label">Promotion duration (Seconds):</label>
                                          <div class="col-sm-10">
                                              <input type="text" data-bind="value: promotion.duration" class="form-control">
                                          </div>
                                      </div>
                                    
                                    <div class="form-group">
                                          <label class="col-sm-2 col-sm-2 control-label">Extra:</label>
                                          <div class="col-sm-10">
                                              <input type="text" data-bind="value: promotion.extra" class="form-control">
                                          </div>
                                      </div>
                                    <div class="form-group">
                                          <label class="col-sm-2 col-sm-2 control-label">Store ID:</label>
                                          <div class="col-sm-10">
                                              <input type="text" data-bind="value: promotion.storeId" class="form-control">
                                          </div>
                                      </div>
                                    
                                    <div class="form-group">
                                          <label class="col-sm-2 col-sm-2 control-label">Graph URL:</label>
                                          <div class="col-sm-10">
                                              <input type="text" data-bind="value: promotion.graphUrl" class="form-control">
                                          </div>
                                      </div>
                                    <div class="form-group">
                                          <label class="col-sm-2 col-sm-2 control-label">Discount:</label>
                                          <div class="col-sm-10">
                                              <input type="text" data-bind="value: promotion.discount" class="form-control">
                                          </div>
                                      </div>
								</div> 
							</div>
		           		</div>
                        
						<div class="row">
							<div class="col-lg-12">
                                <div class="form-horizontal tasi-form">
                                    <!-- ko if: changed -->
                                    <button type="button" class="btn btn-success btn-warning btn-block"  data-bind="click: submit" style="margin-top:10px;">Submit</button>
                                	<!-- /ko -->
                                </div>
                            </div>
                        </div>
		            </div>
		        </section>		
		    </div>
		</div>
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />