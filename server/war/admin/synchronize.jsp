<jsp:include page="template_before.jsp" />
<!--main content start-->
<script src="p_scripts/synchronize.js"></script>
<section id="main-content">
    <section class="wrapper">
        
        <div class="row">
		    <div class="col-lg-12">
		      <section class="panel">
		            <header class="panel-heading">
		            	Backup
		            </header>
		            <div class="panel-body">
                        <div class="form-horizontal tasi-form">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <a class="btn btn-success btn-block" href="/_ah/api/vacinaserver/v1/backup/get" download>Download</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <form action="/backup" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                          <label for="exampleInputFile">File input</label>
                                          <input type="file" name="file" id="exampleInputFile">
                                          <p class="help-block">Example block-level help text here.</p>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="submit" class="btn btn-danger btn-block" value="Upload" >
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />