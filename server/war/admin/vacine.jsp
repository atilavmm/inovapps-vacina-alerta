<jsp:include page="template_before.jsp" />
<!--main content start-->
<script src="p_scripts/vacine.js"></script>
<link rel="stylesheet" type="text/css" href="p_css/meta_list.css" />
<section id="main-content">
<section class="wrapper">
<div class="row">
	<div class="col-lg-12" style="float: none;">
		<section class="panel">
			<header class="panel-heading">
				Vacine
			</header>
			<div class="panel-body">
				<div class="row">
					<div class="form-horizontal tasi-form">
						<div class="col-lg-2">
							<a class="btn btn-danger btn-block" data-toggle="modal" href="#deleteModel">Delete Vacine</a>
							<div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title">Delete Element Confirmation</h4>
										</div>
										<div class="modal-body">
											Are you sure to delete this entity?
										</div>
										<div class="modal-footer">
											<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
											<button class="btn btn-warning" type="button" data-bind="click: deleteElement"> Confirm</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-10">
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Id:</label>
								<div class="col-sm-10">
									<input type="text" data-bind="value: id" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Nome:</label>
								<div class="col-sm-10">
									<input type="text" data-bind="value: vacine.name" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Descrição:</label>
								<div class="col-sm-10">
                                    <textarea data-bind="value:vacine.description" style="width:100%;" rows="10" cols="1000"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Previne:</label>
								<div class="col-sm-10">
                                    <textarea data-bind="value:vacine.prevents" style="width:100%;" rows="10" cols="1000"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Indicação:</label>
								<div class="col-sm-10">
                                    <textarea data-bind="value:vacine.indication" style="width:100%;" rows="10" cols="1000"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Contra-Indicação:</label>
								<div class="col-sm-10">
                                    <textarea data-bind="value:vacine.counterIndication" style="width:100%;" rows="10" cols="1000"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Efeitos-Colaterais:</label>
								<div class="col-sm-10">
                                    <textarea data-bind="value:vacine.adverseEffect" style="width:100%;" rows="10" cols="1000"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Homem:</label>
								<div class="col-sm-10">
                                    <input type="checkbox" value="value: vacine.male" data-bind="checked: vacine.male" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Mulher:</label>
								<div class="col-sm-10">
                                    <input type="checkbox" value="value: vacine.female" data-bind="checked: vacine.female" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Dosagem Fixa:</label>
								<div class="col-sm-10">
									<input type="text" data-bind="value: vacine.fixedDosage" class="form-control">
								</div>
                            </div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Dosagem Periodica:</label>
								<div class="col-sm-10">
									<input type="text" data-bind="value: vacine.periodicDosage" class="form-control">
								</div>
                            </div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Periodo:</label>
								<div class="col-sm-4">
									<input type="text" data-bind="value: vacine.start" class="form-control">
								</div>
								<div class="col-sm-2">
									-
								</div>
								<div class="col-sm-4">
									<input type="text" data-bind="value: vacine.end" class="form-control">
								</div>
                            </div>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Disponivel durante Gravidez:</label>
								<div class="col-sm-10">
                                    <input type="checkbox" value="value: vacine.pregnantAvailable" data-bind="checked: vacine.pregnantAvailable" class="form-control">
								</div>
                            </div>
						</div>
					</div>
				</div>
				<br>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-horizontal tasi-form">
							<!-- ko if: changed -->
							<button type="button" class="btn btn-success btn-warning btn-block"  data-bind="click: submit" style="margin-top:10px;">Submit</button>
							<!-- /ko -->
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
</section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />