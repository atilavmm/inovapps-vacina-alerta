<jsp:include page="template_before.jsp" />
<!--main content start-->
<script src="p_scripts/raw_user.js"></script>

<section id="main-content">
    <section class="wrapper">
    	<div class="row">
		    <div class="col-lg-12">
		    <section class="panel">
		            <header class="panel-heading">
		                User
		            </header>
		            <div class="panel-body">
		            <textarea data-bind="value:data" id="gameDataJson" style="width:100%;" rows="30" cols="1000"></textarea>
                    <!-- ko if: changed-->
               	  	<button type="button" class="btn btn-success btn-warning btn-block" data-bind="click: submit" style="margin-top:10px;">Submit</button>
                    <!-- /ko -->
		            </div>
		        </section>		
		    </div>
		</div>
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />