<jsp:include page="template_before.jsp" />
<!--main content start-->
<script src="p_scripts/user_find.js"></script>
<section id="main-content">
    <section class="wrapper">
    	<div class="row">
		    <div class="col-lg-12">
		    <section class="panel">
		            <header class="panel-heading">
		                User Find
		            </header>
		            <div class="panel-body">
                        <div class="form-horizontal tasi-form">
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Find Facebook:</label>
                                <div class="col-sm-5">
                                    <input type="text" data-bind="value: facebookId" class="form-control">
                                </div>
                                <div class="col-sm-5">
                                    <button type="button" class="btn btn-success btn-success btn-block"  data-bind="click: openFacebookUser, disable: hasFacebook">Go</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Id:</label>
                                <div class="col-sm-5">
                                    <input type="text" data-bind="value: id" class="form-control">
                                </div>
                                <div class="col-sm-5">
                                    <button type="button" class="btn btn-success btn-success btn-block"  data-bind="click: openGeneralUser, disable: hasId">Go</button>
                                </div>
                            </div>
                        </div>
		            </div>
		        </section>		
		    </div>
		</div>
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />