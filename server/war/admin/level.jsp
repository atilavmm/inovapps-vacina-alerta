<jsp:include page="template_before.jsp" />
<!--main content start-->
<script src="p_scripts/level.js"></script>
<link rel="stylesheet" type="text/css" href="p_css/meta_list.css" />
<link rel="stylesheet" type="text/css" href="p_css/level.css" />
<section id="main-content">
    <section class="wrapper">
    	<div class="row">
            <div class="col-lg-12" style="float: none;">
                <section class="panel">
                    <header class="panel-heading">
                        Level
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-horizontal tasi-form">
                                <div class="col-lg-2">
                                    <img data-bind="attr:{src: image, id: id}" style="display:none;"/>
                                    <a class="btn btn-danger btn-block" data-toggle="modal" href="#deleteModel">Delete Level</a>
                                    <div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                  <h4 class="modal-title">Delete Element Confirmation</h4>
                                              </div>
                                              <div class="modal-body">
                                                  Are you sure to delete this entity?
                                              </div>
                                              <div class="modal-footer">
                                                  <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                                  <button class="btn btn-warning" type="button" data-bind="click: deleteElement"> Confirm</button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                </div> 
                                <div class="col-lg-10">
                                    <div class="form-group">
                                          <label class="col-sm-2 col-sm-2 control-label">Id:</label>
                                          <div class="col-sm-10">
                                              <input type="text" data-bind="value: id" class="form-control">
                                          </div>
                                      </div>


                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Type:</label>
                                            <div class="col-sm-10">
                                                <div class="col-sm-2 col-sm-2">
                                                    <img class="big_field_element_icon" data-bind="attr:{src: typeIcon}"/>
                                                </div>
                                                <div class="col-sm-10">
                                                   <select data-bind="options: types, optionsText: function(item) {return LEVEL_TYPES_NAMES[types().indexOf(item)];}, value: level.type" class="form-control m-bot15"></select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Difficulty:</label>
                                            <div class="col-sm-10">
                                                <div class="col-sm-2 col-sm-2">
                                                    <img class="big_field_element_icon" data-bind="style:{'background-color': level.difficulty}"/>
                                                </div>
                                                <div class="col-sm-10">
                                                   <select data-bind="options: difficultyTypes, optionsText: function(item) {return DIFFICULTY_TYPES_NAMES[difficultyTypes().indexOf(item)];}, value: level.difficulty" class="form-control m-bot15"></select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Author:</label>
                                            <div class="col-sm-10">
                                                <div class="col-sm-10">
                                                   <select data-bind="options: AUTHORS, optionsText: function(item) {return AUTHORS_NAMES[AUTHORS.indexOf(item)];}, value: level.author" class="form-control m-bot15"></select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Comment:</label>
                                            <div class="col-sm-10">
                                                <input type="text" data-bind="value: level.comment" class="form-control">
                                            </div>
                                        </div>                                        

                                        <!-- ko if: isGate -->
                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Unlock:</label>
                                            <div class="col-sm-5">
                                                <label class="col-sm-4 col-sm-4 control-label">Duration(Seconds):</label>
                                                <div class="col-sm-8">
                                                    <input type="text" data-bind="value: level.unlockGateInSeconds" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <label class="col-sm-2 col-sm-2 control-label">Price:</label>
                                                  <div class="col-sm-10">
                                                      <div class="form-group">
                                                        <div class="col-sm-2 col-sm-2">
                                                            <img class="field_element_icon" src="p_icons/gold.png"/>
                                                        </div>
                                                        <div class="col-sm-10">
                                                            <input type="text" data-bind="value: level.price.gold" class="form-control">
                                                        </div>
                                                      </div>
                                                  </div>
                                            </div>
                                        </div>
                                        <!-- /ko -->

                                        <!-- ko ifnot: isGate -->

                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Tutorial Id:</label>
                                            <div class="col-sm-10">
                                                <select data-bind="options: TUTORIAL_TYPES, optionsText: function(item) {return TUTORIAL_TYPES_NAMES[TUTORIAL_TYPES.indexOf(item)];}, value: level.tutorialId" class="form-control m-bot15"></select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Score Stars:</label>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-2">
                                                        <label class="col-sm-2 col-sm-2 control-label">3:</label>
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <input type="text" data-bind="value: level.scoreTo3Star" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-2">
                                                        <label class="col-sm-2 col-sm-2 control-label">2:</label>
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <input type="text" data-bind="value: level.scoreTo2Star" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-2">
                                                        <label class="col-sm-2 col-sm-2 control-label">1:</label>
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <input type="text" data-bind="value: level.scoreTo1Star" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Total Moves:</label>
                                            <div class="col-sm-10">
                                                <input type="text" data-bind="value: level.totalMoves" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Gems Distribution:</label>
                                            <div class="col-sm-10">
                                              <div class="row">
                                                    <table class="table" style="width:100%; padding:20px 20px 20px 20px;">
                                                          <thead>
                                                              <tr>
                                                                  <th>Icon</th>
                                                                  <th>Gem Type</th>
                                                                  <th>Probability</th>
                                                              </tr>
                                                          </thead>
                                                          <tbody data-bind="foreach: level.gems">
                                                              <tr>
                                                                  <td>
                                                                    <img class="big_field_element_icon" data-bind="attr:{src: icon}"/>
                                                                  </td>
                                                                  <td>
                                                                      <select data-bind="options: GEM_TYPES, optionsText: function(item) {return GEM_TYPES_NAMES[GEM_TYPES.indexOf(item)];}, value: gemType" class="form-control m-bot15"></select>
                                                                  </td>
                                                                  <td>
                                                                     <input type="text" data-bind="value: probability" class="form-control">
                                                                  </td>
                                                              </tr>
                                                          </tbody>
                                                    </table>
                                                </div>
                                                </br>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-horizontal tasi-form">
                                                            <button type="button" class="btn btn-success" data-bind="click: function() { addGem(); }">Add</button>
                                                            <button type="button" class="btn btn-danger" data-bind="click: removeGem">Remove</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                </br>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Raw:</label>
                                            <div class="col-sm-10">
                                                <div class="form-group">
                                                    <textarea data-bind="value:raw" id="gameDataJson" style="width:95%;" rows="30" cols="1000"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /ko -->



                                </div> 
                            </div>
                        </div>

                          <div class="row">
                              <div class="col-lg-12">
                                    <!-- ko if: changed -->
                                    <button type="button" class="btn btn-success btn-warning btn-block"  data-bind="click: submit" style="margin-top:10px;">Submit</button>
                                    <!-- /ko -->
                                </div>
                            </div>
                        </div>
                </section>	
            </div>	
		  </div>

        <!-- Analytics -->
    	<div class="row">
            <div class="col-lg-12" style="float: none;">
                <section class="panel">
                    <header class="panel-heading">
                        Analytics
                    </header>
                    <div class="panel-body">
                    <div class="form-group">
                        <!-- Fuu -->
                        <div class="row">
                            <div class="form-group col-lg-3">
                                <label class="col-sm-6 col-sm-6 control-label">Fuu Factor:</label>
                                <div class="col-sm-6">
                                    <input type="text" data-bind="value: level.analytics.fuuFactor" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                        <!-- Log Count -->
                        <div class="row">
                            <div class="form-group col-lg-3">
                                <label class="col-sm-6 col-sm-6 control-label">Log Count:</label>
                                <div class="col-sm-6">
                                    <input type="text" data-bind="value: level.analytics.logCount" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                        <br>
                        <!-- Moves -->
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label class="col-sm-2 col-sm-2 control-label">Moves:</label>
                                <div class="col-sm-10">
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Min:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.moves.min" class="form-control" disabled>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Mean:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.moves.average" class="form-control" disabled>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Max:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.moves.max" class="form-control" disabled>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Deviation:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.moves.sDev" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        
                        
                        <!-- Score -->
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label class="col-sm-2 col-sm-2 control-label">Score:</label>
                                <div class="col-sm-10">
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Min:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.score.min" class="form-control" disabled>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Mean:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.score.average" class="form-control" disabled>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Max:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.score.max" class="form-control" disabled>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Deviation:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.score.sDev" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        
                        <!-- Tries -->
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label class="col-sm-2 col-sm-2 control-label">Tries:</label>
                                <div class="col-sm-10">
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Min:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.tries.min" class="form-control" disabled>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Mean:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.tries.average" class="form-control" disabled>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Max:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.tries.max" class="form-control" disabled>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            Deviation:
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" data-bind="value: level.analytics.tries.sDev" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        
                        <!-- Fuu -->
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <div class="row">
                                    <label class="col-sm-12 col-sm-12 control-label">Heat Maps:</label>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="col-lg-12">Match</div>
                                        <div class="col-lg-12"><canvas id="heat_match" width="36" height="9" style="width: 100%;"></canvas></div>
                                        <div class="col-lg-12" data-bind="text: level.analytics.heatmapMatchRange">
                                            Range: [0-0]
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="col-lg-12">Death</div>
                                        <div class="col-lg-12"><canvas id="heat_death" width="36" height="9" style="width: 100%;"></canvas></div>
                                        <div class="col-lg-12" data-bind="text: level.analytics.heatmapDeathRange">
                                            Range: [0-0]
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="col-lg-12">Ship</div>
                                        <div class="col-lg-12"><canvas id="heat_ship" width="36" height="9" style="width: 100%;"></canvas></div>
                                        <div class="col-lg-12" data-bind="text: level.analytics.heatmapShipRange">
                                            Range: [0-0]
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">Heatmap Count:<input type="text" data-bind="value: level.analytics.heatmapCount" class="form-control" disabled></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<!--main content end-->
<jsp:include page="template_after.jsp" />